﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Offer;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Readers
{
    public static class OfferReader
    {
        public static OfferSearchResponse ToOfferSearchResult(this DataReader reader)
        {
            if (reader == null) return null;

            var offers = new List<Offer>();
            var totalOffers = -1;

            while (reader.Read())
            {
                offers.Add(
                    new Offer
                    {
                        OfferNumber = reader.GetInt("OfferID"),
                        StartDate = reader.GetDateTimeNullable("StartDate"),
                        EndDate = reader.GetDateTimeNullable("EndDate"),
                        TakeAll = reader.GetBool("TakeAll"),
                        TotalStyles = reader.GetInt("TotalStyles"),
                        TotalCostOnOffer = reader.GetDecimal("TotalCostOnOffer"),
                        TotalCostSold = reader.GetDecimal("TotalCostSold"),
                        TotalCostSellThrough = reader.GetDecimal("TotalCostSellThrough"),
                        UnitsOnOffer = reader.GetInt("UnitsOnOffer"),
                        UnitsSold = reader.GetInt("UnitsSold"),
                        UnitsSellThrough = reader.GetDecimal("UnitsSellThrough"),
                        OfferStatusName = reader.GetString("OfferStatusName"),
                        OfferStatus = (OfferStatus)reader.GetInt("OfferStatusID")
                    });
            }

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    totalOffers = reader.GetInt("TotalOffers");
                }
            }

            reader.Close();


            var response = new OfferSearchResponse()
            {
                Offers = offers,
                TotalOffers = totalOffers
            };
            return response;
        }

        public static OfferLiveSummary ToOfferLiveSummaryResult(this DataReader reader)
        {
            if (reader == null) return null;

            var offersIDs = new List<int>();
            var productIDs = new List<int>();

            while (reader.Read())
            {
                offersIDs.Add(reader.GetInt("OfferID"));
                productIDs.Add(reader.GetInt("ProductId"));
            }

            return new OfferLiveSummary()
            {
                TotalProducts = productIDs.Distinct().Count(),
                TotalOffers = offersIDs.Distinct().Count()
            };
        }

        public static OfferImport ToOfferItemsResult(this DataReader reader)
        {
            if (reader == null) return null;

            bool IsLive = false, isTakeAll = false;
            int OfferImportBatchID = -1, OfferStatusID = 0;
            DateTimeOffset? startDate = null, endDate = null;
            while (reader.Read())
            {
                IsLive = reader.GetBool("IsLive");
                OfferImportBatchID = reader.GetInt("OfferImportBatchID");

                if (!IsLive)
                {
                    startDate = reader.GetDateTimeOffsetNullable("StartDate");
                    endDate = reader.GetDateTimeOffsetNullable("EndDate");
                    isTakeAll = reader.GetBool("TakeAll");
                    OfferStatusID = reader.GetInt("OfferStatusID");
                }
            }

            OfferImport oi = null;
            var products = new List<OfferItemDetails>();
            if (reader.NextResult())
            {

                if (IsLive)
                {
                    while (reader.Read())
                    {
                        products.Add(new OfferItemDetails()
                        {
                            OfferNumber = reader.GetInt("OfferNumber"),
                            CurrencyID = reader.GetInt("CurrencyID"),
                            StartDate = reader.GetDateTimeOffset("StartDate"),
                            EndDate = reader.GetDateTimeOffset("EndDate"),
                            TakeAll = reader.GetBool("TakeAll"),
                            ProductName = reader.GetString("ProductName"),
                            ProductID = reader.GetInt("ProductID"),
                            ModelNumber = reader.GetString("ModelNumber"),
                            ColorCode = reader.GetString("ColorCode"),
                            MaterialCode = reader.GetString("MaterialCode"),
                            RetailPrice = reader.GetDouble("RetailPrice"),
                            OfferCost = reader.GetDouble("OfferCost"),
                            CustomerUnitPrice = reader.GetDouble("CustomerUnitPrice"),
                            TotalUnits = reader.GetInt("TotalUnits"),
                            CustomerTotalPrice = reader.GetDouble("CustomerTotalPrice"),
                            CustomerDiscount = reader.GetDouble("CustomerDiscount"),
                            BrandName = reader.GetString("BrandName"),
                            CategoryName = reader.GetString("CategoryName"),
                            ProductMainImageName = reader.GetString("ProductMainImageName"),
                            OfferStatusID = reader.GetInt("OfferStatusID")
                        });

                        oi = (from o in products
                              group o by o.OfferNumber into grpOffer
                              let grpOfferKey = products.First(p => p.OfferNumber == grpOffer.Key)

                              let offer = new OfferImport
                              {
                                  Id = grpOfferKey.OfferNumber,
                                  StartDate = grpOfferKey.StartDate,
                                  EndDate = grpOfferKey.EndDate,
                                  TakeAll = grpOfferKey.TakeAll,
                                  OfferStatus = (OfferStatus)grpOfferKey.OfferStatusID,
                                  Items = (from p in grpOffer
                                           select new OfferImportBachItem
                                           {
                                               ProductName = p.ProductName,
                                               Currency = (CurrencyType)p.CurrencyID,
                                               ModelNumber = p.ModelNumber,
                                               BrandName = p.BrandName,
                                               CategoryName = p.CategoryName,
                                               ColorCode = p.ColorCode,
                                               MaterialCode = p.MaterialCode,
                                               ProductRetailPrice = p.RetailPrice,
                                               OfferCostValue = p.OfferCost,
                                               OfferTotalPrice = p.OfferCost * p.TotalUnits,
                                               OfferCostValueConverted = p.OfferCostValueConverted,
                                               CustomerUnitPrice = p.CustomerUnitPrice,
                                               ActualTotalUnits = p.TotalUnits,
                                               CustomerTotalPrice = p.CustomerTotalPrice,
                                               CustomerDiscount = p.CustomerDiscount,
                                               MainImageName = p.ProductMainImageName
                                           }
                                           ).ToList()
                              }
                              select offer).First();

                    }
                    oi.TotalCost = oi.Items.Sum(x => x.OfferTotalPrice ?? 0);
                    oi.TotalUnits = oi.Items.Sum(x => x.ActualTotalUnits ?? 0);

                    return oi;
                }
                else
                {
                    var offerImportList = reader.ToOfferImportBachItemList();
                    var errorList = reader.NextResult() ? reader.ToOfferImportBatchItemErrorList() : new List<OfferImportBatchItemError>();
                    offerImportList.ForEach(x => x.Errors = errorList.Where(y => y.OfferImportBatchItemId == x.OfferImportBatchItemId).ToList());

                    oi = new OfferImport
                    {
                        TotalCost = offerImportList.Sum(x => x.OfferTotalPrice ?? 0),
                        TotalUnits = offerImportList.Sum(x => x.ActualTotalUnits ?? 0),
                        Id = OfferImportBatchID,
                        Items = offerImportList,
                        StartDate = startDate,
                        EndDate = endDate,
                        TakeAll = isTakeAll,
                        OfferStatus = (OfferStatus)OfferStatusID
                    };
                }
            }

            return oi;
        }
    }
}
