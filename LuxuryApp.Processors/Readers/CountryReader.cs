﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Processors.Readers
{
    public static class CountryReader
    {
        public static List<Country> ToCountryList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<CountryModelReader>();
            while (reader.Read())
            {
                var item = new CountryModelReader();
                item.CountryID = reader.GetInt("CountryID");
                item.CountryName = reader.GetString("CountryName");
                item.CountryISO2Code = reader.GetString("CountryISO2Code");
                item.StateId = reader.GetInt("StateId");
                item.StateName = reader.GetString("StateName");
                item.StateAbbreviation = reader.GetString("StateAbbreviation");
                items.Add(item);
            }

            reader.Close();

            // process list
            var countries = (from c in items
                             group c by c.CountryID into grpCountry
                             let grpCountryKey = items.First(c => c.CountryID == grpCountry.Key)

                             let country = new Country()
                             {
                                 Id = grpCountryKey.CountryID,
                                 CountryName = grpCountryKey.CountryName,
                                 ISO2Code = grpCountryKey.CountryISO2Code,
                                 States = (from s in grpCountry
                                           select new State()
                                           {
                                               StateId = s.StateId,
                                               StateName = s.StateName,
                                               Abbreviation = s.StateAbbreviation,
                                               CountryId = s.CountryID
                                           }).ToList()
                             }
                             select country).ToList();

            return countries;
        }

    }
}
