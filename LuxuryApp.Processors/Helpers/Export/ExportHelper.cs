﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models.Cart;
using LuxuryApp.Processors.Resources.Export;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System;
using System.Drawing;
using System.IO;
using System.Net;

namespace LuxuryApp.Processors.Helpers.Export
{
    public class ExportHelper
    {
        public static byte[] ExportCartExcel(Cart cart)
        {
            using (var output = new MemoryStream())
            {
                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(output))
                {
                    // Add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(CartExcelNames.WorksheetName);

                    // Add some formatting to the worksheet
                    worksheet.DefaultColWidth = 20;
                    worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    // First row
                    int rowNumber = 1;
                    worksheet.Cells["A1:A2"].Merge = true;
                    worksheet.Cells["B1:V1"].Merge = true;
                    worksheet.Row(rowNumber).Height = 80;
                    AddImage(worksheet, 0, rowNumber - 1, WebConfigs.ExportCartLogoUrl, true);

                    // Second row
                    worksheet.Row(++rowNumber).Height = 20;
                    worksheet.Cells[rowNumber, 2].Value = DateTime.Now.ToString();
                    worksheet.Cells[rowNumber, 8].Value = CartExcelColumnNames.Total;
                    worksheet.Cells[rowNumber, 9].Value = CartExcelColumnNames.Total;
                    worksheet.Cells[rowNumber, 10].Value = CartExcelColumnNames.Total;

                    // Third row
                    worksheet.Row(++rowNumber).Height = 15;
                    worksheet.Cells[rowNumber, 1].Value = CartExcelColumnNames.Picture;
                    worksheet.Cells[rowNumber, 2].Value = CartExcelColumnNames.Brand;
                    worksheet.Cells[rowNumber, 3].Value = CartExcelColumnNames.SKU;
                    worksheet.Cells[rowNumber, 4].Value = CartExcelColumnNames.Color;
                    worksheet.Cells[rowNumber, 5].Value = CartExcelColumnNames.Quantity;
                    worksheet.Cells[rowNumber, 6].Value = CartExcelColumnNames.UnitCost;
                    worksheet.Cells[rowNumber, 7].Value = CartExcelColumnNames.UnitLandedCost;
                    worksheet.Cells[rowNumber, 8].Value = CartExcelColumnNames.TotalProductCost;
                    worksheet.Cells[rowNumber, 9].Value = CartExcelColumnNames.RetailValue;
                    worksheet.Cells[rowNumber, 10].Value = CartExcelColumnNames.TotalLandedCost;

                    worksheet.Row(rowNumber).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Row(rowNumber).Style.Font.Color.SetColor(Color.White);
                    rowNumber++;

                    var currencySymbol = cart.Currency.Name.Equals(CurrencyType.EUR.ToString()) ? CartExcelNames.CurrencySymbolEUR : CartExcelNames.CurrencySymbolUSD;
                    foreach (var cartItem in cart.Items)
                    {
                        foreach (var item in cartItem.SellerUnits)
                        {
                            AddImage(worksheet, 0, rowNumber - 1, cartItem.ProductMainImageLink);
                            worksheet.Cells[rowNumber, 2].Value = cartItem.BrandName;
                            worksheet.Cells[rowNumber, 3].Value = cartItem.ProductSku;
                            worksheet.Cells[rowNumber, 4].Value = cartItem.StandardColorName;
                            worksheet.Cells[rowNumber, 5].Value = item.UnitsCount.ToString();
                            worksheet.Cells[rowNumber, 6].Value = currencySymbol + item.CustomerUnitPrice.ToString();
                            worksheet.Cells[rowNumber, 7].Value = currencySymbol + item.TotalShippingCost.ToString();
                            worksheet.Cells[rowNumber, 8].Value = currencySymbol + item.TotalCustomerCost.ToString();
                            worksheet.Cells[rowNumber, 9].Value = currencySymbol + item.RetailPrice.ToString();
                            worksheet.Cells[rowNumber, 10].Value = currencySymbol + cartItem.TotalLandedCost.ToString();

                            worksheet.Row(rowNumber++).Height = 90;
                        }
                    }

                    worksheet.Column(1).Width = 36;
                    worksheet.Column(8).Width = 25;
                    worksheet.Column(9).Width = 25;
                    worksheet.Column(10).Width = 25;

                    worksheet.Column(2).AutoFit();
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(8).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(10).AutoFit();

                    // save our new workbook
                    package.Save();

                    return output.ToArray();
                }
            }
        }

        private static void AddImage(ExcelWorksheet ws, int columnIndex, int rowIndex, string imageURL, bool isLogo = false)
        {
            using (WebClient wc = new WebClient())
            {
                using (Stream s = wc.OpenRead(imageURL))
                {
                    using (Bitmap bmp = new Bitmap(s))
                    {
                        ExcelPicture picture = null;
                        if (bmp != null)
                        {
                            picture = ws.Drawings.AddPicture("product-image-" + rowIndex.ToString() + columnIndex.ToString(), bmp);
                            picture.From.Column = columnIndex;
                            picture.From.Row = rowIndex;
                            if (!isLogo)
                            {
                                picture.From.ColumnOff = Pixel2MTU(80); //20 pixel space for better alignment
                                picture.From.RowOff = Pixel2MTU(10);//10 pixel space for better alignment
                                picture.SetSize(90);
                            }
                            else
                            {
                                picture.SetSize(100);
                            }
                        }
                    }
                }
            }

        }

        private static int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }
    }
}
