﻿using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LuxuryApp.Processors.Helpers.OfferImport
{
    public static class OfferFileValidator
    {
        public static string ValidateFileStructure(DataSet dataset, List<string> expectedColumns)
        {
            if (dataset?.Tables?.Count != 2)
                return "Invalid file structure. 2 tables are required";

            var dtSizes = dataset.Tables[0];
            if (dtSizes.Columns.Count == 0 || dtSizes.Rows.Count == 0)
                return "Invalid Sizes table - Columns or Rows Missing";


            var dtOffer = dataset.Tables[1];
            if (dtOffer.Columns.Count == 0 || dtOffer.Rows.Count == 0)
                return "Invalid Offer table - Columns or Rows Missing";

            return ValidateOfferRequiredColumns(dtOffer, expectedColumns);
        }

        public static string ValidateOfferRequiredColumns(DataTable dtOffer, List<string> expectedColumns)
        {
            var columnNames = (from dc in dtOffer.Columns.Cast<DataColumn>()
                               select dc.ColumnName.ToLower().Trim()).ToList();

            var missingColumns = expectedColumns.Where(x => !columnNames.Contains(x)).Select(x => x).Distinct().ToList();

            if (missingColumns.Any())
                return $"Product Offer table - Missing columns: {string.Join(";", missingColumns)}";

            return string.Empty;
        }

    }
}
