﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Offer;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Processors.Readers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace LuxuryApp.Processors.Services
{
    public class OfferService : IOfferService
    {
        private readonly Repository _connection;
        private readonly Func<IOfferRepository> _offerRepository;
        private readonly Func<IContextProvider> _contextProvider;

        public OfferService(Func<IOfferRepository> offerRepository, Func<IContextProvider> contextProvider)
        {
            _connection = new Repository();
            _offerRepository = offerRepository;
            _contextProvider = contextProvider;
        }

        public OfferSearchResponse Search(OfferSearch model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@CompanyID", model.CompanyID),
              new SqlParameter("@SellerID", model.SellerID),
              new SqlParameter("@PageSize", model.PageSize),
              new SqlParameter("@PageNumber", model.Page),
              new SqlParameter("@Keyword", model.Keyword),
              new SqlParameter("@StatusID", model.StatusID),
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_Search, parameters);
            return reader.ToOfferSearchResult();
        }

        public OfferLiveSummary GetLiveSummary(OfferLiveSummaryRequest model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@CompanyID",model.CompanyID),
              new SqlParameter("@SellerID", model.SellerID)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetLiveSummary, parameters);
            return reader.ToOfferLiveSummaryResult();
        }

        public async Task<IEnumerable<OfferStatusResponse>> GetStatuses()
        {
            List<OfferStatusResponse> enumValList = new List<OfferStatusResponse>();

            foreach (var e in Enum.GetValues(typeof(OfferStatus)))
            {
                var fi = e.GetType().GetField(e.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                enumValList.Add(new OfferStatusResponse()
                {
                    ID = (int)e,
                    Name = (attributes.Length > 0) ? attributes[0].Description : e.ToString()
                });
            }

            return enumValList;
        }

        public OfferImport GetProducts(int offerNumber)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferNumber",offerNumber)
            };

            var reader = _connection.LoadData(StoredProcedureNames.Offer_GetProducts, parameters);
            return reader.ToOfferItemsResult();
        }

        public void UpdateOfferEndDate(Offer model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferID",model.Id),
              new SqlParameter("@OfferEndDate",model.EndDate)
            };

            _connection.ExecuteProcedure(StoredProcedureNames.Offer_UpdateEndDate, parameters);
        }

        public void DeleteOffer(int offerId)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@OfferNumber",offerId),
            };

            _connection.ExecuteProcedure(StoredProcedureNames.Offer_Delete, parameters);
        }

        public async Task<ApiResult<bool>> IsActiveOffer(int offerId)
        {
            var result = await _offerRepository().IsActiveOffer(offerId);
            return result.ToApiResult();
        }

      }
}
