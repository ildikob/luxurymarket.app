﻿using LuxuryApp.Contracts.Services;
using LuxuryApp.Utils.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LuxuryApp.Processors.Resources.Import;
using System.IO;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Enums;
using System;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Processors.Helpers.OfferImport;
using System.Threading.Tasks;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.Core.Infrastructure.Logging;

namespace LuxuryApp.Processors.Services
{
    public class OfferImportService : IOfferImportService
    {
        private readonly Dictionary<string, string> _offerResourceColumns;
        private readonly List<string> _offerProductColumnsValues;
        private readonly Func<IContextProvider> _contextProviderFactory;
        private readonly ILogger _logger;

        public Func<IOfferImportRepository> _oiRepositoryFactory;
        public readonly Func<ICustomerService> _customerFactory;

        public OfferImportService(Func<IContextProvider> contextProviderFactory, Func<IOfferImportRepository> offerImportRepositoryFactory,
                                  Func<ICustomerService> customerFactory, ILogger logger)
        {
            _contextProviderFactory = contextProviderFactory;
            _oiRepositoryFactory = offerImportRepositoryFactory;
            _customerFactory = customerFactory;
            _offerResourceColumns = OfferImportHelper.GetOfferColumns();
            _offerProductColumnsValues = _offerResourceColumns.Select(x => x.Value).ToList();
            _logger = logger;
        }

        #region Public Methods

        public ApiResult<OfferImport> UploadFile(string fileName, Stream file, int? companyId, int userId)
        {
            if (!companyId.HasValue)
                return new ApiResultForbidden<OfferImport>(null);

            DataSet dataSet = null;
            DataTable products = null;
            DataTable sizes = null;

            try
            {
                dataSet = ExcelParseHelper.ParseWorkSheet(file);

                var errorMessage = OfferFileValidator.ValidateFileStructure(dataSet, _offerProductColumnsValues);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                    return new ApiResultUnprocessableEntity<OfferImport>(null, errorMessage);

                ProcessData(dataSet, out products, out sizes);

            }
            catch (Exception ex)
            {
                _logger.Error($"companyId: {companyId},userid: {userId} fileName: {fileName}  - {ex.Message}");

                return new ApiResultUnprocessableEntity<OfferImport>(null, "The file could not be processed");
            }


            var offerImportBatchId = 0;

            var result = _oiRepositoryFactory().InsertBatchItems(fileName, products, sizes, out offerImportBatchId, companyId ?? 0, userId);
            return result.ToApiResult();
        }

        public async Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId)
        {
            var result = await _oiRepositoryFactory().GetOfferProductFullData(offerImportBatchItemId);
            return result;
        }

        public async Task<ApiResult<OfferImportSummary>> GetOfferImportSummary(int offerImportBatchId)
        {
            var result = await _oiRepositoryFactory().GetOfferImportSummary(offerImportBatchId);
            return result.ToApiResult();
        }

        public async Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId)
        {
            return await _oiRepositoryFactory().UpdateOfferImportGeneralData(item, userId);
        }

        public async Task<ApiResult<OfferImport>> UpdateOfferImportItem(OfferImportBachItemEditModel item, DateTimeOffset date, bool returnUpdatedItem = true)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var errorMessage = string.Empty;
            var returnItem = new OfferImport();
            _oiRepositoryFactory().UpdateOfferImportItem(item, userId, date, out errorMessage);

            if (!string.IsNullOrWhiteSpace(errorMessage))
                return new ApiResultBadRequest<OfferImport>(null, errorMessage);

            if (returnUpdatedItem)
            {
                try
                {
                    var itemData = await _oiRepositoryFactory().GetOfferProductFullData(item.Id);

                    var summary = await _oiRepositoryFactory().GetOfferImportSummary(itemData.OfferImportBatchId);

                    var hasErrors = false;
                    var totalErrorItems = 0;
                    var totalSuccessfullItems = 0;
                    _oiRepositoryFactory().Validate(itemData.OfferImportBatchId, null, false, out hasErrors, out totalErrorItems, out totalSuccessfullItems);

                    returnItem = new OfferImport
                    {
                        Id = summary.OfferImportBatchId,
                        SellerTotalPrice = summary.SellerTotalPrice,
                        OfferStatus = (OfferStatus?)summary.StatusId,
                        OfferStatusName = summary.StatusName,
                        TotalCost = summary.TotalCostOffer,
                        TotalUnits = summary.TotalNumberOfUnits,
                        TakeAll = summary.TakeAll,
                        TotalProducts = summary.TotalProducts,
                        TotalErrorItems = totalErrorItems,
                        TotalSuccessfulItems = totalSuccessfullItems,
                        StartDate = summary.StartDate,
                        EndDate = summary.EndDate,
                        CompanyId = summary.CompanyId,
                        CompanyFee = summary.CompanyFee,
                        Items = new List<OfferImportBachItem> { itemData }
                    };
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    return new ApiResultInternalError<OfferImport>(null, errorMessage);
                }
            }
            return returnItem.ToApiResult();
        }

        public async Task<ApiResult<OfferImport>> BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            var errorMessage = String.Empty;
            var returnItem = new OfferImport();
            _oiRepositoryFactory().BatchUpdateOfferImportItems(model, userId, DateTimeOffset.Now, out errorMessage);

            try
            {
                var summary = await _oiRepositoryFactory().GetOfferImportSummary(model.OfferImportBatchId);

                returnItem = new OfferImport
                {
                    Id = summary.OfferImportBatchId,
                    SellerTotalPrice = summary.SellerTotalPrice,
                    TotalCost = summary.TotalCostOffer,
                    TotalUnits = summary.TotalNumberOfUnits,
                    TotalProducts = summary.TotalProducts
                };
            }
            catch (Exception ex)
            {
                return new ApiResultInternalError<OfferImport>(null, ex.Message);
            }


            return returnItem.ToApiResult();
        }

        public async Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item, int userId)
        {
            return await _oiRepositoryFactory().AddUpdateOfferImportItemSize(item, userId);
        }

        public async Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId)
        {
            return await _oiRepositoryFactory().GetOfferImportItemSizes(offerImportBatchItemId);
        }

        public async Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId)
        {
            return await _oiRepositoryFactory().UpdateOfferItemsLineBuy(item, userId);
        }

        public IEnumerable<OfferImportBatchItemError> Validate(int? offerImportId, int? offerImportItemId, out bool hasErrors, out int totalErrorItems, out int totalSuccessfullItems)
        {
            return _oiRepositoryFactory().Validate(offerImportId, offerImportItemId, true, out hasErrors, out totalErrorItems, out totalSuccessfullItems);
        }

        public async Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId)
        {
            return await _oiRepositoryFactory().GetFiles(offerImportId);
        }

        public async Task<int> UpdateFile(OfferImportFile file, int userId)
        {
            return await _oiRepositoryFactory().UpdateFile(file, userId);
        }

        public ApiResult<int> InsertFile(OfferImportFile file)
        {
            var userId = _contextProviderFactory().GetLoggedInUserId();
            return _oiRepositoryFactory().InsertFiles(file, userId).ToApiResult();
        }

        public async Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item)
        {
            return await _oiRepositoryFactory().GetModelNumberByBrand(item);
        }

        public async Task<int> InsertOfferImportError(OfferImportReportError model, int userId)
        {
            return await _oiRepositoryFactory().InsertOfferImportError(model, userId);
        }

        #endregion

        #region Private Methods

        private void ProcessData(DataSet dsContent,
                               out DataTable products,
                              out DataTable sizes)
        {
            var dtGenericSizes = dsContent.Tables[0];
            var dtProducts = dsContent.Tables[1];

            var dtGenericSizesColumns = dtGenericSizes.Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower().Replace(" ", "")).ToList();
            var dtProductsColumns = dtProducts.Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower().Replace(" ", "")).ToList();

            //intersect columns between dtGenericSizes and dtProducts
            var productSizeColumns = dtGenericSizesColumns.Intersect(dtProductsColumns).ToList();

            products = GetProductBatchItems(dtProducts, productSizeColumns);

            //get dataTable for sizes
            var sizeErrors = string.Empty;
            sizes = GetSizeBatchItems(dtProducts, dtGenericSizes, productSizeColumns, out sizeErrors);
            //if (sizeErrors != string.Empty)
            //    throw new Exception($"Size Types Error: {sizeErrors}");
        }

        private DataTable GetSizeBatchItems(DataTable dtItems, DataTable dtGenericSizes, List<string> productSizeColumns, out string errorMessage)
        {
            errorMessage = string.Empty;

            var sizeErrors = new List<Tuple<int, string>>();
            var sizesList = new List<ProductSizeImportModel>();
            for (var i = 0; i < dtItems.Rows.Count; i++)
            {
                var sizeType = dtItems.Rows[i].Field<string>(OfferExcelColumnNames.SizeType);
                if (string.IsNullOrWhiteSpace(sizeType))
                    continue;

                string errorSizeColumns = string.Empty;
                foreach (var columnName in productSizeColumns)
                {
                    var quantity = dtItems.Rows[i][columnName]?.ToString();
                    if (string.IsNullOrWhiteSpace(quantity))
                        continue;

                    var sizeName = GetSizeNameFromSizeTable(dtGenericSizes, sizeType.Trim().ToLower(), columnName);
                    if (string.IsNullOrWhiteSpace(sizeName))
                        sizeErrors.Add(Tuple.Create(i + 1, columnName));
                    else
                        sizesList.Add(new ProductSizeImportModel
                        {
                            Quantity = quantity,
                            SizeName = sizeName,
                            ProductIndex = i
                        });
                }
            }
            var errorList = sizeErrors.GroupBy(x => x.Item1).Select(x => new { message = $"ProductIndex {x.Key} -Columns {string.Join(",", x.Select(y => y.Item2).ToArray())}" }).Select(x => x.message).ToArray();
            errorMessage = string.Join("; ", errorList);

            return sizesList.ConvertToDataTable();
        }

        private string GetSizeNameFromSizeTable(DataTable dtSizes, string sizeType, string columnName)
        {
            foreach (DataRow row in dtSizes.Rows)
            {
                foreach (DataColumn dc in dtSizes.Columns)
                {
                    if ((row[dc]?.ToString() ?? string.Empty).ToLower().Trim() == sizeType.ToLower().Trim())
                    {
                        return row.Field<string>(columnName);
                    }
                }
            }

            return string.Empty;
        }

        private string GetSizeName(DataTable dtSizeInfo, string columnName, string sizeType)
        {
            DataRow foundRow = dtSizeInfo.Rows.Find(sizeType);
            return foundRow != null ? foundRow.Field<string>(columnName) : null;
        }

        private DataTable GetProductBatchItems(DataTable dtItems, List<string> productSizeColumns)
        {
            return dtItems.AsEnumerable().Select((dr, index) => new OfferImportModel
            {
                ProductIndex = index,
                Brand = dr.Field<string>(OfferExcelColumnNames.Brand),
                Gender = dr.Field<string>(OfferExcelColumnNames.Gender),
                ModelNumber = dr.Field<string>(OfferExcelColumnNames.ModelNumber),
                ColorCode = dr.Field<string>(OfferExcelColumnNames.ColorCode),
                MaterialCode = dr.Field<string>(OfferExcelColumnNames.MaterialCode),
                SizeType = dr.Field<string>(OfferExcelColumnNames.SizeType),
                CurrencyName = dr.Field<string>(OfferExcelColumnNames.Currency),
                OfferCost = dr.Field<string>(OfferExcelColumnNames.OfferCost),
                Discount = dr.Field<string>(OfferExcelColumnNames.Discount),
                TotalQuantity = dr.Field<string>(OfferExcelColumnNames.TotalQuantity),
                TotalPrice = dr.Field<string>(OfferExcelColumnNames.TotalPrice),
                RetailPrice = dr.Field<string>(OfferExcelColumnNames.RetailPrice),

            }).ToList().ConvertToDataTable();
        }

        #endregion
    }
}
