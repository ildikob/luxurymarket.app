﻿using LuxuryApp.Core.Infrastructure.Api.Extensions;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models.Addresses;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;

namespace LuxuryApp.Processors.Services
{
    public class AddressService : IAddressService
    {
        private readonly Func<IAddressRepository> _addressRepositoryFactory;

        private readonly Repository _connection;

        public AddressService(Func<IAddressRepository> addressRepositoryFactory)
        {
            _connection = new Repository();
            _addressRepositoryFactory = addressRepositoryFactory;
        }

        public List<AddressInfo> GetAllByCompanyId(int companyId)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@CompanyID", companyId)
            };
            var reader = _connection.LoadData(StoredProcedureNames.AddressGetAllByCompanyID, parameters.ToArray());
            return reader.ToAddressList();
        }

        public AddressInfo GetByAddressId(int addressId)
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@AddressID", addressId)
            };
            var reader = _connection.LoadData(StoredProcedureNames.AddressGetByAddressID, parameters.ToArray());
            return reader.ToAddressList().FirstOrDefault();
        }

        public bool SetMainAddress(int addressId, int userId, DateTimeOffset currentDate)
        {
            var parameters = new SqlParameter[]
            {
               new SqlParameter("@AddressID", addressId),
               new SqlParameter("@CurrentUser", userId),
               new SqlParameter("@CurrentDate", currentDate)
            };

            return _connection.ExecuteProcedure(StoredProcedureNames.AddressSetMain, parameters.ToArray()) > 0;
        }

        public bool DeleteAddress(int addressId, int userId, DateTimeOffset currentDate)
        {
            var parameters = new SqlParameter[]
            {
               new SqlParameter("@AddressID", addressId),
               new SqlParameter("@CurrentUser", userId),
               new SqlParameter("@CurrentDate", currentDate)
            };

            return _connection.ExecuteProcedure(StoredProcedureNames.AddressDelete, parameters.ToArray()) > 0;
        }

        public ApiResult<int> SaveAddress(AddressInfo address, int userId, DateTimeOffset currentDate)
        {
            var outputAddressIdParam = new SqlParameter("@OutputAddressId", SqlDbType.Int) { Direction = ParameterDirection.Output };
            var parameters = new SqlParameter[]
            {
               new SqlParameter("@AddressID", address.AddressId),
               new SqlParameter("@Street1", address.Street1),
               new SqlParameter("@Street2", address.Street2),
               new SqlParameter("@City", address.City),
               new SqlParameter("@StateID", address.StateId > 0 ? address.StateId : SqlInt32.Null),
               new SqlParameter("@ZipCode", address.ZipCode),
               new SqlParameter("@CountryID", address.CountryId),
               new SqlParameter("@CompanyID", address.CompanyId),
               new SqlParameter("@AddressTypeID", address.AddressTypeId),
               new SqlParameter("@CurrentUser", userId),
               new SqlParameter("@CurrentDate", currentDate),
               outputAddressIdParam
            };

            var returnStatus = _connection.ExecuteProcedure(StoredProcedureNames.AddressSave, parameters.ToArray());
            return int.Parse(outputAddressIdParam.Value.ToString()).ToApiResult();
        }

        public AddressListing GetAddressesByType(List<AddressInfo> addresses)
        {
            var response = new AddressListing();
            response.BillingAddress = new List<AddressInfo>();
            response.ShippingAddress = new List<AddressInfo>();
            response.CompanyAddress = new List<AddressInfo>();
            foreach (var address in addresses)
            {
                switch (address.AddressTypeId)
                {
                    case (int)AddressType.Billing:
                        response.BillingAddress.Add(address);
                        break;
                    case (int)AddressType.Shipping:
                        response.ShippingAddress.Add(address);
                        break;
                    case (int)AddressType.Company:
                        response.CompanyAddress.Add(address);
                        break;
                }
            }
            return response;
        }

        public async Task<IEnumerable<AddressInfo>> GetCompanyMainAddresses(int companyId)
        {
            var result = await _addressRepositoryFactory().GetCompanyMainAddresses(companyId);
            return result;
        }
    }
}
