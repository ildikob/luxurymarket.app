﻿using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Readers;

namespace LuxuryApp.Processors.Services
{
    public class ColorService
    {
        private readonly Repository _connection;

        public ColorService()
        {
            _connection = new Repository();
        }

        public List<Color> Search(int? brandId=null, int? colorId = null, string colorCode = "", int? standardColorId = null)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@BrandID", brandId),
                 new SqlParameter("@ColorID", colorId),
                 new SqlParameter("@ColorCode", colorCode),
                 new SqlParameter("@StandardColorID", standardColorId)
             };
            var reader = _connection.LoadData(StoredProcedureNames.ColorsSearch, parameters.ToArray());
            return reader.ToColorList();
        }

        public List<StandardColorsModel> GetStandards()
        {
            var reader = _connection.LoadData(StoredProcedureNames.StandardColorsSearch, new List<SqlParameter>().ToArray());
            return reader.ToStandardColorList();
        }

    }
}
