﻿using LuxuryApp.Cdn.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Image.Contracts.Model
{
    public class CdnImageUploadParameter : CdnUploadParameter
    {
        public CdnImageSize[] ImageSizes { get; set; }
    }
}
