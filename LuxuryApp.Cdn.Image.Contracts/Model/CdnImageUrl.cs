﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Image.Contracts.Model
{
    public class CdnImageUrl
    {
        public string Key { get; set; }
        public string Url { get; set; }
        public CdnImageSize ImageSize { get; set; }
    }
}
