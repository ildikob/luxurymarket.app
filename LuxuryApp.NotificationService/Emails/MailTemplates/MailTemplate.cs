﻿using LuxuryApp.NotificationService.Extensions;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace LuxuryApp.NotificationService.Emails.MailTemplates
{
    public class MailTemplate
    {
        public static string GetParsedContent(string fileName, object tokens, bool hasOwnMasterTemplate, string masterTemplateName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) throw new ArgumentException("Email template name must be provided", nameof(fileName));

            string content;
            var filePath = BuildPathFor(fileName);
            var tokenList = MessageTemplate.BuildTokenValuePairs(tokens);

            try
            {
                content = MessageTemplate.ReplaceTokens(tokenList, File.ReadAllText(filePath));
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Could not read and parse file {0}, reason: {1}, stack trace: {2}",
                        filePath,
                        ex.Message,
                        ex.StackTrace));
                throw;
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                throw new MailTemplateReaderException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Email content at {0} cannot be empty.",
                        fileName));
            }

            string contentResult = hasOwnMasterTemplate ? content : GetMasterContent(content, masterTemplateName);

            return contentResult;
        }

        public static string GetParsedContentFromTemplateContent(string templateContent, object tokens, string masterTemplateName)
        {
            string content;
            var tokenList = MessageTemplate.BuildTokenValuePairs(tokens);

            try
            {
                content = MessageTemplate.GetParsedContentFromTemplateContent(templateContent, tokens);
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Could not parse template content: {0} for {1}, reason: {2}, stack trace: {3}",
                        tokenList.FirstOrDefault(t => t.Key == "MessageType").Value,
                        tokenList.FirstOrDefault(t => t.Key == "Center").Value,
                        ex.Message,
                        ex.StackTrace));
                throw;
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                throw new MailTemplateReaderException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Email content {0} for {1} cannot be empty.",
                        tokenList.FirstOrDefault(t => t.Key == "MessageType").Value,
                        tokenList.FirstOrDefault(t => t.Key == "Center").Value));
            }

            var masterContent = GetMasterContent(content, masterTemplateName);

            return masterContent;
        }

        private static string BuildPathFor(string file)
        {

            return
                Path.Combine(
                    new[]
                        {
                            AppDomain.CurrentDomain.RelativeSearchPath, //Directory.GetCurrentDirectory(),
                            "Emails\\MailTemplates",
                            string.Format(CultureInfo.InvariantCulture, "{0}.html", file)
                        });
        }

        private static string BuildPathForImage(string imageName)
        {
            return
                Path.Combine(
                    new[]
                        {
                            AppDomain.CurrentDomain.RelativeSearchPath, //Directory.GetCurrentDirectory(),
                            "Emails\\Images",
                            string.Format(CultureInfo.InvariantCulture, "{0}", imageName)
                        });
        }

        private static string GetMasterContent(string bodyContent, string masterTemplateName)
        {
            var masterFilePath = BuildPathFor(masterTemplateName);

            var masterContent = File.ReadAllText(masterFilePath);

            masterContent = masterContent.Replace("[@BODYCONTENT]", bodyContent);

            return masterContent;
        }

        public static List<string> GetMasterImages()
        {
            return new List<string>
            {
                BuildPathForImage("Logo1.png"),
                BuildPathForImage("Logo2.png")
            };
        }

        private static string GetRazorMapping(string templateContent, object model)
        {
            var t = Engine.Razor.RunCompile(templateContent, "RazorTemplate", null, model);
            return t;
        }
        public static string GetRazorParsedContent(string fileName, object model, bool hasOwnMasterTemplate, string masterTemplateName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) throw new ArgumentException("Email template name must be provided", nameof(fileName));

            string content;
            var filePath = BuildPathFor(fileName);
            var templateContent = File.ReadAllText(filePath);

            try
            {
                content = GetRazorMapping(templateContent, model);
            }
            catch (Exception ex)
            {
                Trace.TraceError(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Could not read and parse file {0}, reason: {1}, stack trace: {2}",
                        filePath,
                        ex.Message,
                        ex.StackTrace));
                throw;
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                throw new MailTemplateReaderException(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "Email content at {0} cannot be empty.",
                        fileName));
            }

            string contentResult = hasOwnMasterTemplate ? content : GetMasterContent(content, masterTemplateName);

            return contentResult;
        }

    }
}
