﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using LuxuryApp.Api;
using LuxuryApp.Api.Help;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Swashbuckle.Application;
using NLog;
using System.Configuration;
using System.Linq;
using System.Net.Http.Formatting;
using NLog.Targets;
using LuxuryApp.Core.Infrastructure.Security;
using LuxuryApp.Api.Provider;

[assembly: OwinStartup(typeof(Startup))]
namespace LuxuryApp.Api
{
    public class Startup
    {

        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static HttpConfiguration HttpConfiguration { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            HttpConfiguration = config;

            var dependencyContainer = (new DependencyBuilder()).GetDependencyContainer(config);

            config.Filters.Add(new HostAuthenticationAttribute("bearer"));
            config.Filters.Add(new System.Web.Http.AuthorizeAttribute());

            config.Formatters.Add(new BinaryMediaTypeFormatter());

            var myGlobalHeader = new SwaggerHeaderParameter
            {
                Description = "Authorization",
                Key = "Authorization",
                Name = "Authorization"
            };

            config
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Luxury app");
                    c.PrettyPrint();
                    myGlobalHeader.Apply(c);
                })
                .EnableSwaggerUi();

            app.UseAutofacMiddleware(dependencyContainer);

            app.UseAutofacWebApi(config);

            ConfigureMvc(app, dependencyContainer);

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            GlobalConfiguration.Configuration.Filters.Add(new CustomAuthorizationFilter());

            //Setup NLog
            GlobalDiagnosticsContext.Set("LogTable", ConfigurationManager.AppSettings["NLogTableName"]);
            GlobalDiagnosticsContext.Set("Environment", ConfigurationManager.AppSettings["Environment"]);
            var nlogConnectionString = ConfigurationManager.ConnectionStrings["NLogConnectionString"].ConnectionString;
            var databaseTargets = LogManager.Configuration.AllTargets.OfType<DatabaseTarget>();
            foreach (var databaseTarget in databaseTargets)
                databaseTarget.ConnectionString = nlogConnectionString;
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            var opts = new OAuthBearerAuthenticationOptions()
            {
                Provider = new QueryStringOAuthBearerProvider("token"),
            };
            app.UseOAuthBearerAuthentication(opts);

        }

        protected void ConfigureMvc(IAppBuilder app, IContainer dependencyContainer)
        {

            DependencyResolver.SetResolver(new AutofacDependencyResolver(dependencyContainer));
            app.UseAutofacMvc();

            AreaRegistration.RegisterAllAreas();

        }
    }
}