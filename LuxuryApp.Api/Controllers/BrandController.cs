﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;
using Swashbuckle.Swagger.Annotations;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/brands")]
    public class BrandController : ApiController
    {
        private BrandService _brandService;

        public BrandController()
        {
            _brandService = new BrandService();
        }


        [HttpGet]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Brand>))]
        public IHttpActionResult Get()
        {
            var entities = _brandService.Search();
            return Ok(entities);
        }


        [HttpGet]
        [Route("{brandId:int}")]
        public IHttpActionResult GetById(int brandId)
        {
            var entities = _brandService.Search(brandId: brandId);
            return Ok(entities);
        }


        [HttpGet]
        [Route("brandName/{brandName}")]
        public IHttpActionResult GetByName(string brandName)
        {
            var entities = _brandService.Search(name: brandName);
            return Ok(entities);
        }

        [HttpGet]
        [Route("parent/{parentBrandId:int}")]
        public IHttpActionResult GetByParentId(int parentBrandId)
        {
            var entities = _brandService.Search(parentId: parentBrandId);
            return Ok(entities);
        }
    }
}