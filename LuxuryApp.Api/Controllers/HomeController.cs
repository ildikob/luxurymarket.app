﻿using LuxuryApp.Contracts.Helpers;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Home;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.Infrastructure.Authorization;
using LuxuryApp.NotificationService;
using LuxuryApp.NotificationService.Configurations;
using LuxuryApp.NotificationService.Emails;
using LuxuryApp.Processors.Services;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace LuxuryApp.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/home")]
    public class HomeController : ApiController
    {
        private FeaturedEventService _featuredEventService;
        private readonly Func<IHomeService> _homeServiceFactory;
        private readonly IContextProvider _contextProvider;
        private readonly Func<INotificationHandler<EmailModel>> _emailServiceFactory;

        public HomeController(Func<IHomeService> homeServiceFactory, IContextProvider contextProvider, Func<INotificationHandler<EmailModel>> emailServiceFactory)
        {
            _featuredEventService = new FeaturedEventService();
            _contextProvider = contextProvider;
            _homeServiceFactory = homeServiceFactory;
            _emailServiceFactory = emailServiceFactory;
        }

        [Route("")]
        public IEnumerable<object> Get()
        {
            var identity = User.Identity as ClaimsIdentity;

            return identity.Claims.Select(c => new
            {
                Type = c.Type,
                Value = c.Value
            });
        }


        [HttpGet]
        [Route("get_featured_event")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<FeaturedEvent>))]
        public IHttpActionResult GetFeaturedEvents()
        {
            var entities = _featuredEventService.Get();
            return Ok(entities);
        }

        #region Customer Service

        /// <summary>
        /// Customer Service Page - Get List of Topics From DB
        /// </summary>
        /// <param name="topicId"><see cref="int"/></param>
        /// <returns>List of topics</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("get_customer_service_topics")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<CustomerServiceTopicsModel>))]
        public IHttpActionResult GetcustomerServiceTopics(int topicId = -1)
        {
            var entities = _homeServiceFactory().GetServiceTopics(topicId);
            return Ok(entities);
        }

        /// <summary>
        /// Customer Service Page - Insert a request in DB and Send email with questions
        /// </summary>
        /// <param name="model"><see cref="CustomerServiceRequestModel"/></param>
        /// <returns>request id</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("customer_service_insert_request")]
        [ResponseType(typeof(int))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(int))]
        public async Task<IHttpActionResult> CustomerServiceInsertRequestAsync(CustomerServiceRequestModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = _homeServiceFactory().InsertCustomerServiceRequest(model, _contextProvider.GetLoggedInUserId(), DateTimeOffset.Now);

            var topic = GetCustomerServiceTopicById(model.TopicID);
            var topicName = topic.Count > 0 ? topic.FirstOrDefault().Name : string.Empty;

           await _emailServiceFactory().SendAsync(new EmailModel
            {
                DataBag = new
                {
                    ContactName = model.ContactName,
                    ContactEmail = model.ContactEmail,
                    CompanyName = model.CompanyName,
                    ContactPhone = model.ContactPhoneNumber,
                    TopicName = topicName,
                    QuestionText = model.QuestionText,
                    LOGOURL = WebConfigs.LogoUrl,
                },
                From = model.ContactEmail,
                Subject = $"Customer Service #" + result.Data + " Topic: " + topicName,
                To = ConfigurationKey.EmailSendingTo,
                EmailTemplateName = "CustomerServiceRequest",
                MasterTemplateName = "MasterGrayBackground"
            });
            return Ok(result);
        }

        #endregion

        #region Private Methods
        private List<CustomerServiceTopicsModel> GetCustomerServiceTopicById(int topicId)
        {
            return _homeServiceFactory().GetServiceTopics(topicId);
        }
        #endregion
    }
}
