﻿
namespace Luxury.Integration.Tests
{
    public static class Constants
    {
        public static double CompanyFee = 1.205;

        public static decimal ShippingPercentageApparel = 17;
        public static decimal ShippingPercentageNonApparel = 21;
    }
}
