﻿using Gs1.Vics.Contracts.Models.Common;

namespace Gs1.Vics.Contracts.Models.Order
{
    public class Order: IEdiDocument
    {
        public OrderHeader OrderHeader { get; set; }
        public OrderDetail[] OrderDetails { get; set; }
    }
}