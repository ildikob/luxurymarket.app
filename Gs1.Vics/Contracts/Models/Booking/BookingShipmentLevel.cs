﻿using Gs1.Vics.Contracts.Models.Common;
using System;

namespace Gs1.Vics.Contracts.Models.Booking
{
    public class BookingShipmentLevel
    {
        public int HierarchicalIdNumber { get; set; }
        public int TotalCartons { get; set; }   //Total Cartons for all POs on Booking
        public int TotalUnits { get; set; }   //Total Cartons for all POs on Booking

        public string TransportationMethodCode => TransportationMethod.Air.Text();
        public string LocationQualifier => LocationType.Origin.Text();
        public string LocationIdentifier { get; set; }
        public string CountryCode { get; set; }

        public string PONumber { get; set; }
        public string SellerName { get; set; }
        public string SellerCompanyName { get; set; }
        public string SellerAddress1 { get; set; }
        public string SellerAddress2 { get; set; }
        public string SellerCity { get; set; }
        public string AddressTypeName { get; set; }

        public string SellerIdentificationCode { get; set; }
        public string SellerAddressInformation { get; set; }
        public string SellerCityName { get; set; }
        public string SellerStateOrProvinceCode { get; set; }
        public string SellerPostalCode { get; set; }
        public string SellerCountryCode { get; set; }
        public string SellerRegion { get; set; }

        public DateTimeOffset BookedSupplierDate { get; set; }
        public DateTimeOffset DateSellerReady { get; set; }

        public int CompanyId { get; set; }
        public string CountryIso2Code { get; set; }
        public string BuyerName { get; set; }
        public string BuyerCompanyName { get; set; }

        public BookingOrderLevel[] OrderDetails { get; set; }
    }
}