﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gs1.Vics.Contracts.Models.Common;
using LuxuryApp.Core.Infrastructure.Api.Models;

namespace Gs1.Vics.Contracts.Services
{
    public interface IEdiSerializeService<in T> where T: IEdiDocument
    {
        ApiResult<byte[]> Serialize(T document);
    }
}
