﻿using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts;
using LuxuryApp.Cdn.Image.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Drw = System.Drawing;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace LuxuryApp.CDN.ImageCdnServers.Services
{
    public class ImageCdnService : IImageCdnService
    {
        private IFileCdnService<ICdnAuthor, ICdnSettings> _fileCdnService;

        public ImageCdnService(IFileCdnService<ICdnAuthor, ICdnSettings> fileCdnService)
        {
            _fileCdnService = fileCdnService;
        }

        public CdnResult Upload(CdnImageUploadParameter uploadParameter)
        {
            var internalCdnKeys = GetInternalCdnKeys(uploadParameter.Key, uploadParameter.ImageSizes);
            var paramPreparers =
                (internalCdnKeys.Select(
                ick => Task.Factory.StartNew<CdnUploadParameter>(() =>
                {
                    return new CdnUploadParameter
                    {
                        Key = ick.InternalKey,
                        Content =
                            ResizeImage(uploadParameter.Content,
                                ick.ImageSize,
                                ImageFormat.Jpeg),
                    };
                }))).ToArray();
            Task.WaitAll(paramPreparers);
            var cdnUploadParams = paramPreparers.Select(x => x.Result).ToList();
            cdnUploadParams.Add(
                new CdnUploadParameter
                {
                    Key = GetInternalCdnKeyForOriginalFile(uploadParameter.Key),
                    Content = uploadParameter.Content,
                });
            var uploadResult = _fileCdnService.Upload(cdnUploadParams);
            return new CdnResult(uploadParameter.Key,
                uploadResult
                    .Select(u => u.Exception)
                    .FirstOrDefault());
        }

        public CdnImageUrlResult GetContentUrl(CdnImageUrlQueryParameter queryParameter)
        {
            var internalCdnKeys = GetInternalCdnKeys(queryParameter.Key, queryParameter.ImageSizes.ToArray());
            var cdnResults = _fileCdnService.GetContentUrls(internalCdnKeys.Select(i => i.InternalKey)).ToArray();
            if (cdnResults.Any(c => !c.IsSuccessful))
            {
                return new CdnImageUrlResult(queryParameter.Key,
                    new List<CdnImageUrl>(), cdnResults.Select(c => c.Exception).FirstOrDefault());
            }

            var result = new CdnImageUrlResult(queryParameter.Key,
                from c in cdnResults
                join ik in internalCdnKeys on c.Key equals ik.InternalKey
                select new CdnImageUrl
                {
                    Key = ik.PublicKey,
                    ImageSize = ik.ImageSize,
                    Url = c.Result,
                });
            return result;
        }

        public CdnResult Remove(CdnImageUrlQueryParameter queryParameter)
        {
            var internalCdnKeys = GetInternalCdnKeys(queryParameter.Key, queryParameter.ImageSizes.ToArray());
            var cdnRemoveParams = internalCdnKeys
                .Select(k => k.InternalKey).ToList();
            cdnRemoveParams.Add(GetInternalCdnKeyForOriginalFile(queryParameter.Key));
            var removeResult = _fileCdnService.Remove(cdnRemoveParams);
            return new CdnResult(queryParameter.Key,
                removeResult.Select(u => u.Exception).FirstOrDefault());
        }

        public IEnumerable<CdnResult> Upload(IEnumerable<CdnImageUploadParameter> uploadParameters)
        {
            return uploadParameters.Select(x => Upload(x));
        }

        public IEnumerable<CdnImageUrlResult> GetContentUrls(IEnumerable<CdnImageUrlQueryParameter> queryParameters)
        {
            return queryParameters.Select(x => GetContentUrl(x));
        }

        public IEnumerable<CdnResult> Remove(IEnumerable<CdnImageUrlQueryParameter> queryParameters)
        {
            return queryParameters.Select(x => Remove(x));
        }

        private InternalCdnKey[] GetInternalCdnKeys(string key, CdnImageSize[] imageSizes)
        {
            var path = Path.GetDirectoryName(key);
            var fileName = Path.GetFileName(key);
            var result =
                (from isz in imageSizes
                 select new InternalCdnKey
                 {
                     PublicKey = key,
                     InternalKey = Path.Combine(
                         path,
                         isz.SubFolder,
                         fileName)
                         .Replace('\\', '/'),
                     ImageSize = isz,
                 }).ToArray();
            return result;
        }

        private string GetInternalCdnKeyForOriginalFile(string key)
        {
            var path = Path.GetDirectoryName(key);
            var fileName = Path.GetFileName(key);
            return Path.Combine(path, "original", fileName).Replace('\\', '/');
        }

        #region image resizing
        private byte[] ResizeImage(
            byte[] originalImageContent,
            CdnImageSize size,
            ImageFormat imageFormat)
        {
            Image image;

            using (var ms = new MemoryStream(originalImageContent))
            {
                image = Drw.Image.FromStream(ms);
            }

            using (var resizedImage = ConstrainProportions(image, size.Size, size.Dimension))
            {
                byte[] result;
                using (var ms = new MemoryStream())
                {
                    resizedImage.Save(ms, imageFormat);
                    result = ms.ToArray();
                }
                image.Dispose();
                return result;
            }
        }

        private Drw.Image ConstrainProportions(Drw.Image imgPhoto, int Size, CdnImageDimension dimension)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int destWidth = 0;
            int destHeight = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            float nPercent = 0;

            switch (dimension)
            {
                case CdnImageDimension.Width:
                    nPercent = ((float)Size / (float)sourceWidth);
                    destWidth = Size;
                    destHeight = (int)(sourceHeight * nPercent);
                    break;
                case CdnImageDimension.Height:
                    nPercent = ((float)Size / (float)sourceHeight);
                    destWidth = (int)(sourceWidth * nPercent);
                    destHeight = Size;
                    break;
                default:
                    throw new NotSupportedException(dimension.ToString());
            }

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            try
            {
                using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                {
                    grPhoto.Clear(Color.White);
                    grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic; //todo: for high quality pictures throws error

                    grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX, destY, destWidth, destHeight),
                    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Pixel);
                }
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.ToString());
            }
            return bmPhoto;
        }
        #endregion
    }

    internal class InternalCdnKey
    {
        public string PublicKey { get; set; }
        public string InternalKey { get; set; }
        public CdnImageSize ImageSize { get; set; }
    }
}
