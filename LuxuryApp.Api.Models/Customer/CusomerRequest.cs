﻿using LuxuryApp.Api.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace LuxuryApp.Api.Models.Customer
{
    public class CusomerRequest : EntityBase
    {
        [Required]
        public string ContactName { get; set; }

        [Required]
        public string CompanyName { get; set; }

        public CompanyType CompanyType { get; set; }

        public Address Address { get; set; }

        [Required]
        public string RetailIdNumber { get; set; }

        [Required]
        public string CompanyWebsite { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
