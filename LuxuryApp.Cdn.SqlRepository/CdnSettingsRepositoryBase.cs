﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Cdn.CdnServers;
using LuxuryApp.Cdn.CdnServers.Repository;
using LuxuryApp.Cdn.Contracts;

namespace LuxuryApp.Cdn.SqlRepository
{
    public abstract class CdnSettingsRepositoryBase<TCdnSettings>:
        IMultiCdnSettingsRepository<TCdnSettings> 
        where TCdnSettings: ICdnSettings
    {
        protected string ConnectionString { get; set; }
        protected CdnConfigurationType ConfigurationType { get; set; }

        public CdnSettingsRepositoryBase(string connectionString, CdnConfigurationType configurationType)
        {
            ConnectionString = connectionString;
            ConfigurationType = configurationType;
        }

        public IEnumerable<int> GetCdnSettingsIds(CdnConfigurationType cdnConfigurationType)
        {
            IEnumerable<int> result;
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                result = conn.Query<GetConfigurationIdsResult>
                    (@"SELECT CdnConfigurationId,
				        CdnConfigurationTypeId FROM cdn.GetConfigurationIds(@cdn_configuration_type_id)",
                    new { cdn_configuration_type_id = (int)cdnConfigurationType })
                    .Select(x => x.CdnConfigurationId);
                conn.Close();
            }
            return result;
        }

        private class GetConfigurationIdsResult
        {
            public int CdnConfigurationId { get; set; }
            public int CdnConfigurationTypeId { get; set; }
        }

        public abstract int SaveCdnSettings(TCdnSettings settings);

        public abstract TCdnSettings GetCdnSettings(int configurationId);

        public IEnumerable<TCdnSettings> GetAllCdnSettings()
        {
            return GetCdnSettingsIds(ConfigurationType).Select(x => GetCdnSettings(x));
        }
    }
}
