﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Contracts.Enums;
using LuxuryApp.DataAccess;
using LuxuryApp.Core.DataAccess.DataReaderExtensions;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class OfferImportRepository : RepositoryBase, IOfferImportRepository
    {
        public OfferImportRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public OfferImport InsertBatchItems(string fileName, DataTable products, DataTable productSizes, out int offerImportBatchId, int companyId, int userId)
        {
            var createdDate = DateTimeOffset.Now;

            var parameters = new DynamicParameters(
              new
              {
                  fileName = fileName,
                  CreatedBy = userId,
                  CompanyID = companyId,
                  CreatedDate = createdDate,
                  StatusID = OfferStatus.InProgress,
                  OfferProducts = products,
                  OfferProductSizes = productSizes,
                  ReturnInsertedList = true
              });

            OfferImport offerImportModel = null;
            offerImportBatchId = 0;

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                try
                {
                    var reader = conn.ExecuteReader(StoredProcedureNames.OfferImport_Insert,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure);
                    var dataReader = new DataReader { Connection = conn, Reader = reader };

                    if (dataReader == null) return null;

                    var resultItems = dataReader.ToOfferImportList(ref offerImportBatchId);

                    offerImportModel = new OfferImport
                    {
                        TotalCost = resultItems.Sum(x => x.OfferTotalPrice ?? 0),
                        TotalUnits = resultItems.Sum(x => x.ActualTotalUnits ?? 0),
                        FileName = fileName,
                        Id = offerImportBatchId,
                        Items = resultItems,
                        CreatedDate = createdDate,
                        OfferStatus = OfferStatus.InProgress,
                        TotalErrorItems = resultItems.Where(x => x.HasErrors).Count(),
                        TotalSuccessfulItems = resultItems.Where(x => !x.HasErrors).Count()
                    };

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return offerImportModel;
        }

        public async Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
           new
           {
               OfferImportBatchID = item.Id,
               StartDate = item.StartDate,
               EndDate = item.EndDate,
               StatusID = item.OfferStatus,
               TakeAll = item.TakeAll,
               Active = item.Active,
               Deleted = item.Deleted,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatch_Update,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public async Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId)
        {
            OfferImportBachItem returnItem = null;

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportBatchItems_GetItemFullData,
                                                new { offerImportBatchItemId },
                                                 commandType: CommandType.StoredProcedure);
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItem = dataReader.ToOfferImportItemFullData().FirstOrDefault();

                conn.Close();
            }
            return returnItem;
        }

        public void UpdateOfferImportItem(OfferImportBachItemEditModel item, int userId, DateTimeOffset datetime, out string errorMessage)
        {
            errorMessage = string.Empty;
            var parameters = new DynamicParameters(
               new
               {
                   OfferImportBatchItemID = item.Id,
                   BrandID = item.BrandId,
                   ModelNumber = item.ModelNumber,
                   ColorID = item.ColorId,
                   MaterialID = item.MaterialId,
                   Active = item.Active,
                   BusinessID = item.BusinessId,
                   OfferCost = item.OfferCost,
                   SizeTypeID = item.SizeTypeId,
                   CurrencyID = item.Currency > 0 ? (int?)item.Currency : null,
                   LineBuy = item.LineBuy,
                   ModifiedBy = userId,
                   ModifiedDate = datetime
               });

            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();
                    var reader = conn.ExecuteReader(StoredProcedureNames.OfferImportBatchItems_Update,
                                                         param: parameters,
                                                         commandType: CommandType.StoredProcedure);

                }
                catch (Exception ex)
                {
                    errorMessage = string.IsNullOrWhiteSpace(ex.Message) ? "DBError" : ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
        }

        public void BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model, int userId, DateTimeOffset datetime, out string errorMessage)
        {
            errorMessage = String.Empty;

            var parameters = new DynamicParameters(
              new
              {
                  Active = model.Active,
                  OfferImportBatchItemIDs = ListToDataTable(model.OfferImportBatchItemIds)
              });

            using (var conn = GetReadWriteConnection())
            {
                try
                {
                    conn.Open();
                    var reader = conn.Execute(StoredProcedureNames.OfferImportBatchItems_BatchUpdate, parameters, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    errorMessage = string.IsNullOrWhiteSpace(ex.Message) ? "DBError" : ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public async Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item,
                                                            int userId)
        {
            var totalQuantity = 0;

            var p = new DynamicParameters();
            p.Add("TotalQuantity", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("OfferImportBatchItemID", item.OfferImportBatchItemId);
            p.Add("SizeId", item.SizeId);
            p.Add("Quantity", item.Quantity);
            p.Add("ModifiedBy", userId);
            p.Add("ModifiedDate", DateTimeOffset.Now);
            p.Add("Deleted", item.Deleted);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatchSizeItems_Update,
                                                   param: p,
                                                   commandType: CommandType.StoredProcedure
                                                   );
                totalQuantity = p.Get<int>("TotalQuantity");
                conn.Close();
            }

            return totalQuantity;
        }

        public async Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId)
        {
            IEnumerable<OfferImportBatchItemSize> returnItems = null;
            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportBatchSizeItems_GetByItemID,
                                                   param: new { offerImportBatchItemId },
                                                   commandType: CommandType.StoredProcedure
                                                   );
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToOfferImportItemSizeList();

                conn.Close();
            }

            return returnItems;
        }

        public async Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId)
        {
            var returnStatus = -1;
            var ids = item.Ids == null || item.Ids.Length == 0 ? string.Empty : string.Join(",", item.Ids);
            var parameters = new DynamicParameters(
           new
           {
               OfferImportBatchID = item.OfferImportBatchId,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now,
               LineBuy = item.LineBuy,
               Ids = ids
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportBatchUpdateLineBuy,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public IEnumerable<OfferImportBatchItemError> Validate(int? offerImportId, int? offerImportItemId, bool returnErrorList, out bool hasErrors, out int totalErrorItems, out int totalSuccessfullItems)
        {
            hasErrors = false;
            totalErrorItems = 0;
            totalSuccessfullItems = 0;

            IEnumerable<OfferImportBatchItemError> returnItems = null;
            if (offerImportId == null && offerImportItemId == null) return returnItems;

            var p = new DynamicParameters();
            p.Add("HasErrors", dbType: DbType.Boolean, direction: ParameterDirection.Output);
            p.Add("OfferImportBatchID", offerImportId);
            p.Add("OfferImportBatchItemID", offerImportItemId);
            p.Add("TotalErrorItems", totalErrorItems, direction: ParameterDirection.Output);
            p.Add("TotalSuccessfullItems", totalSuccessfullItems, direction: ParameterDirection.Output);
            p.Add("ReturnErrorList", returnErrorList);

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = conn.ExecuteReader(StoredProcedureNames.OfferImportBatchItem_Validation,
                                                    p,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToOfferImportBatchItemErrorList();

                if (!returnErrorList)
                {
                    hasErrors = p.Get<bool>("HasErrors");
                    totalErrorItems = p.Get<int>("TotalErrorItems");
                    totalSuccessfullItems = p.Get<int>("TotalSuccessfullItems");
                }
                conn.Close();
            }

            return returnItems;

        }

        public async Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId)
        {
            IEnumerable<OfferImportFile> returnItems = null;
            var p = new DynamicParameters();
            p.Add("OfferImportBatchID", offerImportId);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.OfferImportFiles_GetByBatchID,
                                                    p,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToOfferImportFileList();

                conn.Close();
            }

            return returnItems;
        }

        public async Task<int> UpdateFile(OfferImportFile file, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
           new
           {
               OfferImportFileID = file.OfferImportFileId,
               OriginalName = file.OriginalName,
               Deleted = file.Deleted,
               ModifiedBy = userId,
               ModifiedDate = DateTimeOffset.Now
           });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportFiles_Update,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnStatus;
        }

        public int InsertFiles(OfferImportFile file, int userId)
        {
            var returnStatus = -1;
            var p = new DynamicParameters();
            p.Add("OfferImportBatchID", file.OfferImportBatchId);
            p.Add("OriginalName", file.OriginalName);
            p.Add("FileName", file.FileName);
            p.Add("CreatedBy", userId);
            p.Add("CreatedDate", DateTimeOffset.Now);
            p.Add("ID", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = conn.Execute(StoredProcedureNames.OfferImportFiles_Insert,
                                                    param: p,
                                                    commandType: CommandType.StoredProcedure
                                                    );

                returnStatus = p.Get<int>("ID");
                conn.Close();
            }
            return returnStatus;
        }

        public async Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item)
        {
            var returnItems = new List<string>();

            var parameters = new DynamicParameters(
            new
            {
                BrandID = item.BrandId,
                ColorID = item.ColorId,
                MaterialID = item.MaterialId,
                BusinessID = item.BusinessId,
                SizeTypeID = item.SizeTypeId
            });

            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                var reader = await conn.ExecuteReaderAsync(StoredProcedureNames.Products_GetModelNumberByBrandID,
                                                param: parameters,
                                                 commandType: CommandType.StoredProcedure);
                var dataReader = new DataReader { Connection = conn, Reader = reader };

                if (dataReader != null)
                    returnItems = dataReader.ToModelNumberList();

                conn.Close();
            }
            return returnItems;
        }

        public async Task<int> InsertOfferImportError(OfferImportReportError model, int userId)
        {
            var returnStatus = -1;

            var parameters = new DynamicParameters(
            new
            {
                OfferImportBatchID = model.OfferImportBatchId,
                OfferImportBatchItemID = model.OfferImportBatchItemId,
                BrandID = model.BrandId,
                ProductID = model.ProductId,
                ErrorDescription = model.ErrorDescription,
                CreatedBy = userId,
                CreatedDate = DateTimeOffset.Now
            });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnStatus = await conn.ExecuteAsync(StoredProcedureNames.OfferImportReportedErrors_Insert,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }
            return returnStatus;
        }

        public async Task<OfferImportSummary> GetOfferImportSummary(int offerImportBatchId)
        {
            var sql = @"SELECT OfferImportBatchID as Id,
                               CompanyID,
                               CompanyFee,
                               StartDate,
                               EndDate,
                               TakeAll,
                               StatusID,
                               StatusName,
                               TotalUnits as TotalNumberOfUnits,
                               TotalProducts,
                               TotalCost as TotalCostOffer,
                               SellerTotalPrice,
                               CurrencyID";
            sql = $"{sql} from  {SqlFunctionName.fn_OfferImport_GetSummary}(@OfferImportBatchID)";

            IEnumerable<OfferImportSummary> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<OfferImportSummary>(sql, new { offerImportBatchId });
                conn.Close();
            }
            return result.FirstOrDefault();
        }

        private DataTable ListToDataTable(List<int> ids)
        {
            DataTable idsDataTable = null;
            if (ids != null)
            {
                idsDataTable = new DataTable();
                idsDataTable.Columns.Add("ID", typeof(int));
                foreach (var prodId in ids)
                {
                    idsDataTable.Rows.Add(prodId);
                }
            }

            return idsDataTable;
        }
    }
}

