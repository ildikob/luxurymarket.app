﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Models.Ftp;
using LuxuryApp.Contracts.Repository;
using System.Collections.Generic;
using LuxuryApp.Utils.Helpers;
using LuxuryApp.DataAccess;
using System.Threading.Tasks;

namespace LuxuryApp.Core.DataAccess.Repository
{
    public class VendorImageRepository : RepositoryBase,
        IVendorImageRepository
    {
        public VendorImageRepository(IConnectionStringProvider connectionStringProvider) : base(connectionStringProvider)
        {
        }

        public async Task<IEnumerable<VendorImageItem>> GetUnprocessedImagesAsync()
        {
            var sql = @"SELECT Id,
                               URL,
                               ProductID,
                               SellerSKU";
            sql = $"{sql} from  {SqlFunctionName.fn_ProductSyncBatchItemImagesSelect}()";

            IEnumerable<VendorImageItem> result;
            using (var conn = GetReadOnlyConnection())
            {
                conn.Open();
                result = await conn.QueryAsync<VendorImageItem>(sql);
                conn.Close();
                return result;
            }
        }

        public async Task<int> UpdateProcessedImageAsync(VendorImageItem item, int createdBy)
        {
            var returnIdentity = 0;
            var parameters = new DynamicParameters(
              new
              {
                  ID = item.Id,
                  ProductID = item.ProductId,
                  FileName = item.FileName,
                  CreatedBy = createdBy
              });

            using (var conn = GetReadWriteConnection())
            {
                conn.Open();

                returnIdentity = await conn.ExecuteAsync(StoredProcedureNames.VendorProductImagesInsert,
                                                    param: parameters,
                                                    commandType: CommandType.StoredProcedure
                                                    );
                conn.Close();
            }

            return returnIdentity;
        }

    }
}
