﻿namespace LuxuryApp.Core.DataAccess.Models
{
    public class OfferImportBatchItemDenormalized
    {
        public int OfferImportBatchItemId { get; set; }

        public string SKU { get; set; }

        public string BrandName { get; set; }

        public string BusinessName { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public string SizeTypeName { get; set; }

        public string OfferCost { get; set; }

        public string RetailPrice { get; set; }

        public string Discount { get; set; }

        public string TotalQuantity { get; set; }

        public string TotalPrice { get; set; }

        public int OfferImportBatchSizeItemId { get; set; }

        public string SizeName { get; set; }

        public int? SizeId { get; set; }

        public string Quantity { get; set; }

        public int? QuantityValue { get; set; }

        public int? BrandId { get; set; }

        public int? BusinessId { get; set; }

        public int? ProductId { get; set; }

        public int? HierarchyId { get; set; }

        public int CurrencyId { get; set; }

        public int? ColorId { get; set; }

        public int? MaterialId { get; set; }

        public int? SizeTypeId { get; set; }

        public double? OfferCostValue { get; set; }

        public double? ProductRetailPrice { get; set; }

        public double? CustomerDiscount { get; set; }

        public int? ActualTotalUnits { get; set; }

        public double? CustomerUnitPrice { get; set; }

        public double? CustomerTotalPrice { get; set; }

        public bool LineBuy { get; set; }

        public bool Active { get; set; }

        public string CategoryName { get; set; }

        public string ProductName { get; set; }

        public string MainImageName { get; set; }

        public string OriginName { get; set; }

        public string SeasonCode { get; set; }

        public string Dimensions { get; set; }

        public string ProductDescription { get; set; }

        public string Notes { get; set; }

        public string Images { get; set; }

    }
}
