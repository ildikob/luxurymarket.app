using System;
using System.Collections.Generic;
using System.Data;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Models.Offer;

namespace LuxuryApp.Core.DataAccess
{
    public class DataReader
    {
        #region Class Data
        private IDataReader _reader;
        private IDbConnection _connection;
        private List<string> _schema;

        #endregion

        #region Public Methods

        /// <summary>
        /// used for closing a reader
        /// </summary>
        public void Close()
        {
            if (_reader != null && !_reader.IsClosed)
                _reader.Close();

            if (_connection != null && _connection.State == ConnectionState.Open)
                _connection.Close();
        }

        /// <summary>
        /// used for reading a reder
        /// </summary>
        /// <returns>true if the reader is not empty and can be open</returns>
        public bool Read()
        {
            return _reader.Read();
        }

        /// <summary>
        /// return true if the reader succeded to move to the next result set
        /// </summary>
        /// <returns></returns>
        public bool NextResult()
        {
            bool hasNext = _reader.NextResult();

            if (_schema == null)
                _schema = new List<string>();
            else
                _schema.Clear();

            for (int i = 0; i < _reader.FieldCount; i++)
                _schema.Add(_reader.GetName(i));

            return hasNext;
        }

        #endregion

        #region Get Methods

        public int GetInt(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return -1;

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null)
                return -1;

            int result = -1;
            int.TryParse(obj.ToString(), out result);
            return result;
        }

        public long GetLong(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return -1;

            object obj = _reader[columnIndex];
            if (obj != DBNull.Value)
            {
                long result = -1;
                if (long.TryParse(obj.ToString(), out result))
                {
                    return result;
                }
            }

            return -1;
        }

        public string GetString(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return string.Empty;
            else
            {
                object obj = _reader[columnIndex];
                if (obj != DBNull.Value)
                {
                    return obj.ToString();
                }
            }

            return string.Empty;
        }

        public char GetChar(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return ' ';

            object obj = _reader[columnIndex];
            if (obj != DBNull.Value)
            {
                char val = ' ';
                if (char.TryParse(obj.ToString(), out val))
                {
                    return val;
                }
            }

            return ' ';
        }

        public double GetDouble(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return -1;

            object obj = _reader[columnIndex];

            double result = -1;
            double.TryParse(obj.ToString(), out result);
            return result;
        }

        public decimal GetDecimal(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return -1;

            object obj = _reader[columnIndex];

            decimal result = -1;
            decimal.TryParse(obj.ToString(), out result);
            return result;
        }

        public bool GetBool(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return false;

            object obj = _reader[columnIndex];

            bool result = false;
            bool.TryParse(obj.ToString(), out result);
            return result;
        }

        public bool? GetBoolNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return false;

            bool val = false;
            object obj = _reader[columnIndex];

            if (obj != DBNull.Value && bool.TryParse(obj.ToString(), out val))
            {
                return val;
            }
            else
            {
                return null;
            }
        }

        public char? GetCharNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
                return null;

            object obj = _reader[columnIndex];
            char val = ' ';
            if (obj != DBNull.Value && char.TryParse(obj.ToString(), out val))
            {
                return val;
            }
            else
            {
                return null;
            }
        }

        public DateTime GetDateTime(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return DateTime.MinValue;
            }

            DateTime result = DateTime.Now;
            DateTime.TryParse(_reader[columnIndex].ToString(), out result);

            return result;
        }

        public DateTime? GetDateTimeNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            DateTime resPart = DateTime.MinValue;
            DateTime.TryParse(obj.ToString(), out resPart);

            if (resPart == DateTime.MinValue)
                return null;
            else
            {
                return resPart;
            }
        }


        public DateTimeOffset GetDateTimeOffset(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return DateTime.MinValue;
            }

            DateTimeOffset result = DateTimeOffset.Now;
            DateTimeOffset.TryParse(_reader[columnIndex].ToString(), out result);

            return result;
        }

        public DateTimeOffset? GetDateTimeOffsetNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            DateTimeOffset resPart = DateTimeOffset.MinValue;
            DateTimeOffset.TryParse(obj.ToString(), out resPart);

            if (resPart == DateTimeOffset.MinValue)
                return null;
            else
            {
                return resPart;
            }
        }

        public int? GetIntNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            int result = -1;

            int.TryParse(obj.ToString(), out result);

            return result;
        }

        public long? GetLongNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            long result = -1;
            if (long.TryParse(obj.ToString(), out result))
            {
                return result;
            }

            return null;
        }

        public decimal? GetDecimalNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            decimal result = -1;

            decimal.TryParse(obj.ToString(), out result);

            return result;
        }

        public double? GetDoubleNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            double result = -1;

            double.TryParse(obj.ToString(), out result);

            return result;
        }

        public byte GetByte(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return 0;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return 0;

            byte result = 0;
            byte.TryParse(obj.ToString(), out result);

            return result;
        }

        public byte? GetByteNullable(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            byte result = 0;
            byte.TryParse(obj.ToString(), out result);

            return result;
        }

        public byte[] GetBytes(string column)
        {
            int columnIndex = ColumnIndex(column);
            if (columnIndex < 0)
            {
                return null;
            }

            object obj = _reader[columnIndex];
            if (obj == DBNull.Value || obj == null || obj.ToString().Equals(""))
                return null;

            byte[] result = null;
            result = (byte[])obj;

            return result;
        }
        #endregion

        #region Public Properties

        public IDataReader Reader
        {
            get { return _reader; }
            set
            {
                _reader = value;

                if (_schema == null)
                    _schema = new List<string>();
                else
                    _schema.Clear();

                for (int i = 0; i < _reader.FieldCount; i++)
                    _schema.Add(_reader.GetName(i));
            }
        }
        public IDbConnection Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// returns true if the current schema containg the given column
        /// </summary>
        /// <param name="column">column name</param>
        /// <returns>true/false</returns>
        private bool HasColumn(string column)
        {
            return _schema.Contains(column);
        }

        /// <summary>
        /// returns true if the current schema containg the given column
        /// </summary>
        /// <param name="column">column name</param>
        /// <returns>true/false</returns>
        private int ColumnIndex(string column)
        {
            return _schema.IndexOf(column);
        }

        #endregion
    }
}
