﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models;
using System.Data;

namespace LuxuryApp.Contracts.Repository
{
    public interface IOfferImportRepository
    {
        OfferImport InsertBatchItems(string fileName,
                                     DataTable products,
                                     DataTable productSizes,
                                     out int offerImportBatchId,
                                     int companyId,
                                     int userId);

        Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId);

        Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId);

        void UpdateOfferImportItem(OfferImportBachItemEditModel item, int userId, DateTimeOffset datetime, out string errorMessage);

        void BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model, int userId, DateTimeOffset datetime, out string errorMessage);

        Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item, int userId);

        Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId);

        Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId);

        IEnumerable<OfferImportBatchItemError> Validate(int? offerImportId, int? offerImportItemId, bool returnErrorList, out bool hasErrors, out int totalErrorItems, out int totalSuccessfullItems);

        Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId);

        Task<int> UpdateFile(OfferImportFile file, int userId);

        int InsertFiles(OfferImportFile file, int userId);

        Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item);

        Task<int> InsertOfferImportError(OfferImportReportError model, int userId);

        Task<OfferImportSummary> GetOfferImportSummary(int offerImportBatchId);
    }
}
