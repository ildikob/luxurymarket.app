﻿using LuxuryApp.Contracts.Models.Ftp;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Repository
{
    public interface IFtpRepository
    {
        FtpConfig[] GetAllFtpConfigs();

        int EnqueueItem(FtpQueueItem item);
        FtpQueueItem[] GetUploadableItems();

        void SetItemUploadStatus(int ftpQueueItemId, FtpUploadStatus status, string error);
    }
}