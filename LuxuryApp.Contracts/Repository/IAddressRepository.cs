﻿using LuxuryApp.Contracts.Models.Addresses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Repository
{
    public interface IAddressRepository
    {
        Task<IEnumerable<AddressInfo>> GetCompanyMainAddresses(int companyId);
    }
}
