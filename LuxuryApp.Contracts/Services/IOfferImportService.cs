﻿
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// OfferImportService interface
    /// </summary>
    public interface IOfferImportService
    {
        /// <summary>
        /// Upload excel file with offer template
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="file"></param>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <param name="createdDate"></param>
        /// <returns></returns>
        ApiResult<OfferImport> UploadFile(string fileName, Stream file, int? companyId, int userId);

        Task<OfferImportBachItem> GetOfferProductFullData(int offerImportBatchItemId);

        Task<ApiResult<OfferImportSummary>> GetOfferImportSummary(int offerImportBatchId);

        Task<int> UpdateOfferImportGeneralData(OfferImport item, int userId);

        Task<ApiResult<OfferImport>> UpdateOfferImportItem(OfferImportBachItemEditModel item, DateTimeOffset date, bool returnUpdatedItem = true);

        Task<ApiResult<OfferImport>> BatchUpdateOfferImportItems(OfferItemsBatchUpdateModel model);

        Task<int> AddUpdateOfferImportItemSize(OfferImportBachItemSizeModel item, int userId);

        /// <summary>
        /// Get sizes for an offer import item 
        /// </summary>
        /// <param name="offerImportBatchItemId"></param>
        /// <returns></returns>
        Task<IEnumerable<OfferImportBatchItemSize>> GetOfferImportItemSizes(int offerImportBatchItemId);

        Task<int> UpdateOfferItemsLineBuy(OfferImportBatchEditLineBuy item, int userId);

        IEnumerable<OfferImportBatchItemError> Validate(int? offerImportId, int? offerImportItemId, out bool hasErrors, out int totalErrorItems, out int totalSuccessfullItems);

        Task<IEnumerable<OfferImportFile>> GetFiles(int offerImportId);

        Task<int> UpdateFile(OfferImportFile file, int userId);

        ApiResult<int> InsertFile(OfferImportFile file);

        Task<IEnumerable<string>> GetModelNumberByBrand(ProductSearchMappingCriteria item);

        Task<int> InsertOfferImportError(OfferImportReportError model, int userId);


    }
}
