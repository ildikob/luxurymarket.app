﻿
using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.Infrastructure.Api.Models;
using LuxuryApp.Contracts.Models.Offer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Services
{
    /// <summary>
    /// OfferImportService interface
    /// </summary>
    public interface IOfferService
    {
        OfferSearchResponse Search(OfferSearch model);

        Task<IEnumerable<OfferStatusResponse>> GetStatuses();

        OfferLiveSummary GetLiveSummary(OfferLiveSummaryRequest model);

        Task<ApiResult<bool>> IsActiveOffer(int offerId);
    }
}
