﻿using LuxuryApp.Contracts.Models.Home;
using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Services
{
    public interface IHomeService
    {
        List<CustomerServiceTopicsModel> GetServiceTopics(int topicId);

        ApiResult<int> InsertCustomerServiceRequest(CustomerServiceRequestModel model, int userId, DateTimeOffset date);
    }
}
