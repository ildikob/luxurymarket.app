﻿
namespace LuxuryApp.Contracts.Enums
{
    public enum OrderCustomerType
    {
        None = 0,
        Seller = 1,
        Buyer = 2
    }
}
