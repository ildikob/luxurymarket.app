﻿
namespace LuxuryApp.Contracts.Enums
{
    /// <summary>
    /// Enum with the Company Type values
    /// </summary>
    public enum CompanyType
    {
        None,

        Buyer = 1,

        Seller = 2,

        Both = 3
    }
}
