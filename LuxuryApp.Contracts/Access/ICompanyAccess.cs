﻿namespace LuxuryApp.Contracts.Access
{
    public interface ICompanyAccess
    {
        bool CanViewCartForCompany(int companyId);
        bool CanAddToCartForCompany(int companyId);
        bool CanUserSubmitCart(int cartId);
    }
}
