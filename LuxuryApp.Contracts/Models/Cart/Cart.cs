﻿namespace LuxuryApp.Contracts.Models.Cart
{
    public class Cart
    {
        public int Id { get; set; }

        public int BuyerCompanyId { get; set; }

        public CartItem[] Items { get; set; }

        public int ItemsCount { get; set; }
        public int UnitsCount { get; set; }
        public decimal TotalCustomerCost { get; set; }
        public Currency Currency { get; set; }
        public int CartReservedTime { get; set; }
        public Cart()
        {
            Items = new CartItem[0];
        }
    }
}
