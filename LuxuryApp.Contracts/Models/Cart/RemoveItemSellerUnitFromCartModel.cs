﻿namespace LuxuryApp.Contracts.Models.Cart
{
    public class RemoveItemSellerUnitFromCartModel
    {
        public int BuyerCompanyId { get; set; }
        public CartItemSellerUnitIdentifier CartItemSellerUnitIdentifier { get; set; }
    }
}