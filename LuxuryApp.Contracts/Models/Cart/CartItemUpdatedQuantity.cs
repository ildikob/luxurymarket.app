﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models.Cart
{
    public class CartItemUpdatedQuantity
    {
        public int CartItemId { get; set; }

        public int OldQuantity { get; set; }

        public int UpdatedQuantity { get; set; }

        public CartReservedStatusType ReservedStatusType
        {
            get
            {
                return (OldQuantity == UpdatedQuantity) ? CartReservedStatusType.Success :
                       (OldQuantity > UpdatedQuantity && UpdatedQuantity > 0) ? CartReservedStatusType.PartialReserved :
                       (OldQuantity > UpdatedQuantity && UpdatedQuantity == 0) ? CartReservedStatusType.Failed :
                       CartReservedStatusType.None;
            }
        }
    }
}
