﻿namespace LuxuryApp.Contracts.Models.Home
{
    public class CustomerServiceTopicsModel
    {
        public int TopicID { get; set; }

        public string Name { get; set; }
    }
}
