﻿namespace LuxuryApp.Contracts.Models
{
    public class Standard
    {
        public int StandardId { get; set; }
        public string Name { get; set; }
    }

    public class StandardColorsModel : Standard
    {
        public string RGB { get; set; }
    }

    public class StandardBrandModel : Standard
    {
    }

    public class StandardMaterialModel : Standard
    {
    }

    public class StandardOriginModel : Standard
    {
    }

    public class StandardSeasonModel : Standard
    {
    }
}
