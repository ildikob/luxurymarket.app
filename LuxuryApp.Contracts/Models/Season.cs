﻿namespace LuxuryApp.Contracts.Models
{
    public class Season : Attribute
    {
        public int? Year { get; set; }

        public string Name { get; set; }

        public StandardSeasonModel StandardSeason { get; set; }
    }
}
