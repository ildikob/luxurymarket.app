﻿namespace LuxuryApp.Contracts.Models.Ftp
{
    public enum FtpUploadStatus
    {
        Queued = 1,
        Uploading = 2,
        Uploaded = 3,
        Failed = 4,
    }
}