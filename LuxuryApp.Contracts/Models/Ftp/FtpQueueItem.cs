﻿namespace LuxuryApp.Contracts.Models.Ftp
{
    public class FtpQueueItem
    {
        public int Id { get; set; }
        public int FtpConfigId { get; set; }
        public int OrderSellerId { get; set; }
        public string PONumber { get; set; }
        public string FileName { get; set; }
        public byte[] FileContentBytes { get; set; }
    }
}