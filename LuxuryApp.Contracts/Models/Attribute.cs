﻿namespace LuxuryApp.Contracts.Models
{
    public class Attribute
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public int? DisplayOrder { get; set; }
    }
}
