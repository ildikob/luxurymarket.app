﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models.Shipping
{
    public class ShippingSummary
    {
        public int CartId { get; set; }
        public int ShipmentsCount { get; set; }
        public decimal TotalShippingCost { get; set; }
        public Currency Currency { get; set; }
    }
}
