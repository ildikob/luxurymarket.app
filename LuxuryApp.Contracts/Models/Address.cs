﻿using System.ComponentModel.DataAnnotations;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class Address
    {
        public string Id { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Street1 { get; set; }

        [Required]
        public string Street2 { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required]
        public Country Country { get; set; }

        [Required]
        public State State { get; set; }

        public AddressType AddressType { get; set; }
    }
}
