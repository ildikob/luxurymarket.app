﻿namespace LuxuryApp.Contracts.Models
{
    public class SizeType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
