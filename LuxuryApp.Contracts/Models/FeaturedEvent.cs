﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class FeaturedEvent
    {
        public int FeaturedEventID { get; set; }

        public string Title { get; set; }

        public string Subtitle { get; set; }

        public bool IsHero { get; set; }

        public int? DisplayOrder { get; set; }

        public bool HasProductsAssigned { get; set; }

        public List<FeaturedEventImage> Images { get; set; }
    }

    public class FeaturedEventImage
    {
        public FeaturedEventImageTypes Type { get; set; }

        public string ImageName { get; set; }

        public string ImageLink
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLinkFeaturedEvent"] + ImageName;
            }
        }

        public string ImageTitle { get; set; }

        public string ImageSubtitle { get; set; }

        public FeaturedEventThemeType FeaturedEventThemeType { get; set; }
    }

    public class FeaturedEventGetModel
    {
        public int FeaturedEventID { get; set; }

        public string Title { get; set; }

        public string Subtitle { get; set; }

        public bool IsHero { get; set; }

        public int? DisplayOrder { get; set; }

        public string ImageName { get; set; }

        public string ImageTitle { get; set; }

        public string ImageSubtitle { get; set; }

        public int TypeID { get; set; }

        public int FeaturedEventThemeTypeID { get; set; }

        public bool HasProductsAssigned { get; set; }
    }

    public enum FeaturedEventImageTypes
    {
        Hero_Image = 1,
        Small_Tiles = 2,
        Big_Tiles = 3
    }

    public enum FeaturedEventThemeType
    {
        Light = 1,
        Dark = 2
    }
}
