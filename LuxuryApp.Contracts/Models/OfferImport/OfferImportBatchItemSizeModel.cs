﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemSizeModel
    {
        public int? SizeId { get; set; }

        public int Quantity { get; set; }
    }
}
