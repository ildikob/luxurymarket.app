﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferImportReportError
    {
        public int OfferImportBatchId { get; set; }

        public int? OfferImportBatchItemId { get; set; }

        public int? ProductId { get; set; }

        public int? BrandId { get; set; }

        public string ErrorDescription { get; set; }
    }
}
