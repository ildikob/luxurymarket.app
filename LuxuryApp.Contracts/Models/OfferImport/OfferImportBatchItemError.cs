﻿
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemError
    {
        public int OfferImportBatchItemId { get; set; }

        public int? OfferImportBatchSizeItemID { get; set; }

        public string ColumnName { get; set; }

        public string ColumnValue { get; set; }

        public string ErrorType { get; set; }

        public string Message { get; set; }
    }
}
