﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchEditLineBuy
    {
        public int OfferImportBatchId { get; set; }

        public bool LineBuy { get; set; }

        public int[] Ids { get; set; }
    }
}
