﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemSize
    {
        public int OfferImportBatchSizeItemId { get; set; }

        public int OfferImportBatchItemId { get; set; }

        public string SizeName { get; set; }

        public int? SizeId { get; set; }

        public string Quantity { get; set; }

        public int? QuantityValue { get; set; }
    }


}
