﻿using LuxuryApp.Contracts.Enums;
using System.Configuration;

namespace LuxuryApp.Contracts.Models
{
    public class EntityImage
    {
        public int Id { get; set; }

        public int EntityId { get; set; }

        public string Name { get; set; }

        public string Link
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink500"] + Name;
            }
        }

        public int DisplayOrder { get; set; }

        public EntityType EntityType { get; set; }
    }
}
