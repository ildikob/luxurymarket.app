﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }

        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Notes { get; set; }

        public string UserName { get; set; }

        public IEnumerable<Company> Companies { get; set; }

        public string FullName
        {
            get
            {
                var name = $"{FirstName ?? string.Empty} {LastName ?? string.Empty}";
                return name.Trim();
            }
        }
    }

    public class CustomerCompanies
    {
        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public IEnumerable<Company> Companies { get; set; }
    }

    public class AccountInfo
    {
        public int CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactPhoneNumber { get; set; }
    }

    public class CustomerDenormalized
    {
        public int CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public CompanyType CompanyType { get; set; }
    }

}
