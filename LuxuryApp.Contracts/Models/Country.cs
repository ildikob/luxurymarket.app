﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class Country
    {
        public int Id { get; set; }

        public string CountryName { get; set; }

        public string ISO2Code { get; set; }

        public List<State> States { get; set; }
    }

    public class CountryModelReader
    {
        public int CountryID { get; set; }

        public string CountryName { get; set; }

        public string CountryISO2Code { get; set; }

        public int StateId { get; set; }

        public string StateName { get; set; }

        public string StateAbbreviation { get; set; }
    }
}
