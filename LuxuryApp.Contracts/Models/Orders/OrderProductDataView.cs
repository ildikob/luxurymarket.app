﻿using System.Configuration;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderProductDataView
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string SKU { get; set; }

        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string MaterialCode { get; set; }

        public string ColorCode { get; set; }

        public string ModelNumber { get; set; }

        public string SeasonCode { get; set; }

        public string DivisionName { get; set; }

        public double RetailPrice { get; set; }

        public string Description { get; set; }

        public string MainImageName { get; set; }

        public string SizeTypeName { get; set; }

        public string MainImageLink
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink100"] + MainImageName;
            }
        }

    }
}
