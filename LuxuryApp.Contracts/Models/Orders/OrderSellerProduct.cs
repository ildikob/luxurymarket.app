﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models.Orders
{
    public abstract class OrderSellerProduct
    {
        public OrderSellerProduct()
        {
            SizeItems = new List<OrderProductSizeItem>();
            ProductDetails = new OrderProductDataView();
        }

       public int OrderSellerProductId { get; set; }

        public int TotalUnits
        {
            get { return SizeItems.Sum(x => x.DisplayUnits); }
        }

        public OrderProductDataView ProductDetails { get; set; }

        public IEnumerable<OrderProductSizeItem> SizeItems { get; set; }
    }

    public class OrderProductItemBuyerView : OrderSellerProduct
    {
        public OrderProductItemBuyerView() : base()
        {
        }

        public double CustomerUnitPrice { get; set; }

        public double UnitShippingTaxPercentage { get; set; }

        public double ShippingCost
        {
            get
            {
                return TotalUnits * Math.Ceiling(CustomerUnitPrice * UnitShippingTaxPercentage / 100);
            }
        }

        public double TotalCost
        {
            get
            {
                return TotalUnits * CustomerUnitPrice;
            }
        }

        public double TotalLandedCost
        {
            get
            {
                return ShippingCost + TotalCost;
            }
        }

        public int OrderedTotalUnits
        {
            get { return SizeItems.Sum(x => x.Units); }
        }
    }

    public class OrderProductItemSellerView : OrderSellerProduct
    {
        public int OrderSellerProductId { get; set; }

        public double OfferCost { get; set; }

        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        public double CurrencyRate { get; set; }

        public double TotalCost
        {
            get
            {
                return TotalUnits * OfferCost;
            }
        }
    }
}
