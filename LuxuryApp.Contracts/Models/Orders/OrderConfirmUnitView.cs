﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderConfirmUnitView
    {
        public OrderConfirmUnitView()
        {
            ConfirmUnits = new List<ConfirmUnit>();
        }

       public int OrderSellerProductId { get; set; }

        public IEnumerable<ConfirmUnit> ConfirmUnits { get; set; }
    }

    public class ConfirmUnit
    {
        public int Id { get; set; }

        public int Unit { get; set; }
    }
}
