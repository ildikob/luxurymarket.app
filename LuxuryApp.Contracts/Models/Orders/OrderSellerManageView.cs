﻿using System;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models.Orders
{
    public class OrderSellerManageList
    {
        public OrderSellerManageList()
        {
            Items = new List<OrderSellerManageView>();
        }
        public IEnumerable<OrderSellerManageView> Items { get; set; }

    }
    public class OrderSellerManageView : OrderManageDetails
    {
        public int BuyerCompanyId { get; set; }

        public int StatusTypeId { get; set; }

        public string StatusTypeName { get; set; }

        public string BuyerAlias { get; set; }

        public int OrderSellerId { get; set; }

    }
}
