﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Configuration;

namespace LuxuryApp.Contracts.Models
{
    public class ProductSearchResult
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string Variant { get; set; }

        public string SKU { get; set; }

        public string Summary { get; set; }

        public int? MainImageId { get; set; }

        public string MainImageName { get; set; }

        public string MainImageUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["S3BucketLink250"] + MainImageName;
            }
        }

        public int HierarchyId { get; set; }

        public CurrencyType RetailCurrency { get; set; }

        public double? RetailPrice { get; set; }

        public double LMPrice { get; set; }

        public CurrencyType CostCurrency { get; set; }

        public double? WholeSaleCost { get; set; }

        public int ColorId { get; set; }

        public string ColorCode { get; set; }

        public string StandardColorName { get; set; }

        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public string StandardBrandName { get; set; }

        public int BusinessID { get; set; }

        public int DivisionID { get; set; }

        public int DepartmentID { get; set; }

        public int AvailableQuantity { get; set; }
    }

    public class ProductSearchFilters
    {
        public int HierarchyID { get; set; }

        public int BrandID { get; set; }

        public int ColorID { get; set; }
    }

    public class ProductSearchResponse
    {
        public List<ProductSearchResult> Products { get; set; }

        public List<int> Brands { get; set; }

        public List<int> Colors { get; set; }

        public List<int> Hierarchies { get; set; }

        public int TotalPages { get; set; }

        public int TotalProducts { get; set; }

        public OfferSummary OfferSummary { get; set; }
    }

    public class OfferSummary
    {
        public int OfferID { get; set; }

        public int TotalProducts { get; set; }

        public int TotalUnits { get; set; }

        public double TotalPrice { get; set; }
    }
}