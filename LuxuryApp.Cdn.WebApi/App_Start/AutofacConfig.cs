﻿using Autofac;
using Autofac.Integration.WebApi;
using LuxuryApp.Cdn.CdnServers;
using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.CdnServers.Repository;
using LuxuryApp.Cdn.CdnServers.Services;
using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using LuxuryApp.Cdn.Image.Contracts;
using LuxuryApp.Cdn.SqlRepository;
using LuxuryApp.Cdn.WebApi.Helpers;
using LuxuryApp.CDN.ImageCdnServers.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Linq;

namespace LuxuryApp.Cdn.WebApi
{
    public class AutofacConfig
    {
        public IDependencyResolver GetDependencyResolver()
        {
            var builder = new ContainerBuilder();

            var connectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;

            builder.Register(c => new MultiCdnRepositorySql(connectionString)).As<IMultiCdnRepository>();

            var localCdnRepository = new LocalCdnSettingsRepositorySql(connectionString);
            builder.RegisterInstance(localCdnRepository).As<IMultiCdnSettingsRepository<LocalCdnSettings>>();

            var amazonS3CdnRepository = new AmazonS3CdnSettingsRepository(connectionString);
            builder.RegisterInstance(amazonS3CdnRepository).As<IMultiCdnSettingsRepository<AmazonS3CdnSettings>>();

            var azureBlobCdnRepository = new AzureBlobCdnSettingsRepository(connectionString);
            builder.RegisterInstance(azureBlobCdnRepository).As<IMultiCdnSettingsRepository<AzureBlobCdnSettings>>();

            var author = new CdnAuthor { UserId = -1, UserIdSource = "LuxuryAppCndApi-hard-coded" };// todo: get from request when integrating with OAuth
            builder.RegisterInstance(author).As<ICdnAuthor>();

            const string cdnComponetsKey = "cdnComp";

            // local iis cdn provider
            //var localSettings = localCdnRepository.GetAllCdnSettings();
            //if (!localSettings.Any())
            //{
            //    localSettings = new List<LocalCdnSettings> { new CDNSettings().LocalCdnSettings };
            //}
            //foreach (var setting in localSettings)
            //{
            //    builder.RegisterInstance(localSettings).As<ICdnSettings>();
            //    builder.Register(c => new IISFileCdnService(c.Resolve<ICdnAuthor>(), setting))
            //        .Named<IFileCdnService<ICdnAuthor, ICdnSettings>>(cdnComponetsKey);
            //};

            //amazon
            var amazonSettings = amazonS3CdnRepository.GetAllCdnSettings();
            if (!amazonSettings.Any())
            {
                amazonSettings = new List<AmazonS3CdnSettings> { new CDNSettings().AmazonS3CdnSettings };
            }
            foreach (var setting in amazonSettings)
            {
                builder.RegisterInstance(setting).As<ICdnSettings>();
                builder.Register(c => new AmazonS3CdnService(c.Resolve<ICdnAuthor>(), setting))
                    .Named<IFileCdnService<ICdnAuthor, ICdnSettings>>(cdnComponetsKey);
            }

            ////azure
            //foreach (var azureBlobSettings in azureBlobCdnRepository.GetAllCdnSettings())
            //{
            //    builder.RegisterInstance(azureBlobSettings).As<ICdnSettings>();
            //    builder.Register(c => new AzureBlobCdnService(c.Resolve<ICdnAuthor>(), azureBlobSettings))
            //        .Named<IFileCdnService<ICdnAuthor, ICdnSettings>>(cdnComponetsKey);
            //}

            builder.Register(c => new MultiCdnService(
                author,
                new NullCdnSettings(),
                c.Resolve<IMultiCdnRepository>(),
                c.ResolveNamed<IEnumerable<IFileCdnService<ICdnAuthor, ICdnSettings>>>(cdnComponetsKey)))
                .As<IFileCdnService<ICdnAuthor, ICdnSettings>>();

            builder.RegisterType<ImageCdnService>().As<IImageCdnService>();

            //builder.RegisterType<ImageCdnService>().AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();
            var container = builder.Build();

            // Web API
            var resolver = new AutofacWebApiDependencyResolver(container);
            return resolver;
        }
    }
}