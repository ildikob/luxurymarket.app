﻿namespace LuxuryApp.Core.Infrastructure.Authorization
{
    public class NullContextProvider : IContextProvider
    {
        public int GetLoggedInUserId()
        {
            return 0;
        }
    }
}