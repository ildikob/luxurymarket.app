﻿using System.Web;
using Microsoft.AspNet.Identity;

namespace LuxuryApp.Core.Infrastructure.Authorization
{
    public class WebContextProvider : IContextProvider
    {
        public int GetLoggedInUserId()
        {
            var owinContext = HttpContext.Current.GetOwinContext();
            var user = owinContext.Request?.User;
            var userId = user?
                .Identity?
                .GetUserId<int>() ?? 0;
            return userId;
        }
    }
}
