using LuxuryApp.Core.Infrastructure.Api.Models;
using System;
using System.Runtime.Serialization;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class ApiExceptionForbidden : ApiException
    {
        public ApiExceptionForbidden()
        {
            Code = ApiResultCode.Forbidden;
        }

        public ApiExceptionForbidden(string message)
            : base(message)
        {
            Code = ApiResultCode.Forbidden;
        }

        public ApiExceptionForbidden(string message, Exception innerException)
            : base(message, innerException)
        {
            Code = ApiResultCode.Forbidden;
        }

        protected ApiExceptionForbidden(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Code = ApiResultCode.Forbidden;
        }
    }
}