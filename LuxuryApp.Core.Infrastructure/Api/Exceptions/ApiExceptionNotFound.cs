using LuxuryApp.Core.Infrastructure.Api.Models;
namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    public class ApiExceptionNotFound : ApiException
    {
        public ApiExceptionNotFound()
        {
            Code = ApiResultCode.NotFound;
        }
    }
}