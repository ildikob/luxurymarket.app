﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.Contracts.Model
{
    public enum CdnResultCode
    {
        None = 0,
        Successfull = 1,
        NotFound = 2,
        Forbidden = 3,
        InternalError = 4,
        BadRequest = 5,
    }

    public class CdnResult
    {
        public string Key { get; protected set; }
        public CdnResultCode Code { get; protected set; }
        public CdnException Exception {get; protected set; }
        public bool IsSuccessful {
            get 
            {
                return Code == CdnResultCode.Successfull;
            }
        }

        public CdnResult(string key, CdnException exception = null)
        {
            if (key == null)
            {
                throw new ArgumentNullException(key);
            }
            Key = key;
            Exception = exception;
            if (exception == null) {
                Code = CdnResultCode.Successfull;
            }else{
                Code = exception.Code;
            }
        }
    }

    public class CdnResult<T> : CdnResult
    {
        private T _result;

        public CdnResult(string key, T result, CdnException exception = null)
            : base (key, exception)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }
            _result = result;
        }

        public T Result {
            get {
                if (IsSuccessful)
                {
                    return _result;
                }
                else 
                {
                    throw Exception;
                }
            }
        }
    }

    public abstract class CdnException : Exception
    {
        public CdnResultCode Code { get; protected set; }
        public CdnException() { }
        public CdnException(Exception e) : base("", e) { }
    }

    public class CdnExceptionNotFound: CdnException
    {
        public CdnExceptionNotFound()
        {
            Code = CdnResultCode.NotFound;
        }
    }

    public class CdnExceptionForbidden : CdnException
    {
        public CdnExceptionForbidden()
        {
            Code = CdnResultCode.Forbidden;
        }
    }

    public class CdnExceptionInternalError: CdnException
    {
        public CdnExceptionInternalError(Exception e):
            base(e)
        {
            Code = CdnResultCode.InternalError;
        }
    }

    public class CdnExceptionBadRequest : CdnException
    {
        public CdnExceptionBadRequest()
        {
            Code = CdnResultCode.BadRequest;
        }
    }
}
