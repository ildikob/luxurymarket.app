﻿var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean');

var minifyCSS = require('gulp-minify-css');

// Fonts
var fonts = {
    in: [
        'bower_components/bootstrap/dist/fonts/**/*',
        'bower_components/font-awesome/fonts/*',
        'assets/fonts/luxurymarket/*'
    ],
    out: 'dist/fonts/'
};

// CSS source file: .scss files
var css = {
    in: [
        'assets/stylesheets/main.scss'
    ],
    out: 'dist/css/',
    watch: 'assets/stylesheets/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true
    }
};

var scripts = [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/moment/min/moment.min.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
            'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-messages/angular-messages.js',
            'bower_components/angular-local-storage/dist/angular-local-storage.js',
            'bower_components/angular-cookies/angular-cookies.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/angular-filter/dist/angular-filter.js',
            'bower_components/angular-translate/angular-translate.js',
            'bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
            'bower_components/underscore/underscore.js',
            'bower_components/ng-file-upload-shim/ng-file-upload-shim.js',
            'bower_components/ng-file-upload/ng-file-upload.js',
            'bower_components/angular-ui-notification/dist/angular-ui-notification.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'bower_components/angular-endless-scroll/dist/angular-endless-scroll.min.js',
            'bower_components/ui-router-extras/release/ct-ui-router-extras.js',
            'bower_components/angular-slick/dist/slick.js',
            'bower_components/slick-carousel/slick/slick.min.js',
            'bower_components/humanize-duration/humanize-duration.js',
            'bower_components/angular-timer/dist/angular-timer.js',
            'src/**/*.module.js',
            'src/**/*.route.js',
            'src/**/*.config.js',
            'src/**/*.interceptor.js',
            'src/**/*.service.js',
            'src/**/*.controller.js',
            'src/**/*.directive.js',
            'src/**/*.constants.js',
            'src/**/libs.*.js',
            'src/templates/*.html'
];

// Copy bootstrap required fonts to dist
gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

// Compile scss
gulp.task('compile-styles', ['fonts'], function () {
    return gulp.src(css.in)
        .pipe(sass(css.sassOpts))
        .pipe(gulp.dest(css.out));
});

gulp.task('minify-styles', ['compile-styles'], function () {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/angular-bootstrap-datetimepicker/src/css/datetimepicker.css',
        'bower_components/angular-ui-notification/dist/angular-ui-notification.css',
        'bower_components/slick-carousel/slick/slick.css',
        'bower_components/slick-carousel/slick/slick-theme.css',
        'dist/css/main.css'
    ]).pipe(concat('app.css'))
      // only for release
      .pipe(minifyCSS())
      .pipe(gulp.dest('dist/css'));
});

// Copy all image files to dist
gulp.task('copy-images-files', function () {
    return gulp.src('assets/images/**/*.{jpg,jpeg,png,gif}')
        .pipe(gulp.dest('dist/images'));
});

gulp.task('minify-js', function () {
    gulp.src(scripts)
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        // only for production
        //.pipe(uglify())
        //.pipe(gulpif(argv.production, uglify()))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dist/js'));
});

// Clean all files in dist
gulp.task('clean', function () {
    gulp.src('./dist/*')
        .pipe(clean({ force: true }));
});

// build task
gulp.task('build', [
    'minify-styles',
    'minify-js',
    'copy-images-files'
], function () {
    livereload.listen();

    gulp.watch(css.watch, ['minify-styles']);
    gulp.watch('src/**/*.js', ['minify-js']);
    gulp.watch('assets/images/**/*.{jpg,jpeg,png,gif}', ['copy-images-files']);
});
