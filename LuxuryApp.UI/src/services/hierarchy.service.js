﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('HierarchyService', HierarchyService);

    HierarchyService.$inject = ['$http', 'SERVICES_CONFIG', 'localStorageService', '$q'];

    function HierarchyService($http, SERVICES_CONFIG, localStorageService, $q) {

        var svc = {
            load: load,
            get: get
        };

        return svc;

        function load() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/hierarchy/get')
                .then(function (response) {
                    if (response.status === 200) {
                        localStorageService.set('hierarchiesData', response.data);
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load hierarchies' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load hierarchies' };
                });
        }

        // Might need some fixes
        function get() {
            var hierarchiesData = localStorageService.get('hierarchiesData');
            if (hierarchiesData) {
                return $q.resolve(hierarchiesData);
            }
            else {
                return load().then(function (response) {
                    if (response.success) {
                        return localStorageService.get('hierarchiesData');
                    }
                    else {
                        return null;
                    }
                });
            }
        }
    }
})(window.angular);