﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('CdnService', CdnService);

    CdnService.$inject = ['$http', 'SERVICES_CONFIG'];

    function CdnService($http, SERVICES_CONFIG) {

        var svc = {
            getImages: getImages
        };

        return svc;

        function getImages(params) {
            return $http.post(SERVICES_CONFIG.cdnServiceUrl + 'api/imagecdn/getcontenturls', params)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'There was an error. Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.Message };
                });
        }
    }
})(window.angular);