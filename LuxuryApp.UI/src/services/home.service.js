﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('HomeService', HomeService);

    HomeService.$inject = ['$http', 'SERVICES_CONFIG', 'localStorageService'];

    function HomeService($http, SERVICES_CONFIG, localStorageService) {

        var svc = {
            getFeaturedEvents: getFeaturedEvents,
            getTopicList: getTopicList,
            addCustomerServiceRequest: addCustomerServiceRequest
        };

        return svc;

        function getFeaturedEvents() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/home/get_featured_event')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load featured events' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load featured events' };
                });
        }

        function getTopicList() {
            return $http.get(SERVICES_CONFIG.apiUrl + '/api/home/get_customer_service_topics')
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data };
                    }
                    else {
                        return { success: false, error_message: 'Unable to load topics' };
                    }
                }, function (response) {
                    return { success: false, error_message: 'Unable to load topics' };
                });
        }

        function addCustomerServiceRequest(model) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/home/customer_service_insert_request', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true, data: response.data.data };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }
    }
})(window.angular);