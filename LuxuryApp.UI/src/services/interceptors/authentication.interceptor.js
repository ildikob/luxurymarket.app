﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services.interceptors')
        .factory('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', 'localStorageService', '$injector', 'APP_ENV'];

    function authInterceptorService($q, localStorageService, $injector, APP_ENV) {

        var _request = function (config) {

            config.headers = config.headers || {};
            var noBearer = config.noBearer || false;

            var authData = localStorageService.get('authorizationData' + APP_ENV);
            if (authData && authData.token && !noBearer) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        };

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                var authService = $injector.get('AuthenticationService');
         
                authService.refreshToken()
                    .then(function (response) {
                        var state = $injector.get('$state');
                        if (response.success) {
                            state.reload();
                        }
                        else {
                            authService.logout();
                            state.go('auth.login');
                        }
                    });

            }
            return $q.reject(rejection);
        };

        return {
            request: _request,
            responseError: _responseError
        };
    }

})(window.angular);