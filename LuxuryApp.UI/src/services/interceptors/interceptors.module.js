﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services.interceptors', []);

})(window.angular);