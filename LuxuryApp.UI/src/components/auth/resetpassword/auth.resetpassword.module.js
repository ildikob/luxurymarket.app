﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.resetpassword', []);

})(window.angular);