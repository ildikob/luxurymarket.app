﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.resetpassword')
        .controller('ResetPasswordController', ResetPasswordController);

    ResetPasswordController.$inject = ['$state', '$stateParams', 'AuthenticationService', 'COMPANY_TYPES'];

    function ResetPasswordController($state, $stateParams, AuthenticationService, COMPANY_TYPES) {
        var vm = this;

        if (!$stateParams.email || !$stateParams.code) {
            $state.go('auth.login');
        }

        // view models
        vm.setupPasswordModel = {
            email: $stateParams.email,
            password: null,
            confirmPassword: null,
            code: $stateParams.code
        };
        vm.isResetPassword = $stateParams.newUser.toLowerCase() === 'false';
        vm.isLoading = false;

        // functions
        vm.submit = submit;
        vm.validateField = validateField;
        
        function submit() {
            vm.isLoading = true;
            AuthenticationService.resetPassword(vm.setupPasswordModel).then(function (response) {
                if (response.success) {
                    vm.isLoading = false;
                    AuthenticationService.getToken({
                        "userName": vm.setupPasswordModel.email,
                        "password": vm.setupPasswordModel.password,
                        "rememberMe": true
                    }).then(function (response) {
                        vm.isLoading = false;
                        if (response.success) {
                            if (COMPANY_TYPES.Buyer === response.data.Companies[0].CompanyType) {
                                $state.go('site.home');
                            }
                            else {
                                $state.go('admin.new-offer.upload');
                            }
                        }
                        else {
                            $state.go('auth.login');
                        }
                    });
                }
                else {
                    vm.isLoading = false;
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

        function validateField(fieldName) {
          if (vm.setupPasswordForm[fieldName].$invalid && vm.setupPasswordForm[fieldName].$dirty) {
            return true;
          }

          if (vm.setupPasswordForm.$submitted && vm.setupPasswordForm[fieldName].$invalid) {
            return true;
          }

          return false;
        }
    }
})(window.angular);
