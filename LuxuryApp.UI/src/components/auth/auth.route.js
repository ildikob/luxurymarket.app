﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('auth.login', {
                url: "/login",
                templateUrl: "src/templates/auth/new-login.html",
                controller: "LoginController",
                controllerAs: "login"
            })
            .state('auth.register', {
                url: "/register",
                templateUrl: "src/templates/auth/new-register.html",
                controller: "RegisterController",
                controllerAs: "register"
            })
            .state('auth.forgot-password', {
                url: "/forgot-password",
                templateUrl: "src/templates/auth/new-forgot-password.html",
                controller: 'ForgotPasswordController',
                controllerAs: 'forgotpass'
            })
            .state('auth.reset-password', {
                url: '/resetpassword?email&code&newUser',
                templateUrl: 'src/templates/auth/create-password.html',
                controller: 'ResetPasswordController',
                controllerAs: 'reset'
            })
            .state('auth.message', {
                url: "/message",
                templateUrl: "src/templates/auth/message.html",
                controller: 'MessageController',
                controllerAs: 'msg',
                params: {
                    type: null
                }
            })
            .state('auth.static', {
                abstract: true,
                url: '/content',
                templateUrl: 'src/templates/content/index.html',
                controller: 'StaticContentController',
                controllerAs: 'staticcontent'
            })
            .state('auth.static.about', {
                url: "/about",
                templateUrl: "src/templates/content/about.html"
            })
            .state('auth.static.customer-service', {
                url: "/customer-service",
                templateUrl: "src/templates/content/customer-service.html",
                controller: 'CustomerServiceController',
                controllerAs: 'customerService',
                translationPartId: 'customer-service'
            })
            .state('auth.static.policies', {
                url: "/policies",
                templateUrl: "src/templates/content/policies.html"
            })
            .state('auth.static.authenticity', {
                url: "/authenticity",
                templateUrl: "src/templates/content/authenticity.html"
            })
            .state('auth.static.terms-and-conditions', {
                url: "/terms-and-conditions",
                templateUrl: "src/templates/content/terms-and-conditions.html"
            });
    }

})(window.angular);
