﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.ordersummary')
        .controller('OrderSummaryController', OrderSummaryController);

    OrderSummaryController.$inject = ['$state', '$stateParams', 'AuthenticationService',
        'CartService', 'CdnService', 'OrderService', 'AddressService', '_', 'ADDRESS_TYPES', '$scope', 'Notification', '$q'];

    function OrderSummaryController($state, $stateParams, AuthenticationService,
        CartService, CdnService, OrderService, AddressService, _, ADDRESS_TYPES, $scope, Notification, $q) {
        var vm = this;

        // view models
        vm.showInvalidItems = false;
        vm.loadingProducts = false;
        vm.productDetails = null;
        vm.showConfirm = false;
        vm.viewProducts = false;
        vm.preview = {
            selectedPaymentMethod: null,
            paymentMethods: [],
            billingAddress: null,
            shippingAddress: null,
            summary: null
        };
        vm.isSubmitting = false;
        vm.validAddress = false;
        vm.orderNumber = null;
        vm.viewInvalidItems = false;

        // init
        _load();

        // functions
        vm.submitOrder = submitOrder;
        vm.showProducts = showProducts;
        vm.imageLoaded = imageLoaded;

        function submitOrder() {

            // validation to display error messages
            if (vm.submitForm.$invalid || !vm.validAddress) {
                if (vm.submitForm.$invalid && vm.validAddress) {
                    Notification("Please select a payment method");
                }
                else if (vm.submitForm.$valid && !vm.validAddress) {
                    Notification("Please update your addresses");
                }
                else {
                    Notification("Please select a payment and update your addresses");
                }
            }
            else {
                vm.isSubmitting = true;
                if (vm.showInvalidItems) {
                    _submitOrder();
                }
                else {
                    _checkInvalidItems();
                }
            }
        }

        function imageLoaded(product) {
            product.showImage = true;
        }

        function showProducts($event) {
            if (vm.productDetails !== null) {
                vm.viewProducts = !vm.viewProducts;
            }
            else {
                vm.loadingProducts = true;
                _getCartProducts();
            }
        }

        // private functions
        function _load() {
            _updateOrderSummary();

            OrderService.getAvailablePaymentMethods()
                .then(function (response) {
                    if (response.success) {
                        response.data.splice(0, 0, {
                            paymentMethodId: null,
                            paymentMethodName: 'Select payment'
                        });
                        vm.preview.paymentMethods = response.data;
                    }
                    else {
                        // show error
                    }
                });

            AddressService.getMainAddresses(AuthenticationService.getCurrentUserID())
                .then(function (response) {
                    if (response.success) {
                        vm.preview.billingAddress = _.findWhere(response.data, { addressTypeId: ADDRESS_TYPES.Billing_Address });
                        vm.preview.shippingAddress = _.findWhere(response.data, { addressTypeId: ADDRESS_TYPES.Shipping_Address });

                        if (_.isUndefined(vm.preview.billingAddress) || _.isUndefined(vm.preview.shippingAddress)) {
                            vm.validAddress = false;
                        } else {
                            vm.validAddress = true;
                        }
                    }
                    else {
                        // show error
                    }
                });
        }

        function _getCartProducts() {
            if (vm.productDetails !== null) {
                return $q.resolve({});
            }
            else {
                return CartService.getCart(AuthenticationService.getCurrentUserID())
                     .then(function (response) {
                         vm.loadingProducts = false;
                         if (response.success) {
                             vm.viewProducts = !vm.viewProducts;
                             vm.productDetails = response.data;

                             _.forEach(vm.productDetails.items, function (item) {
                                 var unitsTotalCount = 0;
                                 _.forEach(item.sellerUnits, function (su) {
                                     unitsTotalCount += su.unitsCount;
                                 });
                                 item.totalUnitsCount = unitsTotalCount;

                                 var activeSellers = _.find(item.sellerUnits, function (s) {
                                     return s.active;
                                 });

                                 item.inactive = (activeSellers || []).length === 0;
                             });

                         }
                         else {
                             // show error
                         }
                     });
            }
        }

        function _updateOrderSummary(onlyValidItems) {
            CartService.getCartSummary(AuthenticationService.getCurrentUserID(), true, $stateParams.cartId, onlyValidItems)
                .then(function (response) {
                    if (response.success) {
                        vm.preview.summary = response.data;
                    }
                    else {
                        // show error
                    }
                });
        }

        function _submitOrder() {
            OrderService.submit({
                cartId: $stateParams.cartId,
                paymentMethodId: vm.preview.selectedPaymentMethod
            }).then(function (response) {
                vm.isSubmitting = false;
                if (response.success) {
                    $scope.$emit('updateCart', false, {
                        itemsCount: 0,
                        unitsCount: 0,
                        totalCustomerCost: 0
                    });

                    vm.orderNumber = response.data;
                    vm.showConfirm = true;
                }
                else {
                    // show error
                }
            });
        }

        function _checkInvalidItems() {
            CartService.getInvalidItems($stateParams.cartId).then(function (response) {
                if (response.success) {
                    if (response.data.items.length === 0) {
                        _submitOrder();
                    }
                    else {
                        _updateOrderSummary(true);

                        // display invalid items
                        _getCartProducts().then(function () {
                            _.forEach(vm.productDetails.items, function (item) {
                                _.forEach(item.sellerUnits, function (su) {
                                    _.forEach(su.sellerUnitSizes, function (sus) {
                                        var susInfoToUpdate = _.findWhere(response.data.items, { offerProductSizeId: sus.offerProductSizeId });
                                        if (!_.isUndefined(susInfoToUpdate)) {
                                            angular.merge(sus, susInfoToUpdate);
                                            item.hasInvalidItems = true;
                                        }
                                    });
                                });
                            });

                            _.forEach(vm.productDetails.items, function (item) {
                                if (item.hasInvalidItems) {
                                    item.totalUnitsCount = 0;
                                    _.forEach(item.sellerUnits, function (su) {
                                        su.validUnitsTotalCount = 0;
                                        su.invalidUnitsTotalCount = 0;
                                        _.forEach(su.sellerUnitSizes, function (sus) {
                                            if (!sus.isActiveOffer || !sus.offerHasAvailableQuantity) {
                                                su.invalidUnitsTotalCount += sus.quantity;
                                            }
                                            else {
                                                su.validUnitsTotalCount += sus.quantity;
                                            }
                                        });

                                        su.unitsCount = su.validUnitsTotalCount;
                                        item.totalUnitsCount += su.validUnitsTotalCount;
                                    });
                                }
                            });

                            vm.viewProducts = false;
                            vm.showInvalidItems = true;
                            vm.isSubmitting = false;
                        });
                    }
                }
                else {
                    vm.isSubmitting = false;
                    // display error
                }
            });
        }

    }
})(window.angular);