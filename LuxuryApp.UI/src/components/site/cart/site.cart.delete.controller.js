﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.cart')
        .controller('DeleteCartController', DeleteCartController);

    DeleteCartController.$inject = ['$rootScope', '$uibModalInstance', 'Notification', 'type', 'product', 'seller', 'CartService', 'CART_DELETE_TYPES', 'AuthenticationService', '$translatePartialLoader'];

    function DeleteCartController($rootScope, $uibModalInstance, Notification, type, product, seller, CartService, CART_DELETE_TYPES, AuthenticationService, $translatePartialLoader) {
        var vm = this;

        // private vars
        vm.companyId = AuthenticationService.getCurrentUserID();

        // view models
        vm.isLoading = false;
        vm.question = null;
        vm.translationData = null;

        // functions
        vm.close = close;
        vm.submit = submit;

        // init
        $translatePartialLoader.addPart('cart-delete');
        _init();

        // functions      
        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function submit($event) {
            vm.isLoading = true;
            var params = {};
            params.buyerCompanyId = vm.companyId;
            switch (type) {
                case CART_DELETE_TYPES.SELLER:
                    params.cartItemSellerUnitIdentifier = seller.cartItemSellerUnitIdentifier;
                    CartService.removeItemSellerUnit(params).then(function (response) {
                        vm.isLoading = false;
                        if (response.success) {
                            close();
                            $rootScope.$broadcast('updateCartAfterDelete', response.data);
                            Notification('Item seller unit deleted from cart with success');
                        }
                        else {
                            Notification('There was an error. Please try again.');
                        }
                    });
                    break;
                case CART_DELETE_TYPES.PRODUCT:
                    params.productIds = [product.productId];
                    CartService.removeProduct(params).then(function (response) {
                        vm.isLoading = false;
                        if (response.success) {
                            close();
                            $rootScope.$broadcast('updateCartAfterDelete', response.data);
                            Notification('Product deleted from cart with success');
                        }
                        else {
                            Notification('There was an error. Please try again.');
                        }
                    });
                    break;
                case CART_DELETE_TYPES.ALL:
                    CartService.emptyCart(params).then(function (response) {
                        vm.isLoading = false;
                        if (response.success) {
                            close();
                            $rootScope.$broadcast('updateCartAfterDelete');
                            Notification('Cart deleted with success');
                        }
                        else {
                            Notification('There was an error. Please try again.');
                        }
                    });
                    break;
            }
        }

        // private functions
        function _init() {
            switch (type) {
                case CART_DELETE_TYPES.SELLER:
                    vm.title = 'CART_DELETE_SELLER_UNIT_TITLE';
                    vm.question = 'CART_DELETE_SELLER_UNIT_QUESTION';
                    vm.translationData = {
                        productName: product.productName,
                        sellerAlias: seller.sellerAlias
                    };
                    break;
                case CART_DELETE_TYPES.PRODUCT:
                    vm.title = 'CART_DELETE_PRODUCT_TITLE';
                    vm.question = 'CART_DELETE_PRODUCT_QUESTION';
                    vm.translationData = {
                        productName: product.productName
                    };
                    break;
                case CART_DELETE_TYPES.ALL:
                    vm.title = 'CART_DELETE_EMPTY_CART_TITLE';
                    vm.question = 'CART_DELETE_EMPTY_CART_QUESTION';
                    break;
            }
        }
    }
})(window.angular);