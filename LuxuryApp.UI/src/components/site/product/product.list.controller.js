﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.product')
        .controller('ProductsListController', ProductsListController);

    ProductsListController.$inject = ['$stateParams', 'ProductService', '$state', 'CdnService', 'AttributesService', 'HierarchyService', '_', '$scope', 'CartService', 'AuthenticationService', 'COMPANY_TYPES', 'Notification', '$translatePartialLoader', '$sanitize'];

    function ProductsListController($stateParams, ProductService, $state, CdnService, AttributesService, HierarchyService, _, $scope, CartService, AuthenticationService, COMPANY_TYPES, Notification, $translatePartialLoader) {
        var vm = this;

        // private vars
        var firstLoad = true, reloadFilters = false;
        var businessId = $stateParams.businessId;
        var departmentId = $stateParams.departmentId;
        var divisionId = $stateParams.divisionId;
        var featuredEventId = $stateParams.featuredEventId;
        var brandId = $stateParams.brandId;
        var keyword = $stateParams.keyword;
        var sellerId = $stateParams.sId;
        var offerId = $stateParams.oId;

        // view models
        vm.showBrands = _.isUndefined(brandId);
        vm.isSeller = AuthenticationService.getCurrentUserType() === COMPANY_TYPES.Seller;
        vm.header = $stateParams.header || (_.isUndefined(offerId) ? "PRODUCT_LIST_HEADER" : "Take All Offer");
        vm.subtitle = $stateParams.subtitle || "PRODUCT_LIST_SUBTITLE";
        vm.translateHeader = {
            title: _.isUndefined($stateParams.header) || $stateParams.header === null,
            subtitle: _.isUndefined($stateParams.subtitle) || $stateParams.subtitle === null
        };
        vm.showTakeAllDetails = !_.isUndefined(offerId);

        vm.details = null;
        vm.filters = {
            brands: [],
            colors: [],
            categories: [],
            business: null,
            availableBusinesses: [],
            showTree: false
        };
        vm.sort = [
            {
                label: 'PRODUCT_LIST_SORT_NEWEST',
                id: 0,
                active: true
            },
            {
                label: 'PRODUCT_LIST_SORT_PRICE_LOW_HIGH',
                id: 1,
                active: false
            },
            {
                label: 'PRODUCT_LIST_SORT_PRICE_HIGH_LOW',
                id: 2,
                active: false
            }
        ];
        vm.pagination = {
            totalPages: 1,
            page: 1,
            showAll: false,
            pages: []
        };
        vm.isLoading = true;
        vm.showTakeAll = !_.isUndefined(offerId);

        // functions
        vm.changePage = changePage;
        vm.goToDetails = goToDetails;
        vm.imageLoaded = imageLoaded;
        vm.filterBy = filterBy;
        vm.sortBy = sortBy;
        vm.openTree = openTree;
        vm.changeBusiness = changeBusiness;
        vm.addTakeAll = addTakeAll;

        // init
        $translatePartialLoader.addPart('product-list');
        _search(vm.pagination.page);

        // functions
        function changePage($event, type, step) {
            $event.preventDefault();

            switch (type) {
                case 'pageClick':
                    vm.pagination.page = step;
                    break;
                case 'navClick':
                    if (step === 1) {
                        if (vm.pagination.page + 1 > vm.pagination.totalPages) {
                            return false;
                        }

                        vm.pagination.page++;
                    }
                    else {
                        if (vm.pagination.page - 1 === 0) {
                            return false;
                        }

                        vm.pagination.page--;
                    }
                    break;
            }

            _createPages();

            _search(vm.pagination.page);
        }

        function goToDetails($event, product) {
            $event.preventDefault();
            if (product.availableQuantity > 0) {
                $state.go('site.products-wrapper.details', {
                    id: product.id,
                    listParams: $stateParams
                });
            }
        }

        function imageLoaded(product) {
            product.showImage = true;
        }

        function filterBy(type, entity, parent, parentOfParent) {
            switch (type) {
                case 'brand':
                    _filterByBrand(entity);
                    break;
                case 'color':
                    _filterByColor(entity);
                    break;
                case 'division':
                    _filterByDivision(entity);
                    break;
                case 'department':
                    _filterByDepartment(entity, parent);
                    break;
                case 'category':
                    _filterByCategory(entity, parent, parentOfParent);
                    break;
            }
        }

        function sortBy($event, type) {
            $event.preventDefault();

            _.each(vm.sort, function (s) {
                s.active = false;
            });

            type.active = true;

            vm.pagination.page = 1;
            _search(vm.pagination.page);
        }

        function openTree(type, entity, parent) {
            switch (type) {
                case 'division':
                    _openTreeDivision(entity);
                    break;
                case 'department':
                    _openTreeDepartment(entity, parent);
                    break;
            }
        }

        function changeBusiness($event, business) {
            if (business.selected) {
                $event.preventDefault();
                $event.stopPropagation();
            }

            _.forEach(vm.filters.availableBusinesses, function (b) {
                b.selected = false;
            });

            business.selected = true;
            vm.filters.business = business;
            var alreadyOpened = false;
            _.forEach(vm.filters.business.divisions, function (d) {
                if (d.active) {
                    if (!alreadyOpened) {
                        alreadyOpened = true;
                        d.open = true;
                    }
                    else {
                        d.open = false;
                    }
                }
            });

            vm.filters.showTree = true;
            reloadFilters = true;
            businessId = business.businessID;
            vm.filters.page = 1;
            // reset colors filter
            _.forEach(vm.filters.colors, function (c) {
                c.active = false;
            });
            _search(vm.filters.page);
        }


        function addTakeAll($event) {
            $event.preventDefault();

            vm.isAddingOffer = true;
            CartService.addOffer({
                "buyerCompanyId": AuthenticationService.getCurrentUserID(),
                "offerId": offerId
            }).then(function (response) {
                vm.isAddingOffer = false;
                if (response.success) {
                    Notification("Offer added to cart with success");
                    $scope.$emit('updateCart', true);
                }
                else {
                    Notification(response.error_message);
                }
            });
        }

        // private functions
        function _createPages() {
            vm.pagination.pages = [];
            vm.pagination.begin = Math.max(1, vm.pagination.page - 2);
            vm.pagination.end = Math.min(vm.pagination.page + 2, vm.pagination.totalPages);
            for (var i = vm.pagination.begin ; i <= vm.pagination.end; i++) {
                vm.pagination.pages.push(i);
            }

            if (vm.pagination.end + 1 <= vm.pagination.totalPages) {
                vm.pagination.showAll = true;
            }
            else {
                vm.pagination.showAll = false;
            }
        }


        function _search(page) {
            var params = {
                page: page,
                businessId: businessId
            };

            if (departmentId !== null) {
                params.departmentId = parseInt(departmentId);
            }

            if (divisionId !== null) {
                params.divisionId = parseInt(divisionId);
            }

            // get params
            var colors = [];
            _.each(vm.filters.colors, function (color) {
                if (color.active) {
                    colors.push(color.standardId);
                }
            });

            var brands = [];
            if (brandId !== null && !_.isUndefined(brandId)) {
                brands.push(brandId);
            }
            else {
                _.each(vm.filters.brands, function (brand) {
                    if (brand.active) {
                        brands.push(brand.id);
                    }
                });
            }

            if (colors.length) {
                params.colors = colors;
            }

            if (brands.length) {
                params.brands = brands;
            }

            // categories 
            if (vm.filters.categories.length) {
                params.categories = vm.filters.categories;
            }

            // sort
            var sortBy = _.findWhere(vm.sort, { active: true });
            if (!_.isUndefined(sortBy)) {
                params.sort = sortBy.id;
            }

            // featured event id
            if (featuredEventId !== null) {
                params.featuredEventId = featuredEventId;
            }

            // keyword
            if (keyword !== null && !_.isUndefined(keyword)) {
                params.keyword = keyword;
            }

            if (sellerId !== null && !_.isUndefined(sellerId)) {
                params.sellerId = sellerId;
            }

            if (offerId !== null && !_.isUndefined(offerId)) {
                params.offerId = offerId;
            }

            ProductService.search(params)
                .then(function (response) {
                    vm.isLoading = false;
                    if (response.success) {
                        vm.details = response.data;
                        vm.pagination.totalPages = response.data.totalPages;
                        vm.showTakeAllDetails = !_.isUndefined(response.data.offerSummary);

                        _createPages();

                        if (firstLoad) {

                            firstLoad = false;

                            _loadBrands();

                            _loadColors();

                            HierarchyService.get().then(function (hierarchies) {

                                // process hierarchies
                                var markedDivision = false,
                                    markedDepartment = false;
                                _.forEach(hierarchies, function (currentBusiness) {
                                    _.forEach(currentBusiness.divisions, function (currentDivision) {
                                        _.forEach(currentDivision.departments, function (currentDepartment) {
                                            _.forEach(currentDepartment.categories, function (currentCategory) {
                                                if (_.contains(response.data.hierarchies, currentCategory.id)) {
                                                    currentBusiness.active = true;
                                                    currentDivision.active = true;
                                                    currentDepartment.active = true;
                                                    currentCategory.active = true;

                                                    if (!markedDivision) {
                                                        currentDivision.open = true;
                                                        markedDivision = true;
                                                    }

                                                    if (!markedDepartment) {
                                                        currentDepartment.open = true;
                                                        markedDepartment = true;
                                                    }
                                                }
                                            });
                                        });
                                    });
                                });

                                vm.filters.availableBusinesses = _.where(hierarchies, { active: true });
                                if (!_.isUndefined(vm.filters.availableBusinesses) && vm.filters.availableBusinesses.length > 0) {
                                    if (vm.filters.availableBusinesses.length === 1) {
                                        vm.filters.availableBusinesses[0].selected = true;
                                    }
                                    vm.filters.business = vm.filters.availableBusinesses[0];

                                    if (vm.filters.availableBusinesses.length > 1) {
                                        _.forEach(vm.filters.availableBusinesses, function (business) {
                                            if (business.businessID !== vm.filters.business.businessID) {
                                                business.selected = false;
                                            }
                                        });
                                    }
                                }

                                vm.filters.showTree = !_.isUndefined(_.findWhere(vm.filters.availableBusinesses, { selected: true }));
                            });
                        }

                        if (reloadFilters) {
                            reloadFilters = false;
                            _loadColors();
                        }

                        //_loadImages();
                    }
                    else {
                        // display error -> failed to load products
                    }
                });
        }

        function _filterByBrand(brand) {
            if (_.isUndefined(brand.active)) {
                brand.active = false;
            }

            brand.active = !brand.active;

            // trigger search
            vm.pagination.page = 1;
            _search(vm.pagination.page);
        }

        function _filterByColor(color) {
            if (_.isUndefined(color.active)) {
                color.active = false;
            }

            color.active = !color.active;

            // trigger search
            vm.pagination.page = 1;
            _search(vm.pagination.page);
        }

        function _filterByCategory(category, department, division) {
            if (_.isUndefined(category.selected)) {
                category.selected = false;
            }

            category.selected = !category.selected;

            vm.filters.categories = [];
            _.forEach(department.categories, function (c) {
                if (c.active && c.selected) {
                    vm.filters.categories.push(c.categoryID);
                }
            });


            // trigger search
            vm.pagination.page = 1;
            _search(vm.pagination.page);
        }

        function _filterByDivision(division) {
            if (_.isUndefined(division.selected)) {
                division.selected = false;
            }

            if (division.selected) {
                division.selected = false;
            }
            else {
                var alreadyOpened = false;
                _.forEach(vm.filters.business.divisions, function (d) {
                    if (d.active) {
                        if (d.divisionID === division.divisionID) {
                            d.selected = true;
                            d.open = true;
                        }
                        else {
                            d.selected = false;
                            d.open = false;
                        }
                    }
                });
            }

            reloadFilters = true;
            departmentId = undefined;
            if (division.selected) {
                divisionId = division.divisionID;
            }
            else {
                divisionId = undefined;
            }
            vm.filters.page = 1;
            // reset colors filter
            _.forEach(vm.filters.colors, function (c) {
                c.active = false;
            });

            // reset brands filter
            if (!_.isUndefined($stateParams.brandId)) {
                _.forEach(vm.filters.colors, function (c) {
                    c.active = false;
                });
            }
            vm.filters.categories = [];
            _search(vm.filters.page);
        }

        function _filterByDepartment(department, division) {
            if (_.isUndefined(department.selected)) {
                department.selected = false;
            }

            if (department.selected) {
                department.selected = false;
            }
            else {
                var alreadyOpened = false;
                _.forEach(division.departments, function (d) {
                    if (d.active) {
                        if (d.departmentID === department.departmentID) {
                            d.selected = true;
                            d.open = true;
                        }
                        else {
                            d.selected = false;
                            d.open = false;
                        }
                    }
                });
            }

            reloadFilters = true;
            division.selected = true;
            divisionId = division.divisionID;
            if (department.selected) {
                departmentId = department.departmentID;
            }
            else {
                departmentId = undefined;
            }
            vm.filters.page = 1;
            // reset colors filter
            _.forEach(vm.filters.colors, function (c) {
                c.active = false;
            });

            // reset brands filter
            if (!_.isUndefined($stateParams.brandId)) {
                _.forEach(vm.filters.colors, function (c) {
                    c.active = false;
                });
            }
            vm.filters.categories = [];
            _search(vm.filters.page);
        }

        function _loadBrands() {
            AttributesService.getBrands().then(function (response) {
                if (response.success) {
                    vm.filters.brands = _.filter(response.data, function (brand) {
                        return _.contains(vm.details.brands, brand.id);
                    });
                }
                else {

                }
            });
        }

        function _loadColors() {
            AttributesService.getStandardColors().then(function (response) {
                if (response.success) {
                    vm.filters.colors = _.filter(response.data, function (color) {
                        return _.contains(vm.details.colors, color.standardId);
                    });
                }
                else {

                }
            });
        }

        function _loadImages() {
            var params = [];
            _.forEach(vm.details.products, function (item) {
                if (item.mainImageName.length > 0) {
                    params.push({
                        key: item.mainImageName,
                        imageSizes: [
                              {
                                  "dimension": 1,
                                  "size": 250,
                                  "subFolder": "250"
                              }
                        ]
                    });
                }
                else {
                    item.imageUrl = '';
                }
            });

            CdnService.getImages(params)
                .then(function (response) {
                    if (response.success) {
                        _.forEach(response.data, function (image) {
                            // find product for image
                            var product = _.findWhere(vm.details.products, {
                                mainImageName: image.key
                            });

                            if (product) {
                                product.imageUrl = image.result[0].url;
                            }
                            else {
                                product.imageUrl = '';
                            }
                        });
                    }
                    else {
                        // 
                    }
                });
        }

        function _openTreeDivision(entity) {
            if (entity.open) {
                entity.open = false;
                return;
            }

            _.forEach(vm.filters.business.divisions, function (division) {
                if (division.active) {
                    if (division.divisionID === entity.divisionID) {
                        entity.open = !entity.open;
                        division.open = entity.open;
                    }
                    else {
                        division.open = false;
                    }

                    if (!division.open) {
                        _.forEach(division.departments, function (department) {
                            department.open = false;
                        });
                    }
                }
            });
        }

        function _openTreeDepartment(entity, division) {
            if (entity.open) {
                entity.open = false;
                return;
            }

            _.forEach(division.departments, function (department) {
                if (department.active) {
                    if (department.departmentID === entity.departmentID) {
                        entity.open = !entity.open;
                    }
                    else {
                        department.open = false;
                    }
                }
            });
        }
    }
})(window.angular);