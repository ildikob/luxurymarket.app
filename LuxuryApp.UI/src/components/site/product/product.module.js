﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.product', []);

})(window.angular);