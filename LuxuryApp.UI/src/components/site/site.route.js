﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider', '$stickyStateProvider'];

    function routeConfigs($stateProvider, $stickyStateProvider) {

        //$stickyStateProvider.enableDebug(true);

        $stateProvider
            .state('site.home', {
                url: "/home",
                templateUrl: "src/templates/site/home.html",
                controller: "HomeController",
                controllerAs: "home",
                translationPartId: 'home'
            })
            .state('site.products-wrapper', {
                url: "/products",
                templateUrl: "src/templates/site/products-wrapper.html"
            })
            .state('site.products-wrapper.list', {
                url: "/list/:listType?featuredEventId&brandId&businessId&divisionId&departmentId&sId&oId",
                sticky: true,
                views: {
                    'list': {
                        templateUrl: "src/templates/site/products-list.html",
                        controller: "ProductsListController",
                        controllerAs: "products"
                    }
                },
                params: {
                    header: null,
                    title: null,
                    subtitle: null,
                    keyword: null
                }
            })
            .state('site.products-wrapper.details', {
                url: "/details/:id",
                views: {
                    'details': {
                        templateUrl: "src/templates/site/product-details.html",
                        controller: "ProductDetailsController",
                        controllerAs: "pd"
                    }
                },
                params: {
                    breadcrumbs: null,
                    listParams: null
                }
            })


            .state('site.products', {
                url: "/products/brand/:brandId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-women', {
                url: "/products/women/:businessId/:divisionId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-handbags', {
                url: "/products/handbags/:businessId/:divisionId/:departmentId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-accessories', {
                url: "/products/accessories/:businessId/:divisionId/:departmentId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-shoes', {
                url: "/products/shoes/:businessId/:divisionId/:departmentId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.products-men', {
                url: "/products/mens-shop/:businessId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null,
                    subtitle: null
                },
            })
            .state('site.product-details', {
                url: "/products/details/:id",
                templateUrl: "src/templates/site/product-details.html",
                controller: "ProductDetailsController",
                controllerAs: "pd",
                params: {
                    breadcrumbs: null
                }
            })
            .state('site.cart', {
                url: "/cart",
                templateUrl: "src/templates/site/cart-items.html",
                controller: "CartController",
                controllerAs: "cart",
                translationPartId: 'cart'
            })
            .state('site.brands', {
                url: "/brands",
                templateUrl: "src/templates/site/brands.html",
                controller: "BrandsController",
                controllerAs: "brands",
                translationPartId: 'brand'
            })
            .state('site.manage-shippings', {
                url: "/manage-shippings/:cartId",
                templateUrl: "src/templates/site/manage-shipping.html",
                controller: "ShipmentController",
                controllerAs: "shipment",
                translationPartId: 'manage-shippings'
            })
            .state('site.order-preview', {
                url: "/order-preview/:cartId",
                templateUrl: "src/templates/site/order-summary.html",
                controller: "OrderSummaryController",
                controllerAs: "os",
                translationPartId: 'order-summary'
            })
            .state('site.featured-event', {
                url: "/featured-event/:featuredEventId",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    title: null,
                    subtitle: null
                }
            });
    }

})(window.angular);