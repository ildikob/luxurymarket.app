﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('admin.dashboard', {
                url: "/",
                templateUrl: "src/templates/admin/dashboard.html"
            })
          .state('admin.manage-offers', {
              url: "/manage-offers",
              templateUrl: "src/templates/admin/offer/manage-offers.html",
              controller: "ManageOffersController",
              controllerAs: "mo"
          })
         .state('admin.offer-details', {
             url: "/offer/details/:offerNumber",
             templateUrl: "src/templates/admin/offer/offer-details.html",
             controller: 'NewOfferDetailsController',
             controllerAs: 'offerdetails',
             translationPartId: 'offer-details'
         })
          .state('admin.account-info', {
              url: "/account-info",
              templateUrl: "src/templates/admin/account-info.html",
              controller: "AccountInfoController",
              controllerAs: "account",
              translationPartId: 'account-info'
          })
          .state('admin.manage-addresses', {
              url: "/manage-addresses",
              templateUrl: "src/templates/admin/address/manage-addresses.html",
              controller: "AddressListController",
              controllerAs: "addresses",
              translationPartId: 'manage-addresses'
          })
         .state('admin.orders', {
             url: "/orders",
             templateUrl: "src/templates/admin/order/manage-orders.html",
             controller: "OrdersController",
             controllerAs: "orders",
             translationPartId: 'manage-orders'
         })
         .state('admin.order-details', {
             url: "/orders/:orderId",
             templateUrl: "src/templates/admin/order/order-details.html",
             controller: "OrderDetailsController",
             controllerAs: "od",
             translationPartId: 'order-details'
         })
        .state('admin.seller-confirmed', {
            url: "/order-confirmed",
            templateUrl: "src/templates/admin/order/order-confirmed.html",
            translationPartId: 'order-confirmed'
        })
         .state('admin.order-files', {
             url: "/orders/:orderId/files?oc",
             templateUrl: "src/templates/admin/order/support-documents.html",
             controller: "OrderFilesController",
             translationPartId: 'order-support-documents'
         })
        .state('admin.order-pickup-information', {
            url: "/orders/:orderId/order-pickup",
            templateUrl: "src/templates/admin/order/pickup-information.html",
            controller: "OrderPickupInformationController",
            controllerAs: "orderPickup",
            translationPartId: 'order-pickup-information'
        });

    }
})(window.angular);