﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.menu')
        .controller('AdminMenuController', AdminMenuController);

    AdminMenuController.$inject = ['COMPANY_TYPES', 'AuthenticationService', '$translatePartialLoader'];

    function AdminMenuController(COMPANY_TYPES, AuthenticationService, $translatePartialLoader) {
        var vm = this;

        $translatePartialLoader.addPart('admin-left-menu');

        var menuItems = [
            //{
            //    label: 'DASHBOARD',
            //    iconCls: 'fa fa-tachometer',
            //    path: '#/admin/'
            //},
            {
                label: 'MY_ACCOUNT',
                iconCls: 'fa fa-user',
                children: [
                    {
                        label: 'ACCOUNT_INFO',
                        path: '#/admin/account-info'
                    },
                    {
                        label: 'MANAGE_ADDRESS',
                        path: '#/admin/manage-addresses'
                    }
                ]
            }, {
                label: 'OFFERS',
                iconCls: 'fa fa-tag',
                access: COMPANY_TYPES.Seller,
                children: [
                    {
                        label: 'CREATE_NEW_OFFER',
                        path: '#/admin/new-offer/upload'
                    },
                    {
                        label: 'MANAGE_OFFERS',
                        path: '#/admin/manage-offers'
                    }
                ]
            },
            {
                label: 'ORDERS',
                iconCls: 'fa fa-shopping-cart',
                children: [
                    {
                        label: 'MY_ORDERS',
                        path: '#/admin/orders'
                    }
                ]
            }
        ];

        vm.items = _.filter(menuItems, function (menuItem) {
            return _.isUndefined(menuItem.access) || menuItem.access === AuthenticationService.getCurrentUserType() || menuItem.access === COMPANY_TYPES.Both;
        });

        // functions
        vm.menuItemClick = menuItemClick;

        function menuItemClick($event, item) {
            $event.preventDefault();

            if (item.open) {
                item.open = false;
                return;
            }

            angular.forEach(vm.items, function (menuItem) {
                menuItem.open = false;
            });

            item.open = !item.open;
        }
    }

})(window.angular);