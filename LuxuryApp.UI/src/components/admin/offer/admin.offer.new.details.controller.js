﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferDetailsController', NewOfferDetailsController);

    NewOfferDetailsController.$inject = ['$scope', '_', '$stateParams', '$state', 'OfferService', 'Notification', '$timeout', 'OFFER_STATUS_TYPES', 'CURRENCY'];

    function NewOfferDetailsController($scope, _, $stateParams, $state, OfferService, Notification, $timeout, OFFER_STATUS_TYPES, CURRENCY) {
        var vm = this, _currentFilter = null;

        // view variables
        vm.details = {};
        vm.allLineBuy = false;
        vm.isOfferPreview = !_.isUndefined($stateParams.offerNumber);
        vm.readonly = {
            startDate: false,
            endDate: false,
            price: false,
            lineBuy: false,
            takeAll: false,
            includeExclude: false
        };
        vm.showButton = {
            continueProcess: true,
            newOffer: false
        };
        vm.onErrorsTab = false;

        // validation
        if ($stateParams.uniqueId === null && !vm.isOfferPreview) {
            $state.go('admin.new-offer.upload');
        }

        if (!vm.isOfferPreview) {
            _validateOffer();
        }
        else {
            _loadOfferProducts();
        }

        // update steps
        $scope.$emit('updateSteps', { number: 2, complete: false, active: true });

        // functions
        vm.continue = continueProcess;
        vm.selectAllLineBuy = selectAllLineBuy;
        vm.clickIncludeExclude = clickIncludeExclude;
        vm.filterBy = filterBy;
        vm.isStatus = isStatus;
        vm.viewProductDetails = viewProductDetails;
        vm.updateTakeAll = updateTakeAll;
        vm.updateLineBuy = updateLineBuy;
        vm.startDateOnSetTime = startDateOnSetTime;
        vm.endDateOnSetTime = endDateOnSetTime;
        vm.startDateBeforeRender = startDateBeforeRender;
        vm.endDateBeforeRender = endDateBeforeRender;
        vm.imageLoaded = imageLoaded;
        vm.changePrice = changePrice;
        vm.updatePrice = updatePrice;
        vm.checkReadonly = checkReadonly;
        vm.excludeAll = excludeAll;

        function continueProcess($event) {

            // no start date selected => block
            if (vm.details.offerStartDate === null) {
                Notification('You need to select offer start date');
                return false;
            }
            else {
                var activeProducts = 0,
                    activeWithErrorsProducts = 0;

                _.forEach(vm.details.items, function (product) {
                    if (product.include) {
                        activeProducts++;
                        if (product.hasErrors) {
                            activeWithErrorsProducts++;
                        }
                    }
                });

                // no products active => block
                if (activeProducts === 0) {
                    Notification('Your offer has 0 active products');
                    return false;
                }
                else {
                    if (activeWithErrorsProducts > 0) {
                        Notification('Your offer has invalid active products');
                        return false;
                    }
                    else {
                        $scope.$emit('updateSteps', { number: 2, complete: true, active: true });
                        if (vm.isOfferPreview) {
                            $state.go('admin.new-offer.documents', { offerId: vm.details.id, details: vm.details, documents: $stateParams.documents, uniqueId: vm.details.id });
                        }
                        else {
                            $state.go('admin.new-offer.documents', { offerId: vm.details.id, details: vm.details, documents: $stateParams.documents, uniqueId: $stateParams.uniqueId });
                        }
                    }
                }
            }
        }

        function selectAllLineBuy($event) {
            angular.forEach(vm.details.items, function (product) {
                product.isLineBuy = vm.allLineBuy;
            });

            var ids = [];
            _.forEach(vm.details.items, function (p) {
                ids.push(p.offerImportBatchItemId);
            });

            _updateLineBuys(vm.details.id, vm.allLineBuy, ids);
        }

        function clickIncludeExclude($event, product) {
            if (!vm.readonly.includeExclude) {
                product.include = !product.include;

                OfferService.updateItem({
                    offerItemId: product.offerImportBatchItemId,
                    active: product.include
                }).then(function (response) {
                    if (response.success) {
                        // update price ui
                        var productToUpdate = _.findWhere(response.data.items, { offerImportBatchItemId: product.offerImportBatchItemId });
                        angular.merge(product, productToUpdate);

                        // update totals
                        vm.details.totalCost = response.data.totalCost;
                        vm.details.totalUnits = response.data.totalUnits;
                    }
                    else {
                        // show error
                        product.include = !product.include;
                    }
                });
            }
            else {
                $event.preventDefault();
                $event.stopPropagation();
            }
        }

        function filterBy($event, filter) {
            $event.preventDefault();

            var currentFilter = _.find(vm.filters, function (f) {
                return f.type === filter.type;
            });

            angular.forEach(vm.filters, function (f) {
                f.active = false;
            });

            filter.active = true;
            _currentFilter = filter;

            vm.onErrorsTab = filter.type === 3;
        }

        function isStatus(product) {
            switch (_currentFilter.type) {
                case 1:
                    // all
                    return true;
                case 2:
                    // only successfull
                    return !product.hasErrors;
                case 3:
                    // only with errors
                    return product.hasErrors;
            }
        }

        function viewProductDetails($event, product) {
            if (vm.details.offerStatus === OFFER_STATUS_TYPES.Published ||
                vm.details.offerStatus === OFFER_STATUS_TYPES.Expired) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            else {
                var params = {
                    details: vm.details,
                    offerId: vm.details.id,
                    uniqueId: $stateParams.uniqueId,
                    offerItemId: product.offerImportBatchItemId
                };

                if (vm.details.offerStatus === OFFER_STATUS_TYPES.InProgress ||
                    vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval ||
                    vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected) {
                    params.uniqueId = vm.details.id;
                }

                $state.go('admin.new-offer.product-details', params);
            }
        }

        function updateTakeAll() {
            OfferService.updateSettings({
                id: vm.details.id,
                takeAll: vm.details.isTakeAll
            }).then(function (response) {
                if (response.success) {
                    // TODO
                }
                else {
                    // show error
                }
            });
        }

        function updateLineBuy(product) {
            var lineBuyProducts = _.filter(vm.details.items, function (product) {
                return product.isLineBuy;
            });

            vm.allLineBuy = lineBuyProducts.length === vm.details.items.length;

            _updateLineBuys(vm.details.id, product.isLineBuy, [product.offerImportBatchItemId]);
        }

        // Date time picker functions
        ///////////////////////////////////
        function startDateOnSetTime() {
            $scope.$broadcast('start-date-changed');
            _updateOfferDates();
        }

        function endDateOnSetTime() {
            $scope.$broadcast('end-date-changed');
            _updateOfferDates();
        }

        function startDateBeforeRender($event, $dates) {
            var activeDate = moment().subtract(1, 'days');

            $dates.filter(function (date) {
                return date.localDateValue() < activeDate.valueOf();
            }).forEach(function (date) {
                date.selectable = false;
            });
        }

        function endDateBeforeRender($view, $dates) {
            var activeDate = null;
            if (vm.details.offerStartDate) {
                activeDate = moment(vm.details.offerStartDate).subtract(1, $view).add(1, 'minute');
            }
            else {
                activeDate = moment().subtract(1, 'days');
            }
            $dates.filter(function (date) {
                return date.localDateValue() <= activeDate.valueOf();
            }).forEach(function (date) {
                date.selectable = false;
            });
        }
        ///////////////////////////////////////

        function imageLoaded(product) {
            product.showImage = true;
        }

        function changePrice($event, product) {
            product.offerCostNewValue = product.offerCostValue;
            product.showPriceInput = true;
            $timeout(function () {
                angular.element($event.target).parent().find('input[type=text]').focus();
            }, 100);
        }

        function updatePrice($event, product) {
            if (product.updateInProgress) {
                return false;
            }

            product.showPriceInput = false;
            var form = vm['productPriceForm' + product.offerImportBatchItemId];
            if (form.$invalid) {
                return false;
            }

            if (product.offerCostNewValue == product.offerCostValue) {
                // same value do nothing
                return false;
            }
            else {
                product.updateInProgress = true;
                //update product
                OfferService.updateItem({
                    offerItemId: product.offerImportBatchItemId,
                    offerCost: parseFloat(product.offerCostNewValue)
                }).then(function (response) {
                    product.updateInProgress = false;
                    if (response.success) {
                        // update price ui
                        var productToUpdate = _.findWhere(response.data.items, { offerImportBatchItemId: product.offerImportBatchItemId });
                        angular.merge(product, productToUpdate);

                        // update totals
                        vm.details.totalCost = response.data.sellerTotalPrice;
                        vm.details.totalUnits = response.data.totalUnits;
                    }
                    else {
                        Notification.error({ message: response.error_message });
                    }
                });
            }
        }

        function checkReadonly($event, isReadonly) {
            if (isReadonly) {
                $event.preventDefault();
                $event.stopPropagation();
                return false;
            }
        }

        function excludeAll() {
            var ids = [];
            _.forEach(vm.details.items, function (product) {
                if (product.hasErrors) {
                    ids.push(product.offerImportBatchItemId);
                }
            });

            if (ids.length > 0) {
                OfferService.batchUpdateItems({
                    offerImportBatchId: vm.details.id,
                    offerImportBatchItemIds: ids,
                    active: false
                }).then(function (response) {
                    if (response.success) {
                        _.forEach(vm.details.items, function (product) {
                            if (product.hasErrors) {
                                product.include = false;
                            }
                        });

                        vm.details.totalCost = response.data.totalCost;
                        vm.details.totalUnits = response.data.totalUnits;

                        Notification('Product with errors excluded with success');
                    }
                    else {
                        Notification('There was an error. Please try again');
                    }
                });
            }
            else {
                Notification('There are no products with errors');
            }
        }

        /// private functions
        function _validateOffer() {
            if ($stateParams.details === null) {
                $state.go('admin.new-offer.upload');
            }
            else {
                if ($stateParams.details === null) {
                    // TODO: load offer from db - should we ? 
                    $state.go('admin.new-offer.upload');
                }
                else {
                    // update view 
                    vm.details = $stateParams.details;
                    vm.details.currency = CURRENCY[vm.details.items[0].currency];
                    vm.details.offerStartDate = $stateParams.details.offerStartDate || null;
                    vm.details.offerEndDate = $stateParams.details.offerEndDate || null;
                    vm.details.isTakeAll = $stateParams.details.isTakeAll || false;

                    // process products
                    angular.forEach($stateParams.details.items, function (product) {
                        product.isLineBuy = false;
                        product.include = true;
                        product.showImage = false;
                        product.imageUrl = null;
                        product.showPriceInput = false;
                    });

                    // init filters
                    _initFilters(vm.details.items);

                    // load images
                    //_loadImages();
                }
            }
        }

        function _updateOfferDates() {
            var startDate = vm.details.offerStartDate === null ? undefined : vm.details.offerStartDate.toISOString(),
                endDate = vm.details.offerEndDate === null ? undefined : vm.details.offerEndDate.toISOString();

            if (vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired) {
                // update end date
                OfferService.updateEndDate({
                    id: vm.details.id,
                    endDate: endDate
                }).then(function (response) {
                    if (response.success) {
                        Notification("Offer dates updated successfully");
                    }
                    else {
                        // show error
                        Notification("There was an error. Please try again");
                    }
                });
            }
            else {
                OfferService.updateSettings({
                    id: vm.details.id,
                    startDate: startDate,
                    endDate: endDate
                }).then(function (response) {
                    if (response.success) {
                        Notification("Offer dates updated successfully");
                    }
                    else {
                        // show error
                        Notification("There was an error. Please try again");
                    }
                });
            }
        }

        function _initFilters(products) {
            if (products === null) {
                return;
            }

            var productsWithErrors = _.where(products, { hasErrors: true });
            var productsWithErorsCount = (productsWithErrors || []).length;

            vm.filters = [
            {
                label: 'STEP_TWO_FILTER_ALL',
                count: products.length,
                active: true,
                cssClass: '',
                type: 1
            },
            {
                label: 'STEP_TWO_FILTER_SUCCESS',
                count: products.length - productsWithErorsCount,
                active: false,
                cssClass: '',
                type: 2
            },
            {
                label: 'STEP_TWO_FILTER_FAILED',
                count: productsWithErorsCount,
                active: false,
                cssClass: 'message-error',
                type: 3
            }];

            _currentFilter = vm.filters[0];
        }

        function _updateLineBuys(offerId, lineBuy, ids) {
            OfferService.updateLineBuy({
                offerId: offerId,
                lineBuy: lineBuy,
                ids: ids
            }).then(function (response) {
                if (response.success) {
                    // TODO
                }
                else {
                    // show error
                    // TODO
                }
            });
        }

        function _loadOfferProducts() {
            OfferService.getProducts($stateParams.offerNumber)
                .then(function (response) {
                    if (response.success) {
                        vm.details = response.data;
                        vm.details.currency = CURRENCY[vm.details.items[0].currency];
                        _initFilters(vm.details.items);

                        _.forEach(vm.details.items, function (product) {
                            product.isLineBuy = false;
                            product.include = true;
                            product.showImage = false;
                            product.imageUrl = null;
                            product.showPriceInput = false;
                        });

                        vm.details.offerStartDate = _.isUndefined(vm.details.startDate) ? null : new Date(vm.details.startDate);
                        vm.details.offerEndDate = _.isUndefined(vm.details.endDate) ? null : new Date(vm.details.endDate);
                        vm.details.isTakeAll = vm.details.takeAll || false;

                        vm.readonly = {
                            startDate: vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired || vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected,
                            endDate: vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected,
                            price: vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired || vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected,
                            lineBuy: vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired || vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected,
                            takeAll: vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired || vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected,
                            includeExclude: vm.details.offerStatus === OFFER_STATUS_TYPES.Published || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired || vm.details.offerStatus === OFFER_STATUS_TYPES.PendingApproval || vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected
                        };

                        vm.showButton.continueProcess = vm.details.offerStatus === OFFER_STATUS_TYPES.InProgress;
                        vm.showButton.newOffer = vm.details.offerStatus === OFFER_STATUS_TYPES.Rejected || vm.details.offerStatus === OFFER_STATUS_TYPES.Live || vm.details.offerStatus === OFFER_STATUS_TYPES.Expired;
                    }
                    else {
                        // show error
                    }
                });
        }
    }

})(window.angular);