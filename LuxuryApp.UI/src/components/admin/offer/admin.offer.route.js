﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
         .state('admin.new-offer', {
             url: "/new-offer",
             abstract: true,
             templateUrl: "src/templates/admin/offer/newoffer-index.html",
             controller: 'NewOfferController',
             controllerAs: 'newoffer',
         })
        .state('admin.new-offer.upload', {
            url: "/upload",
            templateUrl: "src/templates/admin/offer/upload-offer.html",
            controller: 'UploadNewOfferController',
            translationPartId: 'offer-upload'
        })
        .state('admin.new-offer.details', {
            url: "/details/:offerId",
            templateUrl: "src/templates/admin/offer/offer-details.html",
            controller: 'NewOfferDetailsController',
            controllerAs: 'offerdetails',
            translationPartId: 'offer-details',
            params: {
                details: null,
                offerId: null,
                documents: null,
                uniqueId: null
            }
        })
        .state('admin.new-offer.product-details', {
            url: "/:offerId/product-details/:offerItemId",
            templateUrl: "src/templates/admin/offer/offer-product-details.html",
            controller: 'NewOfferProductDetailsController',
            controllerAs: 'productdetails',
            translationPartId: 'offer-product-details',
            params: {
                details: null,
                offerId: null,
                offerItemId: null,
                uniqueId: null
            }
        })
        .state('admin.new-offer.product-details-error', {
            url: "/:offerItemId/report-error",
            templateUrl: "src/templates/admin/offer/report-error.html",
            controller: 'NewOfferProductErrorController',
            controllerAs: 'producterror',
            translationPartId: 'offer-report-error',
            params: {
                details: null,
                offerId: null,
                offerItemId: null,
                productDetails: null,
                documents: null,
                uniqueId: null
            }
        })
        .state('admin.new-offer.documents', {
            url: "/:offerId/supporting-documents",
            templateUrl: "src/templates/admin/offer/support-documents.html",
            controller: 'NewOfferDocumentsController',
            translationPartId: 'offer-documents',
            params: {
                details: null,
                offerId: null,
                documents: null,
                uniqueId: null
            }
        })
        .state('admin.new-offer.summary', {
            url: "/:offerId/summary",
            templateUrl: "src/templates/admin/offer/offer-summary.html",
            controller: 'NewOfferSummaryController',
            controllerAs: 'offersummary',
            translationPartId: 'offer-summary',
            params: {
                details: null,
                offerId: null,
                documents: null,
                uniqueId: null
            }
        })
        .state('admin.new-offer.published', {
            url: "/published",
            templateUrl: "src/templates/admin/offer/offer-published.html"
        });
    }

})(window.angular);