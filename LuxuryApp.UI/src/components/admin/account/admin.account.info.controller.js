﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.account')
        .controller('AccountInfoController', AccountInfoController);

    AccountInfoController.$inject = ['$rootScope', '$stateParams', '_', 'AccountService', 'Notification'];

    function AccountInfoController($rootScope, $stateParams, _, AccountService, Notification) {
        var vm = this;

        // private vars

        // view models
        vm.isLoading = false;
        vm.isLoadingSumbitChanges = false;
        vm.isLoadingChangePassword = false;
        vm.accountPreviewModel = {};
        vm.accountModel = {};
        vm.changePasswordModel = {};

        // functions
        vm.open = open;
        vm.submit = submit;
        vm.changePassword = changePassword;
        vm.cancel = cancel;

        // init
        _loadInfo();

        //events captured

        // functions
        function open(element) {
            $(element).collapse('show');
        }

        function submit(contactName, phoneNumber) {
            vm.isLoadingSumbitChanges = true;
            var params = {};
            params.customerId = vm.accountPreviewModel.customerId;
            params.contactPhoneNumber = phoneNumber;
            params.firstName = null;
            params.lastName = null;

            if (!_.isNull(contactName)) {
                var contactNameSplitted = contactName.split(/\s+/);
                params.firstName = contactNameSplitted[0];
                params.lastName = contactNameSplitted.slice(1).join(' ');
            }

            AccountService.submitChanges(params)
               .then(function (response) {
                   vm.isLoadingSumbitChanges = false;
                   if (response.success) {
                       if (!_.isNull(contactName)) {
                           cancel('#contact-name');
                           vm.accountPreviewModel.contactName = contactName;
                           $rootScope.$broadcast('changeDisplayName', contactName);
                       }
                       if (!_.isNull(phoneNumber)) {
                           cancel('#phone');
                           vm.accountPreviewModel.contactPhoneNumber = phoneNumber;
                       }
                       Notification('Account info changed with success.');
                   }
                   else {
                       // display error
                   }
               });
        }

        function changePassword(oldPassword, newPassword) {
            vm.isLoadingChangePassword = true;
            var params = {};
            params.email = vm.accountPreviewModel.email;
            params.oldPassword = oldPassword;
            params.newPassword = newPassword;
            params.confirmPassword = newPassword;

            AccountService.changePassword(params)
               .then(function (response) {
                   vm.isLoadingChangePassword = false;
                   if (response.success) {
                       cancel('#password');
                       vm.changePasswordForm.oldPassword.$setValidity("checkOldPassword", true);
                       Notification('Password changed with success.');
                   }
                   else {
                       vm.changePasswordForm.oldPassword.$setValidity("checkOldPassword", false);
                   }
               });
        }

        function cancel(element) {
            $(element).collapse('hide');
        }

        // private functions
        function _loadInfo() {
            vm.isLoading = true;
            AccountService.getCurrent()
               .then(function (response) {
                   vm.isLoading = false;
                   if (response.success) {
                       vm.accountPreviewModel.customerId = response.data.data.customerId;
                       vm.accountPreviewModel.contactName = response.data.data.firstName + ' ' + response.data.data.lastName;
                       vm.accountPreviewModel.companyName = response.data.data.companies[0].companyName;
                       vm.accountPreviewModel.email = response.data.data.email;
                       vm.accountPreviewModel.contactPhoneNumber = response.data.data.phone;
                   }
                   else {
                       // display error
                   }
               });
        }
    }
})(window.angular);