﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfigs($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/site/home");

        $stateProvider
            .state('auth', {
                url: "/auth",
                abstract: true,
                templateUrl: "src/templates/auth/index.html"
            })
            .state('site', {
                url: "/site",
                templateUrl: "src/templates/site/index.html",
                abstract: true
            })
            .state('admin', {
                url: "/admin",
                abstract: true,
                templateUrl: "src/templates/admin/index.html"
            });
    }

})(window.angular);