﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket', [
        'luxurymarket.components',
        'luxurymarket.services',
        'luxurymarket.directives',
        'luxurymarket.libs',
        'ui.router',
        'ngMessages',
        'ngFileUpload',
        'LocalStorageModule',
        'angular.filter',
        'pascalprecht.translate',
        'ngCookies',
        'ngSanitize',
        'ui.bootstrap.datetimepicker',
        'ui-notification',
        'ui.bootstrap',
        'dc.endlessScroll',
        'ct.ui.router.extras',
        'slick',
        'timer'
    ]);

})(window.angular);