﻿using LuxuryApp.Auth.Core.Attributes;
using System;

namespace LuxuryApp.Auth.Core.Entities
{
    [Table("app.RefreshTokens")]
    public class RefreshToken
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public DateTimeOffset IssuedUtc { get; set; }
        public DateTimeOffset ExpiresUtc { get; set; }
        public string ProtectedTicket { get; set; }
    }
}