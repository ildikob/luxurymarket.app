﻿

namespace LuxuryApp.Auth.Core.Interfaces
{
    public interface IConnectionStringProvider
    {
        string ReadWriteConnectionString { get; }
    }
}
