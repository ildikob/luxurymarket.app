﻿using Autofac;
using Luxury.FtpUploadImages.Implementations;
using LuxuryApp.Contracts.Agents;
using LuxuryApp.Contracts.BackgroudServices;
using LuxuryApp.Contracts.Interfaces;
using LuxuryApp.Contracts.Repository;
using LuxuryApp.Core.DataAccess;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Agents;
using LuxuryApp.Core.Infrastructure.Logging;
using LuxuryApp.Cdn.SqlRepository;
using System.Configuration;
using LuxuryApp.Cdn.CdnServers.Repository;
using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.CdnServers.Services;
using LuxuryApp.CDN.ImageCdnServers.Services;
using LuxuryApp.Cdn.Image.Contracts;
using System.Collections.Generic;
using LuxuryApp.Cdn.CdnServers;
using LuxuryApp.Cdn.Contracts.Model;

namespace Luxury.FtpUploadImages
{
    public class DependencyBuilder
    {
        public IContainer GetDependencyContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LuxuryLogger>().As<ILogger>();
            builder.RegisterType<LuxuryMarketConnectionStringProvider>().As<IConnectionStringProvider>();
            builder.RegisterType<FtpUploadImageAgent>().As<IFtpUploadImageAgent>();
            builder.RegisterType<FtpUploadImagesRepository>().As<IFtpUploadImagesRepository>();

            builder.RegisterType<WorkerConfigAppSettings>().As<IWorkerConfig>();
            builder.RegisterType<FtpUploadImageWorker>().As<IWorker>();

            builder.RegisterType<ProductRepository>().As<IProductRepository>();

            //amazon
            var connectionStringName = ConfigurationManager.AppSettings["CDNConnectionString"];
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;

            var amazonS3CdnRepository = new AmazonS3CdnSettingsRepository(connectionString);
            builder.RegisterInstance(amazonS3CdnRepository).As<IMultiCdnSettingsRepository<AmazonS3CdnSettings>>();

            var amazonSettings = amazonS3CdnRepository.GetAllCdnSettings();
            const string cdnComponetsKey = "cdnComp";

            foreach (var setting in amazonSettings)
            {
                builder.RegisterInstance(setting).As<ICdnSettings>();
                builder.Register(c => new AmazonS3CdnService(c.Resolve<ICdnAuthor>(), setting))
                    .Named<IFileCdnService<ICdnAuthor, ICdnSettings>>(cdnComponetsKey).As<IFileCdnService<ICdnAuthor, ICdnSettings>>();
            }
            var author = new CdnAuthor { UserId = -1, UserIdSource = "LuxuryFTPUploadImages" };
            builder.RegisterInstance(author).As<ICdnAuthor>();

            //builder.Register(c => new MultiCdnService(
            //  author,
            //  new NullCdnSettings(),
            //  c.Resolve<IMultiCdnRepository>(),
            //  c.ResolveNamed<IEnumerable<IFileCdnService<ICdnAuthor, ICdnSettings>>>(cdnComponetsKey)))
            //  .As<IFileCdnService<ICdnAuthor, ICdnSettings>>();


            builder.RegisterType<ImageCdnService>().As<IImageCdnService>();
            builder.RegisterType<ServiceUploadImages>().AsSelf();
            var container = builder.Build();
            return container;
        }
    }
}
