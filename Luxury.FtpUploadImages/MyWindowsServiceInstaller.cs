﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Luxury.FtpUploadImages
{
    [RunInstallerAttribute(true)]
    public class MyWindowsServiceInstaller: Installer
    {
        public MyWindowsServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = "Luxury Upload Images Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = "LuxuryUploadImagesService";
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
