﻿
namespace LuxuryApp.Resources.Errors
{
    public static class ResourceErrorMessageProcessor
    {
        public static string ProcessErrorMessage(this string errorType)
        {
            // var rm = new ResourceManager("LuxuryApp.Resources.Errors.ErrorMessages", Assembly.GetExecutingAssembly());

            var value = ErrorMessages.ResourceManager.GetString(errorType);

            return value;
        }
    }
}
