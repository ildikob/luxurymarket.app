﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SeleniumAppTests.PageObjects.authentication
{
    public enum CretateAccountTypes
    {
        BUYER, SELLER
    }
    public class CreateAccountPage : BasePage
    {

        #region Web Elements identifiers

        private String _registrationFormID = "registrationform";
        private String _createBuyerID = "buyer";
        private String _createSellerID = "seller";
        private String _contactNameID = "contactname";
        private String _companyNameID = "companyname";
        private String _streetAddressID = "streetaddrs";
        private String _cityID = "city";
        private String _countryID = "country";
        private String _stateID = "state";
        private String _zipcodeID = "zip";
        //private String _retailID = "retailID";
        private String _phoneNumberID = "phonenumber";
        //private String _companywebsiteID = "companywebsite";
        private String _contactEmailID = "contactemail";
        private String _confirmEmailID = "confirmcontactemail";
        //private String _descriptionID = "businessdescription";
        //private String _termConditionsCheckboxXpath = "/html/body/ui-view/ui-view/div/div[2]/div[2]/form/div[9]/div/div/label/span";
        //private String _termsAndConditionsPageID = "termsconditionscheckbox";
        private String _submitRequestID = "submit";
        private String _goLoginCreateAccID = "gotologincreateacc";
        private String _confirmationMsgID = "messageform";
        private String _goToLoginAfterCreateAccID = "gotologin";

        #endregion

        #region Web Elements

        private IWebElement registrationPage;
        private IWebElement buyerAccount;
        private IWebElement sellerAccount;
        private IWebElement contactName;
        private IWebElement companyName;
        private IWebElement address;
        private IWebElement city;
        private SelectElement country;
        private SelectElement state;
        private IWebElement zip;
        //private IWebElement retailNumber;
        private IWebElement phoneNumber;
        //private IWebElement website;
        private IWebElement contactEmail;
        private IWebElement confirmEmail;
        //private IWebElement description;
        //private IWebElement termConditionsCheckbox;
        //private IWebElement termsConditionsPage;
        private IWebElement submit;
        private IWebElement backToLogin;
        private IWebElement confirmationMessage;
        private IWebElement goToLoginFromMsg;

        #endregion

        public CreateAccountPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_registrationFormID)));
            InitWebElements();
        }

        public void SetAccountType(CretateAccountTypes accountType)
        {
            switch(accountType)
            {
                case CretateAccountTypes.BUYER:
                    buyerAccount.Click();
                    break;
                case CretateAccountTypes.SELLER:
                    sellerAccount.Click();
                    break;              
            }
        }

        public void SetContactName(string value)
        {
            contactName.SendKeys(value);
        }

        public void SetCompany(string value)
        {
            companyName.SendKeys(value);
        }

        public void SetAddress(string value)
        {
            address.SendKeys(value);
        }

        public void SetCity(string value)
        {
            city.SendKeys(value);
        }

        public void SetContryDropdown(string value)
        {
        //    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //    wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("/option[contains(@value, 'number')]")));

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(drv => drv.FindElements(By.CssSelector("#" + _countryID + " option")).Count > 1);

            country.SelectByText(value);           
        }

        public void SetStateDropdown(string value)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(drv => drv.FindElements(By.CssSelector("#" + _stateID + " option")).Count > 1);

            state.SelectByText(value);
        }

        public void SetZipCode(string value)
        {
            zip.SendKeys(value);
        }

        //public void SetRetail(string value)
        //{
        //    retailNumber.SendKeys(value);
        //}

        public void SetPhone(string value)
        {
            phoneNumber.SendKeys(value);
        }

        //public void SetWebsite(string value)
        //{
        //    website.SendKeys(value);
        //}

        public void SetEmail(string value)
        {
            contactEmail.SendKeys(value);
        }

        public void SetConfirmEmail(string value)
        {
            confirmEmail.SendKeys(value);
        }

        //public void SetDescription(string value)
        //{
        //    description.SendKeys(value);
        //}

        //public void TermsCheckbox()
        //{
        //    termConditionsCheckbox.Click();
        //}

        //public void TermsConditionsPage()
        //{
        //    termsConditionsPage.Click();
        //}

        public void SubmitClick()
        {
            submit.Click();
            
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id(_confirmationMsgID), "Your request was sent for processing."));
        }

        //public void BackToLogInClick()
        //{
        //    backToLogin.Click();
        //}

        private void GoToLoginClick()
        {
            goToLoginFromMsg.Click();
        }

        public void newTab()
        {
            IWebElement newTab = WebDriverHelper.Driver.FindElementByXPath("/html/body");
            newTab.SendKeys(Keys.Control + 't');
        }

        public CreateAccountPage createBuyerAcc(CreateAccountPage createaccPage, string contactBuyerName, string companyBuyerName, string emailBuyer)
        {           
            createaccPage.SetAccountType(CretateAccountTypes.BUYER);
            createaccPage.SetContactName(contactBuyerName);
            createaccPage.SetCompany(companyBuyerName);
            createaccPage.SetAddress("Aut buyer park ave");
            createaccPage.SetCity("Aut buyer NY");
            createaccPage.SetContryDropdown("USA");
            createaccPage.SetStateDropdown("New York");
            createaccPage.SetZipCode("10018");
            //createaccPage.SetRetail("Buyer Automation retail ID");
            createaccPage.SetPhone("555");
            //createaccPage.SetWebsite("www.buyerautomation.com");
            createaccPage.SetEmail(emailBuyer);
            createaccPage.SetConfirmEmail(emailBuyer);
            //createaccPage.SetDescription("Buyer Test description");
            //createaccPage.TermsCheckbox();
            createaccPage.SubmitClick();

            return null;
        }

        public CreateAccountPage createSellerAccount(CreateAccountPage createaccPage, string contactSellerName, string companySellerName, string emailSeller)
        {
            createaccPage.SetAccountType(CretateAccountTypes.SELLER);
            createaccPage.SetContactName(contactSellerName);
            createaccPage.SetCompany(companySellerName);
            createaccPage.SetAddress("Aut seller fifht ocean ave");
            createaccPage.SetCity("Aut seller NY");
            createaccPage.SetContryDropdown("USA");
            createaccPage.SetStateDropdown("New York");
            createaccPage.SetZipCode("10015");
            //createaccPage.SetRetail("Seller Automation retail ID");
            createaccPage.SetPhone("555");
            //createaccPage.SetWebsite("www.sellerautomation.com");
            createaccPage.SetEmail(emailSeller);
            createaccPage.SetConfirmEmail(emailSeller);
            //createaccPage.SetDescription("Seller Test description");
            //createaccPage.TermsCheckbox();
            createaccPage.SubmitClick();

            return null;
        }

        private void InitWebElements()
        {
            registrationPage = WebDriver.FindElementById(_registrationFormID);
            buyerAccount = WebDriver.FindElementById(_createBuyerID);
            sellerAccount = WebDriver.FindElementById(_createSellerID);
            contactName = WebDriver.FindElementById(_contactNameID);
            companyName = WebDriver.FindElementById(_companyNameID);
            address = WebDriver.FindElementById(_streetAddressID);
            city = WebDriver.FindElementById(_cityID);
            country = new SelectElement(WebDriver.FindElementById(_countryID));
            state = new SelectElement(WebDriver.FindElementById(_stateID));
            zip = WebDriver.FindElementById(_zipcodeID);
            //retailNumber = WebDriver.FindElementById(_retailID);
            phoneNumber = WebDriver.FindElementById(_phoneNumberID);
            //website = WebDriver.FindElementById(_companywebsiteID);
            contactEmail = WebDriver.FindElementById(_contactEmailID);
            confirmEmail = WebDriver.FindElementById(_confirmEmailID);
            //description = WebDriver.FindElementById(_descriptionID);
            //termConditionsCheckbox = WebDriver.FindElementByXPath(_termConditionsCheckboxXpath);
            //termsConditionsPage = WebDriver.FindElementById(_termsAndConditionsPageID);
            submit = WebDriver.FindElementById(_submitRequestID);
            backToLogin = WebDriver.FindElementById(_goLoginCreateAccID);          
        }
    }
}
