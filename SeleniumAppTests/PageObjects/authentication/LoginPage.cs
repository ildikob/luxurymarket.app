﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authenticated.home;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authentication
{
    public enum LoginErrorTypes
    {
        [Description("The user name or password is incorrect.")]
        InvalidAccount,
        [Description("The user name or password is incorrect.")]
        InvalidPassword,
        [Description("Password is required")]
        NoPassword,
        [Description("Email is required")]
        NoEmail,
        [Description("Email is invalid")]
        InvalidEmail
    }

    public enum LoginAccountTypes
    {
        Buyer,
        Seller,
    }

    public class LoginPage : BasePage
    {
        #region Web Elements Identifiers

        private String _loginFormID = "loginform";
        private String _usernameInputID = "emailaddress";
        private String _passwordInputID = "password";
        private String _loginButtonID = "login";
        private String _forgotPassID = "forgotpassword";
        private String _requestNewAccID = "requestnewaccount";
        private String _errorMsgClass = "submit-error";
        private String _invalidDetailsClass = "form-group";
        private String _browseFileClass = "drag-and-drop-zone";
        private String _siteMenuID = "sitemenu";
        private String _termspopupClass = "modal-content";
        private String _acceptedTermsXpath = ".//div[contains(@class, 'col-md-12')]//..//button[contains(@class, 'btn-lm')]";

        #endregion

        #region Web Elements 

        private IWebElement loginForm;
        private IWebElement username;
        private IWebElement password;
        private IWebElement loginButton;
        private IWebElement forgotPassword;
        private IWebElement requestAccount;
        private IWebElement errorMessage;
        private IWebElement invalidDetails;

        #endregion

        /// <summary>
        ///  This is a CONSTRUCTOR
        ///  - When you create a class of type LoginPage (Ex: new LoginPage(driver)) this method is invoked.
        ///  - By adding :base(driver) code to the definition of the contructor means that 
        ///  the constructor for the base class is called before this one. !!! Meaning the BasePage class constructor.
        /// </summary>
        /// <param name="driver"></param>
        public LoginPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));
            initWebElements();
        }

        public LoginPage setUsername(string value, bool expectsError = false, LoginErrorTypes errorType = LoginErrorTypes.NoEmail)
        {
            username.Clear();
            username.SendKeys(value);

            if (expectsError)
            {
                // check submit error /html/body/ui-view/ui-view/div/div/div[2]/form/div[3]/div/div
                var errorDescription = Utils.GetEnumDescription(errorType);
                var errorWebElement = password.FindElement(By.XPath("/html/body/ui-view/ui-view/div/div/div[2]/form/div[2]/div/div"));
                if (errorWebElement != null)
                {
                    if (errorWebElement.Text.Equals(errorDescription))
                    {
                        // check is ok => return null instead of new page. We are still on login page.
                        return null;
                    }
                    else
                    {
                        throw new Exception("Error message is different than expected!!!");
                    }
                }
                else
                {
                    // not found / not appeared => test failed
                    throw new Exception("Error message didn't appear!!!");
                }
            }

            return new LoginPage(WebDriverHelper.Driver);
        }

        public LoginPage setPassword(string value, bool expectsError = false, LoginErrorTypes errorType = LoginErrorTypes.NoPassword)
        {
            password.Clear();
            password.SendKeys(value);

            if (expectsError)
            {
                // check submit error /html/body/ui-view/ui-view/div/div/div[2]/form/div[3]/div/div
                var errorDescription = Utils.GetEnumDescription(errorType);
                var errorWebElement = password.FindElement(By.XPath(".."));
                if (errorWebElement != null)
                {
                    if (errorWebElement.Text.Equals(errorDescription))
                    {
                        // check is ok => return null instead of new page. We are still on login page.
                        return null;
                    }
                    else
                    {
                        throw new Exception("Error message is different than expected!!!");
                    }
                }
                else
                {
                    // not found / not appeared => test failed
                    throw new Exception("Error message did not appear!!!");
                }
            }

            return new LoginPage(WebDriverHelper.Driver);

        }

        /// <summary>
        /// This is a METHOD with no parameters.
        /// - In order to call this message you would need to have a LoginPage class instance.
        /// -- Sample Code:
        /// -------------------------------------------------------
        /// -- // driver is a variable defined before this lines
        /// -- LoginPage loginPage = new LoginPage(driver);
        /// -- loginPage.loginClick();
        /// -------------------------------------------------------
        /// </summary>
        public BasePage loginClick(LoginAccountTypes accType, bool expectsError = false, LoginErrorTypes errorType = LoginErrorTypes.InvalidAccount)
        {
            loginButton.Click();

            if (expectsError)
            {
                var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_errorMsgClass)));

                var errorDescription = Utils.GetEnumDescription(errorType);
                var errorWebElement = loginForm.FindElement(By.ClassName(_errorMsgClass));

                if (errorWebElement != null)
                {
                    if (errorWebElement.Text.Equals(errorDescription))
                    {
                        // check is ok => return null instead of new page. We are still on login page.
                        return null;
                    }
                    else
                    {
                        throw new Exception("Error message is different than expected!!!");
                    }
                }
                else
                {
                    // not found / not appeared => test failed
                    throw new Exception("Error message did not appear!!!");
                }
            }

            var termsbox = WebDriverHelper.Driver.FindElementsByClassName(_termspopupClass).ToList();
            termsbox.Count();

            if (termsbox.Count() > 0)
            {
                if (accType == LoginAccountTypes.Seller)
                {
                    WebDriverHelper.Driver.FindElementByXPath(_acceptedTermsXpath).Click();

                    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_browseFileClass)));

                    return new UploadOfferAndDetails(WebDriverHelper.Driver);
                }
                else
                {
                    WebDriverHelper.Driver.FindElementByXPath(_acceptedTermsXpath).Click();

                    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_siteMenuID)));

                    return new HomePage(WebDriverHelper.Driver);
                }
            }
            else
            {
                if (accType == LoginAccountTypes.Seller)
                {
                    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_browseFileClass)));

                    return new UploadOfferAndDetails(WebDriverHelper.Driver);
                }
                else
                {
                    var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_siteMenuID)));

                    return new HomePage(WebDriverHelper.Driver);
                }
            }

        }

        /// <summary>
        /// This is a METHOD that returns a new instance of a Page Object.
        /// NOTE: THIS TYPE OF METHOD IS CREATED AND USED WHEN THE ACTION WILL NAVIGATE TO ANOTHER PAGE OR WHEN IT NEEDS TO RETURN A NEW PAGE OBJECT.
        /// -- Sample Code:
        /// -------------------------------------------------------
        /// -- // driver is a variable defined before this lines
        /// -- LoginPage loginPage = new LoginPage(driver);
        /// -- ForgotPasswordPage forgotPasswordPage = loginPage.forgotPassClick();
        /// -- // at this point you have a new page to work with and you can use the methods now
        /// -- forgotPasswordPage.sampleMethod();
        /// -------------------------------------------------------
        /// </summary>
        /// <returns></returns>
        public ForgotPasswordPage forgotPassClick()
        {
            forgotPassword.Click();

            return new ForgotPasswordPage(WebDriverHelper.Driver);
        }

        public CreateAccountPage newAccountClick()
        {
            requestAccount.Click();

            return new CreateAccountPage(WebDriverHelper.Driver);
        }

        private void initWebElements()
        {
            loginForm = WebDriver.FindElementById(_loginFormID);
            username = WebDriver.FindElementById(_usernameInputID);
            password = WebDriver.FindElementById(_passwordInputID);
            loginButton = WebDriver.FindElementById(_loginButtonID);
            forgotPassword = WebDriver.FindElementById(_forgotPassID);
            requestAccount = WebDriver.FindElementById(_requestNewAccID);
            errorMessage = WebDriver.FindElementByClassName(_errorMsgClass);
            invalidDetails = WebDriver.FindElementByClassName(_invalidDetailsClass);
        }
    }
}