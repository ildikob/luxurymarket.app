﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{
    public class OffersRequest
    {
        #region Web Element Identifiers

        private String _searchFieldID = "search";
        private String _offersBodyXpath = "/html/body/main/div/div[2]/div/table/tbody";
        private String _viewProductsXpath = "//*[contains(text(), 'View Products')]";

        #endregion

        #region Web Elements

        private IWebElement searchfield;
        private IWebElement offersbody;
        private IWebElement viewproductbtn;

        #endregion

        public OffersRequest()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_offersBodyXpath)));
            InitWebElements();
        }

        public void searchOffer(int searchofferid)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_offersBodyXpath)));

            WebDriverHelper.Driver.FindElementById(_searchFieldID).SendKeys(searchofferid.ToString());
            SendKeys.SendWait(@"{Enter}");

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(_viewProductsXpath)));
        }

        public OfferImportProductsPage checkOfferIdAndView(int offeridcms)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_offersBodyXpath)));

            WebDriverHelper.Driver.FindElementByXPath(String.Format("//*[@{0} = '{1}']",
                                   "data-offer-id",
                                   offeridcms)).Click();

            return new OfferImportProductsPage();
        }

        public void InitWebElements()
        {
            searchfield = WebDriverHelper.Driver.FindElementById(_searchFieldID);
            offersbody = WebDriverHelper.Driver.FindElementByXPath(_offersBodyXpath);
            viewproductbtn = WebDriverHelper.Driver.FindElementByXPath(_viewProductsXpath);               
        }

    }
}
