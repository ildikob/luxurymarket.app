﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{

    public class CmsHeader //: BaseCmsPage
    {
        #region Web Elements Identifiers 

        private String _headerMenuClass = "hide-on-med-and-down";

        #endregion

        #region Web Elements

        private IWebElement menu;

        #endregion

        //constructor
        public CmsHeader(RemoteWebDriver driver) //: base(driver)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_headerMenuClass)));
            InitWebElements();
        }

        public PartnerRequestsPage Navigate(CMSPages page)
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));

            switch (page)
            {
                case CMSPages.Offers:

                    menu.FindElement(By.PartialLinkText("Offers")).Click();
                    menu.FindElement(By.PartialLinkText("Requests")).Click();

                    return null;

                case CMSPages.OffersAvailable:

                    InitWebElements();
                    menu.FindElement(By.PartialLinkText("Offers")).Click();
                    Thread.Sleep(500);
                    menu.FindElement(By.PartialLinkText("Offers Available")).Click();

                    return null;

                case CMSPages.Partners:

                    InitWebElements();
                    menu.FindElement(By.PartialLinkText("Partners")).Click();
                    menu.FindElement(By.PartialLinkText("Requests")).Click();

                    return new PartnerRequestsPage();

                case CMSPages.ExistingPartners:

                    InitWebElements();
                    menu.FindElement(By.PartialLinkText("Partners")).Click();

                    wait.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Existing Partners")));

                    menu.FindElement(By.PartialLinkText("Existing Partners")).Click();
                    
                    return null;

                case CMSPages.UserLogout:

                    InitWebElements();
                    menu.FindElement(By.ClassName("profile")).Click();

                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("logout")));

                    menu.FindElement(By.ClassName("logout")).Click();

                    return null;
            }
            return null;
        }

        public PartnerRequestsPage partnersMenuClick(CMSPages page)
        {
            switch (page)
            {
                case CMSPages.Partners:

                    menu.FindElement(By.PartialLinkText("Partners")).Click();

                    return new PartnerRequestsPage();
            }

            return null;
        }

        public PartnerRequestsPage ExistingPartners(CMSPages page)
        {
            switch (page)
            {
                case CMSPages.ExistingPartners:

                    menu.FindElement(By.PartialLinkText("Partners")).Click();
                    menu.FindElement(By.PartialLinkText("Existing Partners")).Click();

                    return new PartnerRequestsPage();
            }

            return null;

            //var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Existing Partners")));

            //WebDriverHelper.Driver.FindElementByLinkText("Existing Partners").Click();
        }

        public void InitWebElements()
        {
            menu = WebDriverHelper.Driver.FindElementByClassName(_headerMenuClass);
        }

        public enum CMSPages
        {
            Dashboard,
            Catalog,
            Partners,
            PartnerRequests,
            AddNewPartner,
            ExistingPartners,
            Offers,
            OffersRequest,
            OffersAvailable,
            Orders,
            Settings,
            UserLogout
        }


    }
}
