﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{
    public class PartnerRequestsPage
    {

        #region Web Element Identifiers

        private String _searchID = "search";
        private String _contactClass = "contact";
        private String _approveID = "btnAprove";
        private String _declineID = "btnDecline";
        private String _confirmRequestPopupID = "confirmAction";
        private String _yesApprovalID = "yesAnswer";
        private String _requestStatusClass = "select-dropdown";
        private String _declinedRequestsListXpath = "/html/body/main/div/div[2]/div[1]/div/div/div/ul/li[3]/span";
        private String _declinedStatusXpath = "/html/body/main/div/div[2]/div[1]/table/tbody/tr[1]/td[1]/div"; // class chip declined 
        private String _requestsHeaderClass = "page-header";
        private String _companyLine = "company";
        private String _toasterMsgID = "toast-container";

        #endregion


        #region Web Elements
       
        //private IWebElement searchInput;
        //private IWebElement contactVerification;
        //private IWebElement approve;
        //private IWebElement decline;
        //private IWebElement requestPopup;
        //private IWebElement yesApproval;
        //private IWebElement approveMessage;

        #endregion

        public void searchField(string value)
        {
            WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(value);
            WebDriverHelper.Driver.FindElementById(_searchID).SendKeys(Keys.Enter);

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_companyLine), value));
        }
        //  var page = ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_requestsHeaderClass), "Partner Requests");
        //    if (page == null)
        //    {
        //        var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //        wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_contactClass), value));
        //    }
        //    else
        //    {
        //        var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
        //        wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_companyLine), value));
        //    }
        //}

        public void approveClick()
        {
            WebDriverHelper.Driver.FindElementById(_approveID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_confirmRequestPopupID)));
        }

        public void declineClick()
        {
            WebDriverHelper.Driver.FindElementById((_declineID)).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_confirmRequestPopupID)));
        }

        public void yesPopupClick()
        {
            WebDriverHelper.Driver.FindElementById(_yesApprovalID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id(_yesApprovalID)));            
        }

        public void statusRequestsDropdown()
        {
            WebDriverHelper.Driver.FindElementByClassName(_requestStatusClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_declinedRequestsListXpath)));
        }

        public void declinedRequestsPage()
        {
            WebDriverHelper.Driver.FindElementByXPath(_declinedRequestsListXpath).Click();
            //<div id = "toast-container"  Company approved with success. >< div class="toast" style="touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); top: 0px; opacity: 1;">I am a toast</div></div>
        }

        public void allRequests()
        {
            statusRequestsDropdown();
            WebDriverHelper.Driver.FindElementByLinkText("All").Click();
        }

        public CmsHeader approveRequest(string value)
        {
            searchField(value);
            approveClick();
            yesPopupClick();

            return new CmsHeader(WebDriverHelper.Driver);
        }

        public void declineRequest(string value)
        {
            searchField(value);
            declineClick();
            yesPopupClick();
        }

        public CmsHeader checkDeclinedRequest(string value)
        {
            statusRequestsDropdown();
            declinedRequestsPage();
            searchField(value);

            return new CmsHeader(WebDriverHelper.Driver);
        }
     }
}
