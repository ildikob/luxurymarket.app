﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.CMS.authenticated
{
    public class OfferImportProductsPage
    {
        #region Web Element Identifiers

        private String _descriptionXpath = "//*[contains(text(), 'Description')]";
        private String _supportingDocsXpath = "//*[contains(text(), 'Supporting')]";
        private String _approvebtnID = "btnAprove";
        private String _declinebtnID = "btnDecline";
        private String _importBodyID = "description";
        private String _offersBodyXpath = "/html/body/main/div/div[2]/div/table/tbody";
        private String _viewProductsXpath = "//*[contains(text(), 'View Products')]";

        #endregion

        #region Web Elements

        private IWebElement descriptionTab;
        private IWebElement supportingDocsTab;
        private IWebElement approve;
        private IWebElement decline;
        private IWebElement importBody;

        #endregion

        public OfferImportProductsPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_importBodyID)));
            InitWebElements();
        }

        public OffersRequest  approveOffer()
        {
            WebDriverHelper.Driver.FindElementById(_approvebtnID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_offersBodyXpath)));

            var viewbtn = WebDriverHelper.Driver.FindElementsByXPath(_viewProductsXpath).ToList();
            viewbtn.Count();

            if (viewbtn.Count() == 0)
            {
                return null;
            }
            else
            {
                return new OffersRequest();
            }
        }

        public OffersRequest declineOffer()
        {
            WebDriverHelper.Driver.FindElementById(_declinebtnID).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_offersBodyXpath)));

            var viewbtn = WebDriverHelper.Driver.FindElementsByXPath(_viewProductsXpath).ToList();
            viewbtn.Count();

            if(viewbtn.Count() == 0)
            {
                return null;
            }
            else
            {
                return new OffersRequest();
            }
        }

        public void InitWebElements()
        {
            descriptionTab = WebDriverHelper.Driver.FindElementByXPath(_descriptionXpath);
            supportingDocsTab = WebDriverHelper.Driver.FindElementByXPath(_supportingDocsXpath);
            approve = WebDriverHelper.Driver.FindElementById(_approvebtnID);
            decline = WebDriverHelper.Driver.FindElementById(_declinebtnID);
            importBody = WebDriverHelper.Driver.FindElementById(_importBodyID);
        }

    }
}
