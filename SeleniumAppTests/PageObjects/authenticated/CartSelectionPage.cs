﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class CartSelectionPage
    {
        #region Web Element Identifiers

        private String _cartItemsTableClass = "cart-items-table";
        private String _exportBtnClass = "btn-lm-export";
        private String _continuePurchaseBtnXpath = "//*[contains(text(), 'Continue Purchase')]";
        private String _emptyCartClass = "empty-cart-link";
        private String _setProductActiveClass = "fa-check-circle";
        private String _setProductInactiveClass = "fa-times-circle";
        private String _trashProductClass = "fa-trash-o";
        private String _cartLineItemDropdownClass = "fa-angle-down";
        private String _continueCheckoutBtnClass = "checkout-btn";
        private String _emptyCartYesClass = "save-btn";
        private String _emptyCartNoClass = "cancel-btn";
        private String _emptyCartPopupClass = "modal-content";
        private String _emptyCartMsgClass = "message";

        #endregion

        #region Web Elements

        IWebElement cartitemstable;
        IWebElement exportbutton;
        IWebElement continuepurchase;
        IWebElement emptycart;
        IWebElement activeproduct;
        IWebElement inactiveproduct;
        IWebElement trashproduct;
        IWebElement cartlinedropdown;

        #endregion

        public CartSelectionPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_setProductActiveClass)));
            InitWebElements();
        }

        public CartShipmentPage continuePurchaseBtn()
        {
            WebDriverHelper.Driver.FindElementByXPath(_continuePurchaseBtnXpath).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_continueCheckoutBtnClass)));

            return new CartShipmentPage();
        }

        public void emptyCartLink()
        {
            WebDriverHelper.Driver.FindElementByClassName(_emptyCartClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(_emptyCartPopupClass)));
        }

        public void yesEmptyCart()
        {
            WebDriverHelper.Driver.FindElementByClassName(_emptyCartYesClass).Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.ClassName(_emptyCartMsgClass), "Cart deleted with success"));
        }

        public void InitWebElements()
        {
            cartitemstable = WebDriverHelper.Driver.FindElementByClassName(_cartItemsTableClass);
            exportbutton = WebDriverHelper.Driver.FindElementByClassName(_exportBtnClass);
            continuepurchase = WebDriverHelper.Driver.FindElementByXPath(_continuePurchaseBtnXpath);
            emptycart = WebDriverHelper.Driver.FindElementByClassName(_emptyCartClass);
            activeproduct = WebDriverHelper.Driver.FindElementByClassName(_setProductActiveClass);
            inactiveproduct = WebDriverHelper.Driver.FindElementByClassName(_setProductInactiveClass);
            trashproduct = WebDriverHelper.Driver.FindElementByClassName(_trashProductClass);
            cartlinedropdown = WebDriverHelper.Driver.FindElementByClassName(_cartLineItemDropdownClass);
        }
    }
}
