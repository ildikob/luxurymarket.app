﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class ProductBrandsPage
    {
        #region Web Element Identifiers

        private String _newestXpath = "//*[contains(text(), 'Newest')]";
        private String _priceLowHighXpath = "//*[contains(text(), 'Price Low-High')]";
        private String _priceHighLowXpath = "//*[contains(text(), 'Price High-Low')]";
        private String _categoriesFilterXpath = "//*[contains(text(), 'Categories')]";
        private String _colorsFilterXpath = "//*[contains(text(), 'Colors')]";
        private String _womensCategoryXpath = "//*[contains(text(), 'Womens')]";
        private String _productsTableClass = "row";
        private String _buyNowBtnId = "dLabel";
        private String _productListClass = "products-list";
        private String _product = "product";

        #endregion


        #region Web Elements

        private IWebElement newest;
        private IWebElement priceLowHigh;
        private IWebElement priceHighLow;
        private IWebElement categories;
        private IWebElement colors;
        private IWebElement womenCategory;
        private IWebElement allProducts;

        #endregion

        public ProductBrandsPage()
        {
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_womensCategoryXpath)));
            InitWebElements();
        }

        public ProductDetailsPage clickOnProduct()
        {
            IWebElement productsList = WebDriverHelper.Driver.FindElementByClassName(_productListClass);
            List<IWebElement> product = productsList.FindElements(By.ClassName(_product)).ToList();

            Random pickProduct = new Random();
            int randomproduct = pickProduct.Next(product.Count -1);

            product[randomproduct].Click();

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(_buyNowBtnId)));

            return new ProductDetailsPage();
        }

        public void InitWebElements()
        {
            newest = WebDriverHelper.Driver.FindElementByXPath(_newestXpath);
            priceLowHigh = WebDriverHelper.Driver.FindElementByXPath(_priceLowHighXpath);
            priceHighLow = WebDriverHelper.Driver.FindElementByXPath(_priceHighLowXpath);
            categories = WebDriverHelper.Driver.FindElementByXPath(_categoriesFilterXpath);
            colors = WebDriverHelper.Driver.FindElementByXPath(_colorsFilterXpath);
            womenCategory = WebDriverHelper.Driver.FindElementByXPath(_womensCategoryXpath);
            allProducts = WebDriverHelper.Driver.FindElementByClassName(_productsTableClass);
        }
    }
}
