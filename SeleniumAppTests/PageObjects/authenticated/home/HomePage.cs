﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated.home
{
    public class HomePage : BaseSitePage
    {
        #region Web Element Identifiers

        private String _homePageID = "homepage";

        #endregion

        #region Web Elements

        private IWebElement homepage;

        #endregion
        
        public HomePage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_homePageID)));
        }
    }
}
