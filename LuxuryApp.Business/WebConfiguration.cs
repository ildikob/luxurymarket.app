﻿using System.Configuration;

namespace LuxuryApp.Business
{
    public static class WebConfiguration
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["ConnectionStringName"]].ConnectionString;
    }
}
