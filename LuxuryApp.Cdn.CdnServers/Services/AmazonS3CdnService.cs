﻿using Amazon.S3;
using Amazon.S3.Model;
using LuxuryApp.Cdn.CdnServers.Model;
using LuxuryApp.Cdn.Contracts;
using LuxuryApp.Cdn.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Data.HashFunction;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Cdn.CdnServers.Services
{
    public class AmazonS3CdnService: IFileCdnService<ICdnAuthor, AmazonS3CdnSettings>
    {
        private IHashFunction _hashFunction;

        public AmazonS3CdnService(ICdnAuthor author, AmazonS3CdnSettings settings, IHashFunction hashFunction = null)
        {
            Author = author;
            Settings = settings;
            _hashFunction = hashFunction;
        }

        public ICdnAuthor Author { get; protected set; }
        public AmazonS3CdnSettings Settings { get; protected set; }

        private IAmazonS3 GetAmazonS3Client()
        {
            var regionEndpoint =
                (Amazon.RegionEndpoint)(
                typeof(Amazon.RegionEndpoint)
                .GetField(Settings.Region, BindingFlags.Static | BindingFlags.Public)
                .GetValue(null));
            return new AmazonS3Client(
                Settings.KeyId,
                Settings.KeySecret,
                regionEndpoint);
        }

        public CdnResult Upload(string key, byte[] content)
        {
            return Upload(new[] { new CdnUploadParameter { Key = key, Content = content } }).First();
        }

        public CdnResult<string> GetContentUrl(string key)
        {
            using (var s3client = GetAmazonS3Client())
            {
                var request = new GetPreSignedUrlRequest();
                request.BucketName = Settings.Bucket;
                request.Key = OptimizeKey(key);
                request.Expires = DateTime.Now.AddMinutes(5);
                return new CdnResult<string>(key, s3client.GetPreSignedURL(request));
            }
        }

        public CdnResult Remove(string key)
        {
            return Remove(new string[] { key }).First();
        }

        public IEnumerable<CdnResult> Upload(IEnumerable<CdnUploadParameter> uploadParameters)
        {
            uploadParameters = uploadParameters.ToArray();
            var requests = (from up in uploadParameters
                                .Where(x => !string.IsNullOrEmpty(x.Key) && x.Content != null)
                           select new {
                               Key = up.Key,
                               ObjRequest = new PutObjectRequest
                               {
                                   Key = OptimizeKey(up.Key),
                                   BucketName = Settings.Bucket,
                                   InputStream = new MemoryStream(up.Content)
                               }
                            }).ToArray();
            List<CdnResult> result = new List<CdnResult>(uploadParameters.Count());
            if (requests.Any())
            {
                using (var s3Client = GetAmazonS3Client())
                {
                    var uploaders = (from r in requests
                        select new
                        {
                            Key = r.Key,
                            Task = s3Client.PutObjectAsync(r.ObjRequest),
                        }).ToArray();
                    Task.WaitAll(uploaders.Select(x => x.Task).ToArray());
                    result.AddRange(uploaders
                        .Select(x => GetCdnResult(x.Key, x.Task))
                        .ToList());
                }
            }
            result.AddRange(uploadParameters.Where(x => string.IsNullOrEmpty(x.Key) || x.Content == null)
                .Select(y => 
                    new CdnResult(y.Key, new CdnExceptionBadRequest())).ToList());
            return result;
        }

        public IEnumerable<CdnResult>Remove(IEnumerable<string> keys)
        {
            var requests = from k in keys

                           select new
                           {
                               Key = k,
                               ObjRequest = new DeleteObjectRequest
                               {
                                   BucketName = Settings.Bucket,
                                   Key = OptimizeKey(k),
                               }
                           };
            IEnumerable<CdnResult> result;
            using (var s3Client = GetAmazonS3Client())
            {
                var removers =
                    (from r in requests
                     select new
                     {
                         Key = r.Key,
                         Task = s3Client.DeleteObjectAsync(r.ObjRequest)
                     }).ToArray();
                Task.WaitAll(removers.Select(x => x.Task).ToArray());
                result = removers.Select(x => GetCdnResult(x.Key, x.Task)).ToList();
            }
            return result;
        }

        public IEnumerable<CdnResult<string>> GetContentUrls(IEnumerable<string> keys)
        {
            return keys.Select(k => GetContentUrl(k));
        }

        private CdnResult GetCdnResult(string key, Task task)
        {
            if (task.IsCompleted)
            {
                return new CdnResult(key);
            }
            else
            {
                return new CdnResult(key, HandleS3TaskException(task.Exception));
            }
        }

        private CdnException HandleS3TaskException(Exception ex)
        {
            //todo : handle amazon exception cases
            return new CdnExceptionInternalError(ex);
        }

        private string OptimizeKey(string key)
        {
            if (_hashFunction == null)
            {
                return key;
            }
            var prefix = BitConverter.ToUInt32(_hashFunction.ComputeHash(Encoding.UTF8.GetBytes(key)), 0);
            return string.Format("{0}{1}", prefix, key);
        }
    }
}
